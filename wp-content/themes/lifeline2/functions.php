<?php
require_once( get_template_directory() . '/core/init.php' );
lifeline2_Module_Init::lifeline2_Init();

require_once( get_template_directory() . '/envato_setup/envato_setup.php' );
add_filter('lifeline2_theme_setup_wizard_username', 'lifeline2_set_theme_setup_wizard_username', 10);
if( ! function_exists('lifeline2_set_theme_setup_wizard_username') ){
    function lifeline2_set_theme_setup_wizard_username($username){
        return 'webinane';
    }
}

add_filter('lifeline2_theme_setup_wizard_oauth_script', 'lifeline2_set_theme_setup_wizard_oauth_script', 10);
if( ! function_exists('lifeline2_set_theme_setup_wizard_oauth_script') ){
    function lifeline2_set_theme_setup_wizard_oauth_script($oauth_url){
        return 'http://api.webinane.com/envato/api/server-script.php';
    }
}

function lifeline2_make_href_root_relative( $input ) {
    if ( function_exists( 'lifeline2_href_root' ) ) {
        return lifeline2_href_root( $input );
    }
}

defined( 'LIFELINE2_KEY' ) || define( 'LIFELINE2_KEY', 'lifeline2' );

function lifeline2_root_relative_permalinks( $input ) {
    return lifeline2_make_href_root_relative( $input );
}
add_filter( 'the_permalink', 'lifeline2_root_relative_permalinks' );


add_filter( 'post_row_actions', 'lifeline2_remove_view_link_faq', 10, 1 );

function lifeline2_remove_view_link_faq( $action ) {
    global $post;
    if ( lifeline2_set( $post, 'post_type' ) == 'lifeline2_forms' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_faq' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_testimonial' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_sponsor' ) {
        unset( $action['view'] );
        return $action;
    } else {
        return $action;
    }
}
add_filter( 'faq_category_row_actions', 'lifeline2_remove_view_link_faq_category' );

function lifeline2_remove_view_link_faq_category( $action ) {

    unset( $action['view'] );
    return $action;
}
add_filter( 'testimonial_category_row_actions', 'lifeline2_remove_view_link_testimonial_category' );

function lifeline2_remove_view_link_testimonial_category( $action ) {

    unset( $action['view'] );
    return $action;
}
add_filter( 'sponsor_category_row_actions', 'rlifeline2_emove_view_link_sponsor_category' );

function rlifeline2_emove_view_link_sponsor_category( $action ) {

    unset( $action['view'] );
    return $action;
}
function lifeline2_remove_row_actions( $actions )

{
    if( get_post_type() === 'lif_sponsor' || get_post_type() === 'lif_testimonial' || get_post_type() === 'lif_faq' ) // choose the post type where you want to hide the button
        unset( $actions['view'] ); // this hides the VIEW button on your edit post screen
    return $actions;
}
add_filter( 'post_row_actions', 'lifeline2_remove_row_actions', 10, 1 );
add_filter( 'get_sample_permalink_html', 'lifeline2_perm', '', 4 );

function lifeline2_perm( $return, $id, $new_title, $new_slug ) {

    $post = get_post( $id );
    if ( lifeline2_set( $post, 'post_type' ) == 'lifeline2_forms' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_faq' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_testimonial' || lifeline2_set( $post, 'post_type' ) == 'lifeline2_sponsor' ) {
        return '';
    } else {
        return $return;
    }
}

add_filter('get_sample_permalink_html', 'lifeline2_hide_permalinks', 10, 5);

function lifeline2_hide_permalinks($return, $id, $new_title, $new_slug)
{
    $post = get_post($id);
    if (lifeline2_set($post, 'post_type') == 'lif_sponsor') {
        return '';
    } else {
        return $return;
    }
}
add_action( 'wp_enqueue_scripts', 'lifeline2_override_woo_frontend_scripts' );

function lifeline2_override_woo_frontend_scripts() {
    if ( function_exists( 'lifeline2_deequ' ) ) {
        lifeline2_deequ( 'wc-cart' );
    }
}

add_filter( 'get_header', 'lifeline2_under_construction' );

function lifeline2_under_construction() {
    if ( !is_admin() && !is_user_logged_in() ) {
        $settings = lifeline2_get_theme_options();
//printr($settings);
        if ( lifeline2_set( $settings, 'under_construction_status' ) ) {

            include('tpl-coming_soon.php');
            exit;
        }
    }
}

add_filter( 'widget_title', 'lifeline2_OverwriteTitle', 10, 3 );

function lifeline2_OverwriteTitle( $title, $instance = array(), $id_base = '' ) {

    if($id_base == 'rss') return $title;
    $opt      = lifeline2_get_theme_options();
    $default  = array(
        'archives', 'calendar', 'categories', 'nav_menu', 'meta', 'pages', 'recent-comments', 'recent-posts', 'tag_cloud', 'text', 'search'
    );
    $lifeline = array(
        'lifeline2-upcoming-events', 'lifeline2-twitter-tweets', 'lifeline2-recent-posts', 'lifeline2-flickr-feed', 'lifeline2-donation-banner', 'lifeline2-custom-video-banner', 'lifeline2-about-us'
    );
    if ( in_array( $id_base, $default ) ) {
        $exp      = explode( ' ', $title, 2 );
        $subTitle = lifeline2_set( $opt, 'optSidebarWidget' . $id_base );
        return '<h4>' . lifeline2_set( $exp, '0' ) . ' <span>' . lifeline2_set( $exp, '1' ) . '</span></h4><span>' . $subTitle . '</span>';
    } else if ( in_array( $id_base, $lifeline ) ) {
        $exp      = explode( ' ', $title, 2 );
        $subTitle = lifeline2_set( $instance, 'subtitle' );
        return '<h4>' . lifeline2_set( $exp, '0' ) . ' <span>' . lifeline2_set( $exp, '1' ) . '</span></h4><span>' . $subTitle . '</span>';
    } else {
        $exp      = explode( ' ', $title, 2 );
        $subTitle = '';
        return '<h4>' . lifeline2_set( $exp, '0' ) . ' <span>' . lifeline2_set( $exp, '1' ) . '</span></h4><span>' . $subTitle . '</span>';
    }
}

/**
 * My Account Donation Menu Items
 *
 * @param arr $items
 * @return arr
 */
function lifeline2_account_menu_items( $items ) {

    $items['donations'] = __( 'Donations', 'lifeline2' );

    return $items;

}

add_filter( 'woocommerce_account_menu_items', 'lifeline2_account_menu_items', 10, 1 );
/**
 * Add endpoint
 */
function lifeline2_add_my_account_endpoint() {

    add_rewrite_endpoint( 'donations', EP_PAGES );

}

add_action( 'init', 'lifeline2_add_my_account_endpoint' );
/**
 * Donations content
 */
function lifeline2_donations_endpoint_content() {
    $default = get_option('wp_donation_basic_settings', true);
    $basic_set = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
    $trans_num = (WPD_Common::wpd_set($basic_set, 'trans_num')) ? WPD_Common::wpd_set($basic_set, 'trans_num') : 0;
    $hide = '';
    global $wpdb;
    global $charset_collate;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $table_name = $wpdb->prefix . "wpd_easy_donation";
    $wpdb->show_errors();

    $all_query = "SELECT COUNT(id) FROM $table_name";
    $all_result = $wpdb->get_results($all_query);
    $count_all = WPD_Common::wpd_set(WPD_Common::wpd_set($all_result, '0'), 'COUNT(id)');


    $success_q = "SELECT COUNT(id) FROM $table_name where source IN ('Credit Card','PayPal','custom','Bank')";
    $success_r = $wpdb->get_results($success_q);
    $success_all = WPD_Common::wpd_set(WPD_Common::wpd_set($success_r, '0'), 'COUNT(id)');

    $fail_q = "SELECT COUNT(id) FROM $table_name where source IN( 'Bank' )";
    $fail_r = $wpdb->get_results($fail_q);
    $fail_all = WPD_Common::wpd_set(WPD_Common::wpd_set($fail_r, '0'), 'COUNT(id)');

    global $current_user;
    wp_get_current_user();
    $my_donation = $current_user->user_email;
    $res_q = "SELECT * FROM $table_name where email IN ('$my_donation') ORDER BY id DESC LIMIT 0, $trans_num";
    //printr($res_q);
    $res_r = $wpdb->get_results($res_q);

    update_option('donation_recent_query', $res_q);
    if (!empty($res_r)) {
        foreach ($res_r as $res) {
            $date = strtotime(WPD_Common::wpd_set($res, 'date'));
            $new_date = date('d M Y', $date);
            echo '<hr>';
            echo '<br><table><tr style="background-color:silver; color:#fff;">';
            echo _e('<td><b>Donation Id</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Date</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Name</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Amount</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Source</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Donation For</b>', WPD_NAME).'</td>';
            echo _e('<td><b>Email Id</b>', WPD_NAME).'</td></tr>';

            echo '<tr>';
            echo '<td>'.WPD_Common::wpd_set($res, 'id').'</td>';
            echo '<td>'.$new_date.'</td>';
            echo '<td>'.WPD_Common::wpd_set($res, 'f_name').'&nbsp';
            echo WPD_Common::wpd_set($res, 'l_name').'</td>';
            echo '<td>'.WPD_Common::wpd_set($res, 'amount');
            echo WPD_Common::wpd_set($res, 'currency').'</td>';
            echo '<td>'.WPD_Common::wpd_set($res, 'source').'</td>';
            echo '<td>'.WPD_Common::wpd_set($res, 'donation_for').'</td>';
            echo '<td>'.WPD_Common::wpd_set($res, 'email').'</td></tr></table>';
        }
    }

    //printr($res_r); exit();
}

add_action( 'woocommerce_account_donations_endpoint', 'lifeline2_donations_endpoint_content' );
/**
 * Helper: is endpoint
 */
function lifeline2_is_endpoint( $endpoint = false ) {
    global $wp_query;

    if( !$wp_query )
        return false;

    return isset( $wp_query->query[ $endpoint ] );

}

if ( function_exists( 'vc_map' ) ) {
	function lifeline2_vc_disable_update() {
		if ( function_exists( 'vc_license' ) && function_exists( 'vc_updater' )
		     && ! vc_license()->isActivated()
		) {

			remove_filter( 'upgrader_pre_download',
				[ vc_updater(), 'preUpgradeFilter' ], 10 );
			remove_filter( 'pre_set_site_transient_update_plugins', [
				vc_updater()->updateManager(),
				'check_update'
			] );

		}
	}

	add_action( 'admin_init', 'lifeline2_vc_disable_update', 9 );
}

function cmb2_sanitize_text_callback( $override_value, $value ) {
	//convert square brackets to angle brackets
	$value = str_replace('[', '<', $value);
	$value = str_replace(']', '>', $value);
	return $value;
}
add_filter( 'cmb2_sanitize_text', 'cmb2_sanitize_text_callback', 10, 2 );

if( function_exists( 'layerslider_set_as_theme' ) ) {
	layerslider_set_as_theme();
}
// Remove LayerSlider's update notifications
function filter_plugin_updates( $value ) {
	unset( $value->response['layerslider/layerslider.php'] );
	return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );