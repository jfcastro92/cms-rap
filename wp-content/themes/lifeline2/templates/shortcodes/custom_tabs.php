<?php

extract( $values );

$tabs_informations = (json_decode(urldecode($tabs_informations)));

$uniqid = uniqid('custom_tabs' );

if (class_exists('lifeline2_Resizer'))
$img_obj = new lifeline2_Resizer();
$parent_class='';
if($tab_style=='style1'):
$parent_class='tabs1';
elseif($tab_style=='style2'):
$parent_class='tabs2';
elseif($tab_style=='style3'):
$parent_class='tabs3';
elseif($tab_style=='style4'):
$parent_class='tabs4';
elseif($tab_style=='style5'):
$parent_class='tabs4 gray-bg';
elseif($tab_style=='style5'):
$parent_class='tabs4 gray-bg';
elseif($tab_style=='style6'):
$parent_class='tabs4 bg-img';
endif;
$i=1;
$j=1;

?>
<?php if (!empty($tabs_informations)): ?>
    <div class="<?php echo esc_attr($parent_class); ?> tabs-styles" <?php echo($tab_style=='style6' && $tab_bg_image)?' style="background: url('.wp_get_attachment_url($tab_bg_image).') no-repeat scroll center / cover"' :'';?>>
        <ul class="nav nav-tabs">
            <?php
            foreach ($tabs_informations as $tabs_information):
                $tab_title = lifeline2_set($tabs_information, 'tab_title');
                ?>
                <li <?php echo($i==1)? 'class="active"' :''; ?>><a href="#tabs1-tab<?php echo esc_attr( $uniqid . $i ); ?>" data-toggle="tab"><?php echo esc_html($tab_title); ?></a></li>
                <?php $i++;
            endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php
            foreach ($tabs_informations as $tabs_information):
                $description = lifeline2_set($tabs_information, 'description');
                ?>
                <div class="tab-pane fade <?php echo ($j==1)?'in active':''; ?>" id="tabs1-tab<?php echo esc_attr( $uniqid . $j ); ?>">
                    <p><?php echo esc_html($description); ?></p>
                </div>
                <?php $j++;
            endforeach; ?>

        </div>
    </div>
    <?php
    wp_enqueue_script('lifeline2_' . 'bootstrap');
endif;
?>
