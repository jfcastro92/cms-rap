<?php

wp_head();
$settings = lifeline2_get_theme_options();
$bgimg = array(lifeline2_set($settings, 'under_construction_bgimg'));
$date =  lifeline2_set($settings, 'under_construction_date');
$new_date = date('Y-m-d', strtotime($date));
//printr($new_date);
$counter = 0;

wp_enqueue_script(array('lifeline2_' . 'modernizr-custom', 'lifeline2_' . 'poptrox', 'lifeline2_' . 'plugin', 'lifeline2_' . 'throttle', 'lifeline2_' . 'countdown', 'lifeline2_' . 'classycountdown'));
                        ?>

<section class="block coming-layer">
    <div class="fixed-bg" style="background: url(<?php echo ($bgimg[0]['url']); ?>)no-repeat scroll center / cover;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="coming-soon-page">
                    <div class="coming-soon-inner">
                        <?php echo (lifeline2_set($settings, 'under_construction_titletext')) ? '<h4>'.lifeline2_set($settings, 'under_construction_titletext').'</h4>' : '';?>
                        <div class="coming-soon-timer">
                            <div id="coming-counter" class="comming-counter"></div>
                        </div>
                        <?php echo (lifeline2_set($settings, 'under_construction_title')) ? '<p>'.lifeline2_set($settings, 'under_construction_title').'</p>' : '';?>
                        <form class="coming-sub-form">
                            <input type="email" placeholder="Submit Your Email">
                            <button class="submit"><i class="fa fa-arrow-right"></i></button>
                        </form>
                    </div>
                </div><!-- Coming Soon Page -->
            </div>
        </div>
    </div>
</section>
    <div class="coming-bottom">
        <div class="container">
            <p><?php echo balanceTags( lifeline2_set( $settings, 'copyright_text' ) ); ?></p>
        </div>
    </div>
<?php
$jsOutput = "jQuery(document).ready(function($){
        'use strict';
        $('#coming-counter').ClassyCountdown({
            theme: 'flat-colors-wide',
           now: new Date( ).getTime() / 1000,
           end: new Date( '$new_date' ).getTime() / 1000,
            style: {
                element: '',
                textResponsive: .5,
                days: {
                    gauge: {
                        thickness: .02,
                        bgColor: 'rgba(255,255,255,0.5)',
                        fgColor: '#e47257'
                    },
                },
                hours: {
                    gauge: {
                        thickness: .02,
                        bgColor: 'rgba(255,255,255,0.5)',
                        fgColor: '#e47257'
                    },
                },
                minutes: {
                    gauge: {
                        thickness: .02,
                        bgColor: 'rgba(255,255,255,0.5)',
                        fgColor: '#e47257'
                    }
                },
                seconds: {
                    gauge: {
                        thickness: .02,
                        bgColor: 'rgba(255,255,255,0.5)',
                        fgColor: '#e47257'
                    }
                }

            }
        });

    });";
wp_add_inline_script('lifeline2_' . 'countdown', $jsOutput);
?>
<?php
wp_footer();
