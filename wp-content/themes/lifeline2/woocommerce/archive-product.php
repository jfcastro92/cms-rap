<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.4.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
get_header( 'shop' );
$opt = lifeline2_get_theme_options();
if ( is_tax() ) {
    $queried_object = get_queried_object();
    $term_id        = $queried_object->term_id;
    $meta           = get_option( 'lifeline2_' . 'product_cat_settings' . $term_id );
    //printr($queried_object);
    $cat_meta       = lifeline2_set( lifeline2_set( $meta, 'sh_product_cat_options' ), 0 );
    $sidebar        = (lifeline2_set( $cat_meta, 'metaSidebar' )) ? lifeline2_set( $cat_meta, 'metaSidebar' ) : '';
    $position       = (lifeline2_set( $cat_meta, 'layout' )) ? lifeline2_set( $cat_meta, 'layout' ) : '';
    $background     = (lifeline2_set( $cat_meta, 'title_section_bg' )) ? 'style="background:url(' . lifeline2_set( $cat_meta, 'title_section_bg' ) . ');"' : '';
    $top_section    = lifeline2_set( $cat_meta, 'page_title_section' );
    $page_title     = $queried_object->name;
} else {
    $page_id        = wc_get_page_id( 'shop' );
    $lifeline2_meta = lifeline2_Common::lifeline2_post_data( $page_id, 'page' );
    $sidebar        = (lifeline2_set( $lifeline2_meta, 'metaSidebar' )) ? lifeline2_set( $lifeline2_meta, 'metaSidebar' ) : '';
    $position       = (lifeline2_set( $lifeline2_meta, 'layout' )) ? lifeline2_set( $lifeline2_meta, 'layout' ) : '';
    $background     = (lifeline2_set( $lifeline2_meta, 'title_section_bg' )) ? 'style="background:url(' . lifeline2_set( $lifeline2_meta, 'title_section_bg' ) . ');"' : '';
    $top_section    = lifeline2_set( $lifeline2_meta, 'show_title_section' );
    $page_title     = (lifeline2_set( $lifeline2_meta, 'banner_title' )) ? lifeline2_set( $lifeline2_meta, 'banner_title' ) : get_the_title( $page_id );
}

if ( $top_section && apply_filters( 'woocommerce_show_page_title', true ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, true ) );
$span = ($sidebar != '') ? 'col-md-9' : 'col-md-12';
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>	
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <?php
                    /**
                     * woocommerce_archive_description hook
                     *
                     * @hooked woocommerce_taxonomy_archive_description - 10
                     * @hooked woocommerce_product_archive_description - 10
                     */
                    do_action( 'woocommerce_archive_description' );
                    ?>

                    <?php if ( have_posts() ) : ?>

                        <?php
                        /**
                         * woocommerce_before_shop_loop hook
                         *
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action( 'woocommerce_before_shop_loop' );
                        ?>



                        <?php woocommerce_product_subcategories(); ?>
                        <div class="products-list">
                            <div class="row">
                                <?php while ( have_posts() ) : the_post(); ?>

                                    <?php wc_get_template_part( 'content', 'product' ); ?>

                                <?php endwhile; // end of the loop.  ?>
                            </div>
                        </div>


                        <?php
                        /**
                         * woocommerce_after_shop_loop hook
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                        ?>

                    <?php elseif ( !woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                        <?php wc_get_template( 'loop/no-products-found.php' ); ?>

                    <?php endif; ?>


                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>   

<?php get_footer( 'shop' ); ?>
