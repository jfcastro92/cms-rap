<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.2
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
?>
<div class="images">

<?php
if ( has_post_thumbnail() ) {

    $image_title   = esc_attr( get_the_title( get_post_thumbnail_id() ) );
    $image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
    $image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
    $image         = get_the_post_thumbnail( $post->ID, array(370,545), array(
        'title' => $image_title,
        'alt'   => $image_title
        ) );
    echo balanceTags($image);
} else {

    echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'lifeline2' ) ), $post->ID );
}
do_action( 'woocommerce_product_thumbnails' );
?>

</div>
