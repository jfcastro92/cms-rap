<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !$messages ) {
    return;
}
?>

<?php foreach ( $messages as $message ) : ?>
    <div class="woocommerce-message"><?php echo wp_kses_post( str_replace( 'button wc-forward', 'theme-btn theme-mini-btn', $message ) ); ?></div>
<?php endforeach; ?>
