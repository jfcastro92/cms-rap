<?php
/**
 * Shipping Calculator
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( get_option( 'woocommerce_enable_shipping_calc' ) === 'no' || ! WC()->cart->needs_shipping() ) {
	return;
}
wp_enqueue_script('lifeline2_'.'select2');
?>

<div class="cart-heading">
    <h5><?php esc_html_e('Calculate Shipping', 'lifeline2');?></h5>
</div>
<div class="cart-list">
<form class="woocommerce-shipping-calculator" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
    <div class="cart-item">
	<div class="row">
            <div class="col-md-3">
                <select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state select" rel="calc_shipping_state">
                        <option value=""><?php esc_html_e( 'Select a country', 'lifeline2' ).'&hellip;'; ?></option>
                        <?php
                                foreach( WC()->countries->get_shipping_countries() as $key => $value )
                                        echo '<option value="' . esc_attr( $key ) . '"' . selected( WC()->customer->get_shipping_country(), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
                        ?>
                </select>
            </div>
            <div class="col-md-3" id="calc_shipping_state_field">
	
                <?php
                        $current_cc = WC()->customer->get_shipping_country();
                        $current_r  = WC()->customer->get_shipping_state();
                        $states     = WC()->countries->get_states( $current_cc );

                        // Hidden Input
                        if ( is_array( $states ) && empty( $states ) ) {

                                ?><input type="hidden" name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'lifeline2' ); ?>" /><?php

                        // Dropdown Input
                        } elseif ( is_array( $states ) ) {

                                ?><span>
                                        <select name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'lifeline2' ); ?>">
                                                <option value=""><?php esc_html_e( 'Select a state', 'lifeline2' ).'&hellip;'; ?></option>
                                                <?php
                                                        foreach ( $states as $ckey => $cvalue )
                                                                echo '<option value="' . esc_attr( $ckey ) . '" ' . selected( $current_r, $ckey, false ) . '>' . $cvalue .'</option>';
                                                ?>
                                        </select>
                                </span><?php

                        // Standard Input
                        } else {

                                ?><input type="text" class="input-text field" value="<?php echo esc_attr( $current_r ); ?>" placeholder="<?php esc_attr_e( 'State / county', 'lifeline2' ); ?>" name="calc_shipping_state" id="calc_shipping_state" /><?php

                        }
                ?>
            </div>

            <?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_city', false ) ) : ?>

                    <div class="form-row form-row-wide col-md-3" id="calc_shipping_city_field">
                            <input type="text" class="input-text field" value="<?php echo esc_attr( WC()->customer->get_shipping_city() ); ?>" placeholder="<?php esc_attr_e( 'City', 'lifeline2' ); ?>" name="calc_shipping_city" id="calc_shipping_city" />
                    </div>

            <?php endif; ?>

            <?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_postcode', true ) ) : ?>

                    <div class="form-row form-row-wide col-md-3" id="calc_shipping_postcode_field">
                        <input type="text" class="input-text field" value="<?php echo esc_attr( WC()->customer->get_shipping_postcode() ); ?>" placeholder="<?php esc_attr_e( 'Postcode / Zip', 'lifeline2' ); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
                    </div>

            <?php endif; ?>
            
            <div class="col-md-3"><button type="submit" name="calc_shipping" value="1" class="theme-btn"><?php esc_html_e( 'Update Totals', 'lifeline2' ); ?></button></div>

            <?php wp_nonce_field( 'woocommerce-cart' ); ?>
        </div>
    </div>
</form>
</div>
