<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' );
?>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

    <?php do_action( 'woocommerce_before_cart_table' ); ?>

    <div class="cart-table">
        <div class="cart-heading">
            <ul>
                <li class="col1"><?php esc_html_e( 'Porducts', 'lifeline2' ); ?></li>
                <li class="col2"><?php esc_html_e( 'Price', 'lifeline2' ); ?></li>
                <li class="col2"><?php esc_html_e( 'Quantity', 'lifeline2' ); ?></li>
                <li class="col2"><?php esc_html_e( 'ID', 'lifeline2' ); ?></li>
                <li class="col2"><?php esc_html_e( 'Total', 'lifeline2' ); ?></li>
                <li class="col2"><?php esc_html_e( 'Remove', 'lifeline2' ); ?></li>
            </ul>
        </div>
        <div class="cart-list">

            <?php do_action( 'woocommerce_before_cart_contents' ); ?>

            <?php
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                    ?>
                    <div class="cart-item">
                        <ul class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                            <li class="col1">
                                <?php
                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                if ( !$_product->is_visible() ) {
                                    echo balanceTags($thumbnail);
                                } else {
                                    printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                                }
                                ?>
                                <span>
                                    <?php
                                    if ( !$_product->is_visible() ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                                    }

                                    // Meta data
                                    echo wc_get_formatted_cart_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on back order', 'lifeline2' ) . '</p>';
                                    }
                                    ?>
                                </span>
                            </li>
                            <li class="col2">
                                <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
                            </li>
                            <li class="col2">
                                <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {
                                    $product_quantity = woocommerce_quantity_input( array(
                                        'input_name'  => "cart[{$cart_item_key}][qty]",
                                        'input_value' => $cart_item['quantity'],
                                        'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                        'min_value'   => '0'
                                        ), $_product, false );
                                }
                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                                ?>
                            </li>
                            <li class="col2"><?php echo '#' . $product_id; ?></li>
                            <li class="col2">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                                ?>
                            </li>
                            <li class="col2">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s"><i class="ti-close"></i></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item', 'lifeline2' ), esc_attr( $product_id ), esc_attr( $_product->get_sku() )
                                    ), $cart_item_key );
                                ?>
                            </li>
                        </ul>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="cart-bottom">
                <?php if ( WC()->cart->coupons_enabled() ) { ?>
                    <div class="coupon">

                        <label for="coupon_code"><?php esc_html_e( 'Coupon', 'lifeline2' ); ?>:</label>
                        <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'lifeline2' ); ?>" />
                        <input type="submit" class=" theme-btn" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'lifeline2' ); ?>" />
                        <?php do_action( 'woocommerce_cart_coupon' ); ?>
                    </div>
                <?php } ?>

                <input type="submit" class="theme-btn" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'lifeline2' ); ?>" />

                <?php do_action( 'woocommerce_cart_actions' ); ?>

                <?php wp_nonce_field( 'woocommerce-cart' ); ?> 
                <a title="<?php echo esc_attr( get_the_title( wc_get_page_id( 'shop' ) ) ); ?>" href="<?php echo esc_url( get_permalink( wc_get_page_id( 'shop' ) ) ); ?>" class="theme-btn" itemprop="url"><?php esc_html_e( 'Continue Shopping', 'lifeline2' ); ?></a>
            </div>
        </div>
    </div>
</div>
<?php do_action( 'woocommerce_cart_collaterals' ); ?>
</form>


<?php do_action( 'woocommerce_after_cart' ); ?>
<?php
wp_enqueue_script( 'lifeline2_' . 'select2' );
$custom_script = 'jQuery(document).ready(function($) {
                $(".form-control").bootstrapNumber();
                $(".cart-item select").select2();
            });	';
wp_add_inline_script( 'lifeline2_' . 'select2', $custom_script )
?>
