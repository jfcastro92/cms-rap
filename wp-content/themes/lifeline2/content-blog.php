<?php
global $wp_query, $post;
$args   = array(
    'post_type'   => 'post',
    'post_status' => 'publish',
    'paged'       => (isset( $wp_query->query['paged'] )) ? $wp_query->query['paged'] : 1,
);
$querys = new WP_Query( $args );
while ( $querys->have_posts() ): $querys->the_post();
    $format = get_post_format( $post->ID );
    if ( false === $format ) {
        $format = 'standard';
    }
    if ( $format == 'video' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-video.php' );
    } else if ( $format == 'link' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-link.php' );
    } else if ( $format == 'image' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-image.php' );
    } else if ( $format == 'quote' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-quote.php' );
    } else if ( $format == 'status' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-status.php' );
    } else if ( $format == 'gallery' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-gallery.php' );
    } else if ( $format == 'audio' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/format-audio.php' );
    } else if ( $format == 'standard' ) {
        include( lifeline2_ROOT . 'core/application/library/post-formats/fromat-standerd.php' );
    }
endwhile;
wp_reset_postdata();
lifeline2_Common::_the_pagination( array( 'total' => $querys->max_num_pages ), 1, true );
