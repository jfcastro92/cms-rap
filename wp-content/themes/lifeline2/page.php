<?php
get_header();
$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' );
$sidebar    = lifeline2_set( $page_meta, 'metaSidebar' );
$position   = lifeline2_set( $page_meta, 'layout' );
$span       = ( $sidebar ) ? 'col-md-9' : 'col-md-12';
$page_title = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$background = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
if ( lifeline2_set( $page_meta, 'show_title_section' ) )
        echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">

                    <?php while ( have_posts() ): the_post(); ?>
                        <div class="page-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages(); ?>
                        </div>
                    <?php endwhile; ?>
                    <?php comments_template(); ?>

                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
