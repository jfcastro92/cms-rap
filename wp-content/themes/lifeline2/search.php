<?php
get_header();
$lifeline2_settings = lifeline2_get_theme_options();

$lifeline2_sidebar     = (lifeline2_set( $lifeline2_settings, 'search_page_sidebar' )) ? lifeline2_set( $lifeline2_settings, 'search_page_sidebar' ) : '';
$lifeline2_layout      = (lifeline2_set( $lifeline2_settings, 'search_page_sidebar_layout' )) ? lifeline2_set( $lifeline2_settings, 'search_page_sidebar_layout' ) : '';
$lifeline2_page_title  = (lifeline2_set( $lifeline2_settings, 'search_page_title' )) ? lifeline2_set( $lifeline2_settings, 'search_page_title' ) : 'Blog Archive';
$lifeline2_top_section = lifeline2_set( $lifeline2_settings, 'search_page_title_section' );
$lifeline2_background  = (lifeline2_set( lifeline2_set( $lifeline2_settings, 'search_title_section_bg' ), 'background-image' )) ? 'style="background:url(' . lifeline2_set( lifeline2_set( $lifeline2_settings, 'search_title_section_bg' ), 'background-image' ) . ') no-repeat scroll 0 0 / 100% 100%"' : '';

$lifeline2_style       = (lifeline2_set( $lifeline2_settings, 'search_posts_listing_style' )) ? lifeline2_set( $lifeline2_settings, 'search_posts_listing_style' ) : 'list';
$lifeline2_limit       = lifeline2_set( $lifeline2_settings, 'search_page_posts_character_limit' );
$lifeline2_title_limit = (lifeline2_set( $lifeline2_settings, 'search_title_character_limit' )) ? lifeline2_set( $lifeline2_settings, 'search_title_character_limit' ) : 30;
$lifeline2_author      = lifeline2_set( $lifeline2_settings, 'search_blog_post_author' );
$lifeline2_date        = lifeline2_set( $lifeline2_settings, 'search_blog_post_date' );
$lifeline2_more_btn    = lifeline2_set( $lifeline2_settings, 'search_post_read_more' ); //printr($more);
$lifeline2_label       = lifeline2_set( $lifeline2_settings, 'search_post_read_more_label' );
$lifeline2_tags        = lifeline2_set( $lifeline2_settings, 'search_blog_post_tags' );
if ( $lifeline2_top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $lifeline2_page_title, $lifeline2_background, true ) );
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'left' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
                <div class="<?php echo (($lifeline2_layout == 'left' || $lifeline2_layout == 'right') && $lifeline2_style != 'grid_3_cols') ? 'col-md-9' : 'col-md-12'; ?> column">
                    <div class="search-page">
                        <h5><?php esc_html_e( 'Related Search Result', 'lifeline2' ); ?> <span>"<?php echo get_search_query(); ?>"</span></h5>
                        <?php if ( have_posts() ): ?>
                            <div class="blog-list<?php echo ($lifeline2_style == 'list') ? ' list-style' : ''; ?>">
                                <div class="row">

                                    <?php
                                    echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '<div class="col-md-12">' : '';

                                    while ( have_posts() ) {
                                        the_post();
                                        include(lifeline2_ROOT . 'post-format/content-standard.php');
                                    }
                                    echo ($lifeline2_style != 'grid_3_cols' || $lifeline2_style != 'grid_2_cols') ? '</div>' : '';
                                    ?>
                                </div>
                            </div><!-- Blog List -->
                            <?php
                            lifeline2_Common::lifeline2_pagination();
                            ?>
                        <?php else: ?>
                            <?php if ( lifeline2_set( $lifeline2_settings, 'search_noresult_text' ) ): ?>
                                <p><?php echo esc_html( lifeline2_set( $lifeline2_settings, 'search_noresult_text' ) ); ?></p>
                            <?php else: ?>
                                <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'lifeline2' ); ?></p>
                            <?php endif; ?>
                            <?php if ( lifeline2_set( $lifeline2_settings, 'search_form' ) ): ?>
                                <form action="<?php echo home_url( '/' ); ?>" class="contact-form" method="get" role="search">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="<?php esc_attr_e( 'Enter Search Item', 'lifeline2' ); ?>" class="search" name="s" value="<?php echo get_search_query(); ?>">
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit"><?php esc_attr_e( 'Search', 'lifeline2' ); ?></button>
                                        </div>
                                    </div>
                                </form>
                                <div class="full-border"></div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ( $lifeline2_sidebar && $lifeline2_layout == 'right' && is_active_sidebar( $lifeline2_sidebar ) && $lifeline2_style != 'grid_3_cols' ) : ?>
                    <aside class="col-md-3 column sidebar">
                        <?php dynamic_sidebar( $lifeline2_sidebar ); ?>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>