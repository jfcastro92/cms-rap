<?php
/**
 * switch_label field class.
 *
 * @version     1.0.0
 */
/* exit if accessed directly. */
if ( !defined( 'ABSPATH' ) ) exit;

/* don't duplicate me! */
if ( !class_exists( 'ReduxFramework_importer' ) ) {

    /**
     *  ReduxFramework_switch_label class.
     *
     * @since       1.0.0
     */
    class ReduxFramework_importer extends ReduxFramework {

        /**
         * field constructor.
         *
         * @access      public
         * @since       1.0.0         
         * @return      void
         */
        public function __construct( $field = array(), $value = '', $parent ) {
            $this->parent = $parent;
            $this->field  = $field;
            $this->value  = $value;
        }
        /* __construct() */

        /**
         * render field output.
         * 
         * @access      public
         * @since       1.0.0
         * @return      void
         */
        public function render() {
            include lifeline2_ROOT . 'core/application/panel/redux-extensions/extensions/importer/demos.php';
            if ( !empty( $demosArray ) && count( $demosArray ) > 0 ) {
                ?>
                <div class="redux-demo-importer" data-opt-name="" data-id="opt-<?php echo esc_attr( lifeline2_set( $this->field, 'id' ) ) ?>">
                    <?php
                    foreach ( $demosArray as $demo ) {
                        ?>
                        <div class="wrapper">
                            <div class="image">
                                <img src="<?php echo esc_url( lifeline2_set( $demo, 'src' ) ) ?>" />
                                <span class="preview">
                                    <a title="<?php esc_html_e( 'view demo', 'lifeline2' ) ?>" href="<?php echo esc_url( lifeline2_set( $demo, 'preview' ) ) ?>" target="_blank"><img src="<?php echo esc_url( lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/external_link.png' ) ?>" /></a>
                                </span>
                                <div class="overly-btn">
                                    <a data-uri="<?php echo esc_attr( lifeline2_set( $demo, 'uri' ) ) ?>" id="overlay-install-demo" href="javascript:void(0)" data-demo="1">
                                        <?php esc_html_e( 'Import', 'lifeline2' ) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="bar">
                                <div class="demo-name"><?php echo esc_html( lifeline2_set( $demo, 'name' ) ) ?></div>
                                <div data-uri="<?php echo esc_attr( lifeline2_set( $demo, 'uri' ) ) ?>" id="install-demo" class="install-btn"><?php esc_html_e( 'Import Demo', 'lifeline2' ) ?></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
        }
        /* render() */

        /**
         * enqueue styles and/or scripts.
         *
         * @access      public
         * @since       1.0.0
         * @return      void
         */
        public function enqueue() {

            $extension = ReduxFramework_extension_importer::getInstance();
            wp_enqueue_script(
                'redux-field-importer-js', lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/script.js', array( 'jquery', 'redux-js' ), time(), true
            );
            wp_enqueue_style(
                'redux-field-importer-css', lifeline2_URI . 'core/application/panel/redux-extensions/extensions/importer/assets/style.css', time(), 'all'
            );
            if ( lifeline2_set( $this->field, 'full_width' ) === true ) {
                $custom_css = "
                .redux-container-importer > div:nth-child(1){
                        display: none;
                }";
                //wp_add_inline_style( 'redux-field-importer-css', $custom_css );
            }
        }
        /* enqueue() */

        /**
         * Output Function.
         *
         * Used to enqueue to the front-end
         *
         * @access      public
         * @since       1.0.0         
         * @return      void
         */
        public function output() {

            if ( $this->field['enqueue_frontend'] ) {
                
            }
        }
        /* output() */
    }
}
