<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_Core {

    private static $_instance = null;
    public $view = null;

    private function __construct() {
        $this->view = new lifeline2_View();
    }

    static public function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}
