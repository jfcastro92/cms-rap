<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_gallery_listing_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_gallery_listing( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Gallery Listing", 'lifeline2' ),
                "base"     => "lifeline2_gallery_listing_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Listing Style', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose columns to show galleries in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of galleries to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'gallery_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose gallery categories for which galleries you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for galleries listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Categories', 'lifeline2' ),
                        "param_name"  => "show_categories",
                        "value"       => array( 'Enable Categories' => 'true' ),
                        "description" => esc_html__( 'Enable to show categories in gallery listing', 'lifeline2' ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_gallery_listing_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_gallery',
            'order'          => $order,
            'posts_per_page' => $num,
        );
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'gallery_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        ob_start();
        $i                 = 0;
        if ( $cols == 3 ){
            $size = array( 
                        array('width'=>387,'height'=>415),
                        array('width'=>770,'height'=>562),
                        array('width'=>387,'height'=>415),
                        array('width'=>770,'height'=>562),
                        array('width'=>387,'height'=>415), 
                        array('width'=>387,'height'=>415),
                        array('width'=>770,'height'=>562),
                        array('width'=>770,'height'=>562)
            );
        }else{ 
            $size = array( 
                        array('width'=>387,'height'=>415),
                        array('width'=>770,'height'=>562),
                        array('width'=>387,'height'=>415),
                        array('width'=>387,'height'=>415), 
                        array('width'=>770,'height'=>562),
                        array('width'=>770,'height'=>562)
            );
        }
        wp_enqueue_script( array( 'lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize' ) );
        ?>
        <?php if ( have_posts() ): ?>
            <div class="row">
                <div class="gallery-page masonary">
                    <?php while ( have_posts() ):the_post(); ?>
                        <div class="col-md-<?php echo esc_attr( $cols ); ?>">
                            <div class="gallery">
                                <div class="gallery-img">
                                    <a title="<?php the_title(); ?>" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" itemprop="url">
                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $size[$i]['width'], $size[$i]['height'], true)); ?>
                                        <?php else: ?>
                                            <?php the_post_thumbnail('full'); ?>
                                        <?php endif; ?>
                                    </a>
                                </div>

                                    <div class="gallery-detail">
                                        <?php if ( $show_categories == 'true' ): ?>
                                            <ul>
                                                <?php lifeline2_Common::lifeline2_get_post_categories( get_the_ID(), 'gallery_category', '', 'list' ); ?>
                                            </ul>
                                        <?php endif; ?>

                                        <?php echo '<h3><a title="" href="' . esc_url( get_permalink( get_the_ID() ) ) . '" itemprop="url">' . esc_html( get_the_title( get_the_ID() ) ) . '</a></h3>'; ?>

                    </div>

                            </div>
                        </div>
                        <?php $i++; ?>
                        <?php
                        if ( $i == 8 && $cols == 3 ) $i = 0;
                        elseif ( $i == 6 && $cols == 4 ) $i = 0;
                        ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }
}