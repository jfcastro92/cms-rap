<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_custom_service_with_feature_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_custom_service_with_feature($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Custom Services With Feature Image", 'lifeline2'),
                "base" => "lifeline2_custom_service_with_feature_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'services_informations',
                        "heading" => esc_html__("Add Custom Services", 'lifeline2'),
                        "show_settings_on_create" => true,
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__('Service Feature Image', 'lifeline2'),
                                "param_name" => "service_feature_image",
                                "description" => esc_html__('Select the feature image for service', 'lifeline2'),
                            ),
                            array(
                                "type" => "dropdown",
                                "holder" => "div",
                                "class" => "",
                                "heading" => __("Service Icon Type", 'lifeline2'),
                                "param_name" => "service_type",
                                "value" => array('Image' => 'image', 'Icon' => 'icon'),
                                "description" => __("Choose serice icon type", 'lifeline2')
                            ),
                            array(
                                'type' => 'iconpicker',
                                'heading' => esc_html__('Icon', 'lifeline2'),
                                'param_name' => 'icon',
                                'value' => 'fa fa-adjust', // default value to backend editor admin_label
                                'settings' => array(
                                    'emptyIcon' => false,
                                    'iconsPerPage' => 4000,
                                ),
                                'description' => esc_html__('Select icon from library.', 'lifeline2'),
                                'dependency' => array(
                                    'element' => 'service_type',
                                    'value' => array('icon')
                                ),
                            ),
                            array(
                                "type" => "attach_image",
                                "class" => "",
                                "heading" => esc_html__('Service Image', 'lifeline2'),
                                "param_name" => "service_image",
                                "description" => esc_html__('Select the image for service', 'lifeline2'),
                                'dependency' => array(
                                    'element' => 'service_type',
                                    'value' => array('image')
                                ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Title", 'lifeline2'),
                                "param_name" => "service_title",
                                "description" => esc_html__("Enter the service title", 'lifeline2')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Link", 'lifeline2'),
                                "param_name" => "service_link",
                                "description" => esc_html__("Enter the service link", 'lifeline2')
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__("Subtitle", 'lifeline2'),
                                "param_name" => "subtitle",
                                "description" => esc_html__("Enter the subtitle for service", 'lifeline2')
                            ),
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_service_with_feature_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $services_informations = (json_decode(urldecode($services_informations)));
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();

        ob_start();
        ?>
        <?php if (!empty($services_informations)): ?>

            <div class="services-listing style2">
                <div class="row">
                    <?php
                    foreach ($services_informations as $services_information):
                        $service_type = lifeline2_set($services_information, 'service_type');
                        $icon = lifeline2_set($services_information, 'icon');
                        $service_image = lifeline2_set($services_information, 'service_image');
                        $service_title = lifeline2_set($services_information, 'service_title');
                        $service_link = lifeline2_set($services_information, 'service_link');
                        $subtitle = lifeline2_set($services_information, 'subtitle');
                        $service_feature_image = lifeline2_set($services_information, 'service_feature_image');
                        ?> 
                        <div class="col-md-4">
                            <div class="service">
                                <div class="service-img">
                                    <span>
                                        <?php if ($service_type == 'image'): ?>
                                            <?php if (class_exists('lifeline2_Resizer')): ?>
                                                <?php echo wp_get_attachment_image($service_image, 'full'); ?>
                                                <?php
                                            else:
                                                echo wp_get_attachment_image($service_image, 'full');
                                                ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php echo ($icon) ? '<i class="' . $icon . '"></i>' : ''; ?>
                                        <?php endif; ?>
                                    </span>
                                    <?php if (class_exists('lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url($service_feature_image, 'full'), 370, 290, true)); ?>
                                        <?php
                                    else:
                                        echo wp_get_attachment_image($service_image, 'full');
                                        ?>
                                    <?php endif; ?>
                                </div>
                                <div class="service-detail">
                                    <h3><a itemprop="url" href="<?php echo esc_attr($service_link); ?>" title=""> <?php echo esc_html($service_title); ?></a></h3>
                                    <span><?php echo esc_html($subtitle); ?></span>
                                </div>
                            </div><!-- Service Box -->
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><!-- Service Listing -->


        <?php endif; ?>

        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>

        <?php
        return $output;
    }

}
