<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_custom_video_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_custom_video( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Custom Video", 'lifeline2' ),
                "base"     => "lifeline2_custom_video_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Title", 'lifeline2' ),
                        "param_name"  => "title",
                        "description" => esc_html__( "Enter the title for video to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "attach_image",
                        "class"       => "",
                        "heading"     => esc_html__( "Image", 'lifeline2' ),
                        "param_name"  => "image",
                        "description" => esc_html__( "Upload video image to show in this section", 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textarea_raw_html",
                        "class"       => "",
                        "heading"     => esc_html__( "Embed Video Code Here", 'lifeline2' ),
                        "param_name"  => "video",
                        "description" => esc_html__( "Enter the embedded video code to show video in this section", 'lifeline2' ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_custom_video_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        ob_start();
        ?>
        <div class="video style2">
            <?php echo wp_get_attachment_image( $image, 'full' ) ?>
            <a title="" href="javascript:void(0)" class="play" itemprop="url"></a><!-- Play Button -->
            <a title="" href="javascript:void(0)" class="pause" itemprop="url"></a><!-- Pause Button -->
            <?php
            if ( function_exists( 'lifeline2_decrypt' ) ) {
                echo urldecode( lifeline2_decrypt( $video ) );
            }
            ?>
            <div class="video-title">
                <h3><?php echo esc_html( $title ); ?></h3>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_clean();
        ?>
        <?php
        return $output;
    }
}