<?php
if (!defined("lifeline2_DIR")) die('!!!');

class lifeline2_single_event_banner_VC_ShortCode extends lifeline2_VC_ShortCode
{
    static $counter = 0;

    public static function lifeline2_single_event_banner($atts = null)
    {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Single Event Banner", 'lifeline2'),
                "base" => "lifeline2_single_event_banner_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Select Event', 'lifeline2'),
                        "param_name" => "event",
                        "value" => array_flip(lifeline2_Common::lifeline2_posts('lif_event')),
                        "description" => esc_html__('Choose the event to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Button', 'lifeline2'),
                        "param_name" => "more_button",
                        "value" => array('Enable Button' => 'true'),
                        "description" => esc_html__('Enable to show button to redirect the user to your required link', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Label", 'lifeline2'),
                        "param_name" => "label",
                        "description" => esc_html__("Enter the button label", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'more_button',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Button Link Type', 'lifeline2'),
                        "param_name" => "button_type",
                        "value" => array(esc_html__('External Link', 'lifeline2') => 'external', esc_html__('Event Detail Link', 'lifeline2') => 'detail'),
                        "description" => esc_html__('Choose the button link type to redirect the user', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Button Link", 'lifeline2'),
                        "param_name" => "link",
                        "description" => esc_html__("Enter the button link to redirect the user", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'button_type',
                            'value' => array('external')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Overlap', 'lifeline2'),
                        "param_name" => "overlap",
                        "value" => array('Enable Overlap' => 'true'),
                        "description" => esc_html__('Enable to overlap this section to upward', 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_single_event_banner_output($atts = null, $content = null)
    {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';

        $args = array(
            'post_type' => 'lif_event',
            'post_name__in' => array($event),
        );

        query_posts($args);
        static $counter = 1;
        ob_start();
        wp_enqueue_script(array('lifeline2_' . 'downcount'));
        if (have_posts()):
            while (have_posts()):the_post();
                $starDate = get_post_meta(get_the_ID(), 'start_date', true);
                ?>
                <div class="upcoming-eventbar <?php echo ($overlap == 'true') ? 'overlap' : ''; ?>">
                    <div class="event-date"><strong><?php echo esc_html(date('d', get_post_meta(get_the_ID(), 'start_date', true))); ?></strong><?php echo esc_html(date('F, Y', $starDate)) ?></div>
                    <div class="event-inner">
                        <div class="event-intro">
                            <h3><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                        </div>
                        <div class="start-time">
                            <?php $testdate = rand(10,100); ?>
                            <ul class="countdown-row countdown-single<?php echo $testdate; ?>">
                                <li class="countdown-section">
                                    <span class="days countdown-amount">00</span>
                                    <p class="days_ref countdown-period"><?php echo esc_html('days', 'lifeline2'); ?></p>
                                </li>

                                <li class="countdown-section">
                                    <span class="hours countdown-amount">00</span>
                                    <p class="hours_ref countdown-period"><?php echo esc_html('hours', 'lifeline2'); ?></p>
                                </li>

                                <li class="countdown-section">
                                    <span class="minutes countdown-amount">00</span>
                                    <p class="minutes_ref countdown-period"><?php echo esc_html('minutes', 'lifeline2'); ?></p>
                                </li>

                                <li class="countdown-section">
                                    <span class="seconds countdown-amount">00</span>
                                    <p class="seconds_ref countdown-period"><?php echo  esc_html('seconds', 'lifeline2'); ?></p>
                                </li>
                            </ul>
                            <?php
                            if (get_post_meta(get_the_ID(), 'start_date', true)):

                                $jsOutput = "jQuery(document).ready(function ($) {
                                        $('.countdown-single$testdate').downCount({
                                            date: '" . date('m/d/Y H:i:s', get_post_meta(get_the_ID(), 'start_date', true)) . "',
                                            offset: " . get_option('gmt_offset') . "
                                        });
                                    });";
                                wp_add_inline_script('lifeline2_' . 'downcount', $jsOutput);
                            endif;
                            ?>
                        </div>
                        <?php if ($more_button == 'true'): ?>
                            <?php if ($button_type == 'external'): ?>
                                <div class="join-us"><a itemprop="url" class="theme-btn call-popup" href="<?php echo esc_url($link);
                                    ?>" title="<?php the_title(); ?>"><?php echo ($label) ? $label : esc_html__('JOIN US TODAY', 'lifeline2'); ?></a></div>
                            <?php else: ?>
                                <div class="join-us"><a itemprop="url" class="theme-btn call-popup" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><?php echo ($label) ? $label : esc_html__('Read More', 'lifeline2'); ?></a></div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div><!-- Event Bar -->
                <?php
            endwhile;
            wp_reset_query();
        endif;
        $output = ob_get_contents();
        ob_clean();
        $counter++;


        return $output;
    }
}