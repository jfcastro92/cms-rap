<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_team_members_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_team_members( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Team Members", 'lifeline2' ),
                "base"     => "lifeline2_team_members_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Listing Style', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of members to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose team categories for which team you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for causes listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Social Media', 'lifeline2' ),
                        "param_name"  => "show_social_media",
                        "value"       => array( 'Enable Social Media' => 'true' ),
                        "description" => esc_html__( 'Enable to show social media profiles icons for team member', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Social Media Title", 'lifeline2' ),
                        "param_name"  => "social_media_title",
                        "description" => esc_html__( "Enter the title for social media icons section", 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'show_social_media',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Designation', 'lifeline2' ),
                        "param_name"  => "show_designation",
                        "value"       => array( 'Enable Designation' => 'true' ),
                        "description" => esc_html__( 'Enable to show designation for team member', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable carousel for members listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the auto play timeout for members carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for members carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for members listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousel for members listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of members listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of members listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of members listing', 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "carousel",
                            "value"   => array( "true" )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_team_members_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_team',
            'order'          => $order,
            'posts_per_page' => $num,
        );
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'team_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="members">
                <div class="row">
                    <div class="masonary">
                    <?php if ( $carousel == 'true' ) echo '<div class="team-members' . $counter . '">'; ?>
                    <?php while ( have_posts() ):the_post(); ?>
                        <?php
                        $member_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'team' );
                        $social_icons = get_post_meta( get_the_ID(), 'social_media_icons', true );
                        ?>
                   <?php  
                        if ($cols == 3) {
                            $width = 297;
                            $height = 297;
                        } elseif ($cols == 4) {
                            $width = 436;
                            $height = 436;
                        } elseif ($cols == 6) {
                            $width = 597;
                            $height = 597;
                        } 
                    ?>
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="team-member" itemscope itemtype="http://schema.org/Person">
                            <div class="member-img">

                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php the_title(); ?>">
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true)); ?>
                                    </a>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                <?php if ( !empty( $social_icons ) && $show_social_media == 'true' ): ?>
                                    <div class="member-social">
                                        <?php echo ($social_media_title) ? '<span>' . $social_media_title . '</span>' : ''; ?>
                                        <div class="social-links">
                                            <?php foreach ( $social_icons as $s_icon ): ?>
                                                <a itemprop="url" title="<?php echo esc_url( lifeline2_set( $s_icon, 'social_title' ) ); ?>" href="<?php echo esc_url( lifeline2_set( $s_icon, 'social_link' ) ); ?>"><i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $s_icon, 'social_icon' ) ) ) . lifeline2_set( $s_icon, 'social_icon' ); ?>"></i></a>
                                            <?php endforeach; ?>
                                        </div>
                                    </div><!-- Members -->
                                <?php endif; ?>
                            </div><!-- Member Image -->
                            <h4 itemprop="name"><a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                            <?php echo ($show_designation == 'true') ? '<i itemprop="jobTitle">' . lifeline2_set( $member_meta, 'designation' ) . '</i>' : ''; ?>
                        </div><!-- Team Member -->

                        <?php echo ($carousel != 'true') ? '</div>' : ''; ?>

                        <?php
                    endwhile;
                    wp_reset_query();
                    if ( $carousel == 'true' ) echo '</div>';
                    ?>
                </div>
                </div>
            </div>
            <?php
            if ( $carousel != 'true' ) {
                wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
            }
            ?>
            <?php
        endif;
        if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            $jsOutput = 'jQuery(document).ready(function($){$(".team-members' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script( 'lifeline2_' . 'owl-carousel', $jsOutput );
        }
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }
}