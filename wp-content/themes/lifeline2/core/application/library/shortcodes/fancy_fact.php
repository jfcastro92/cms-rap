<?php
if ( !defined( "lifeline2_DIR" ) )
	die( '!!!' );

class lifeline2_fancy_fact_VC_ShortCode extends lifeline2_VC_ShortCode {

	static $counter = 0;

	public static function lifeline2_fancy_fact( $atts = null ) {
		if ( $atts == 'lifeline2_Shortcodes_Map' ) {
			$return = array(
				"name" => esc_html__( "Fun Fact", 'lifeline2' ),
				"base" => "lifeline2_fancy_fact_output",
				"icon" => VC . 'about_blog.png',
				"category" => esc_html__( 'Webinane', 'lifeline2' ),
				'content_element' => true,
				"as_child" => array( 'only' => 'lifeline2_fun_facts_fancy_style_output' ),
                //"show_settings_on_create" => true,
				//"show_settings_on_edit" => true,
				"params" => array(
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Title", 'lifeline2' ),
						"param_name" => "title",
						"description" => esc_html__( "Enter the title to show on this section", 'lifeline2' )
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Fact Value", 'lifeline2' ),
						"param_name" => "value",
						"description" => esc_html__( "Enter the fact value show on this section", 'lifeline2' )
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => esc_html__( 'Grid Type', 'lifeline2' ),
						"param_name" => "grid_type",
						"value" => array( esc_html__( 'Grid 1/3', 'lifeline2' ) => '4', esc_html__( 'Grid 1/4', 'lifeline2' ) => '3' ),
						"description" => esc_html__( "Select grid type to show for fact in this section", 'lifeline2' )
					),
				)
			);

			return $return;
		}
	}

	public static function lifeline2_fancy_fact_output( $atts = null, $content = null ) {

		include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
		ob_start();
		?>
		<div class="col-md-<?php echo ($grid_type == '3') ? 3 : 4; ?> :  column">
			<div class="fancy-counter-box">

				<?php echo ($title) ? '<h5>' . balanceTags( $title ) . '</h5>' : ''; ?>
				<?php echo ($value) ? '<strong class="count">' . balanceTags( $value ) . '</strong>' : ''; ?>

			</div><!-- Counter Box -->
		</div>
		<?php
		$output = ob_get_contents();
		ob_clean();
		?>
		<?php
		return $output;
	}

}
