<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_our_sponsors_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_our_sponsors($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Our Sponsors", 'lifeline2'),
                "base" => "lifeline2_our_sponsors_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of sponsors to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'sponsor_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose sponsors categories for which sponsors you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for sponsors listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Carousel', 'lifeline2'),
                        "param_name" => "carousel",
                        "value" => array(esc_html__('Enable Carousel', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable Carousel for sponsors listing.', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Mobile Items', 'lifeline2'),
                        "param_name" => "mobileitems",
                        "description" => esc_html__('Enter the number of items to display for mobile versions', 'lifeline2'),
                        "default" => "2",
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Tablet Items', 'lifeline2'),
                        "param_name" => "tabletitems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for tablet versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Ipad Items', 'lifeline2'),
                        "param_name" => "ipaditems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for ipad versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Desktop Items', 'lifeline2'),
                        "param_name" => "items",
                        "default" => "4",
                        "description" => esc_html__('Enter the number of items to display for desktop versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('RTL', 'lifeline2'),
                        "param_name" => "rtl",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable RTL to scroll projects from right to left for the carousel of projects listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_our_sponsors_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $settings = lifeline2_get_theme_options();
        $cat = explode(',', $cat);
        if ($cat && lifeline2_set($cat, 0) == 'all'):
            $cat = array();
        else:
            $cat=$cat;
        endif;
        $args = array(
            'post_type' => 'lif_sponsor',
            'order' => $order,
            'posts_per_page' => $num,
        );
        if (!empty($cat))
            $args['tax_query'] = array(array('taxonomy' => 'sponsor_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        static $counter = 1;
        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="sponsors-carousel sponsors-carousel<?php echo esc_attr($counter); ?>">
                <?php if ($carousel != 'true') echo '<div class="row">'; ?>
                <?php while (have_posts()):the_post(); ?>
                    <?php if ($carousel != 'true') echo '<div class="col-md-3">'; ?>
                    <?php $sponsor_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'sponsor'); ?>
                    <div class="sponsor"><a itemprop="url" href="<?php echo esc_url(lifeline2_set($sponsor_meta, 'link')); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('159x51'); ?></a></div>
                        <?php if ($carousel != 'true') echo '</div>'; ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                    <?php if ($carousel != 'true') echo '</div>'; ?>
            </div>

            <?php
        endif;

        if ($carousel == 'true') {
            wp_enqueue_script('lifeline2_' . 'owl-carousel');
            $jsOutput = 'jQuery(document).ready(function( $ ) {
                $(".sponsors-carousel' . $counter . '").owlCarousel({
                    autoplay:true,
                    autoplayTimeout:2500,
                    smartSpeed:2000,
                    loop:true,
                    dots:false,
                    nav:true,
                    margin:10,
                    mouseDrag:true,
                    autoplayHoverPause:true,
                    autoHeight:true,';
            $jsOutput .= ($rtl) ? 'rtl:' . $rtl . ',' : '';
            $jsOutput .= ($items) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
        }
        $init_script = '
            jQuery(document).ready(function($) {
                $(\'.owl-carousel .owl-item\').on(\'mouseenter\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'stop.owl.autoplay\');
                })
                $(\'.owl-carousel .owl-item\').on(\'mouseleave\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'play.owl.autoplay\', [500]);
                });
            });';

        wp_add_inline_script( 'lifeline2_owl-carousel', $init_script );
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }

}
