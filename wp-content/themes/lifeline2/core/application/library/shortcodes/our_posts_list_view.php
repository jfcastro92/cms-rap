<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_our_posts_list_view_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_our_posts_list_view($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Our Posts List View", 'lifeline2'),
                "base" => "lifeline2_our_posts_list_view_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of posts to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose posts categories for which posts you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Description Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the posts description character limit to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for posts listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Author', 'lifeline2'),
                        "param_name" => "show_author",
                        "value" => array('Enable Author' => 'true'),
                        "description" => esc_html__('Enable to show posts author', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Date', 'lifeline2'),
                        "param_name" => "show_date",
                        "value" => array('Enable Date' => 'true'),
                        "description" => esc_html__('Enable to show posts date', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Categories', 'lifeline2'),
                        "param_name" => "show_categories",
                        "value" => array('Enable Categories' => 'true'),
                        "description" => esc_html__('Enable to show posts categories', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Read More Button', 'lifeline2'),
                        "param_name" => "show_button",
                        "value" => array('Enable Read More Button' => 'true'),
                        "description" => esc_html__('Enable to show read more button for post detail page', 'lifeline2'),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Pagination', 'lifeline2'),
                        "param_name" => "show_pagination",
                        "value" => array('Enable Pagination' => 'true'),
                        "description" => esc_html__('Enable pagination to load more posts', 'lifeline2'),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_our_posts_list_view_output($atts = null, $content = null) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();

        ob_start();
        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_defaut_atts_output.php';
        $cat = explode(',', $cat);
        if ($show_pagination == 'true') {
            $args = array(
                'post_type' => 'post',
                'order' => $order,
                'posts_per_page' => $num,
                'paged' => 1,
            );
        } else {
            $args = array(
                'post_type' => 'post',
                'order' => $order,
                'showposts' => $num
            );
        }

        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        ?>
        <?php if (have_posts()): ?>
            <div class="all-posts list-style">
                <?php while (have_posts()):the_post(); ?>
                    <div itemtype="http://schema.org/BlogPosting" itemscope="" class="post-listview">
                        <div class="col-md-6">
                            <div class="post-img">
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                <a title="" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url">
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 400, 259, true)); ?>
                                </a>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                <?php if ($show_button == 'true' || $show_date == 'true'): ?>
                                    <span itemprop="datePublished" content="<?php ($show_date == 'true') ? get_the_date(get_option('date_format', get_the_ID())) : '' ?>">
                                        <?php echo ($show_date == 'true') ? '<i class="fa fa-calendar-o"></i><span>' . get_the_date(get_option('date_format', get_the_ID())) . '</span>' : ''; ?>
                                        <?php echo ($show_button) ? '<a title="" href="' . esc_url(get_permalink(get_the_ID())) . '" itemprop="url">' . esc_html__('Read More', 'lifeline2') . '</a>' : ''; ?>
                                    </span>
                                <?php endif; ?>
                            </div><!-- Post Image -->
                        </div>
                        <div class="col-md-6">
                            <div class="post-detail">
                                <h3 itemprop="headline"><a title="" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url"><?php the_title(); ?></a></h3>
                                <?php //if ($show_tags == 'true' || $show_author == 'true'): ?>
                                    <ul class="meta">
                                        <?php if ($show_author == 'true'): ?>
                                            <li><i class="fa fa-user"></i> <?php esc_html_e('By', 'lifeline2'); ?> <a itemprop="url" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" title="<?php ucwords(the_author_meta('display_name')); ?>"><?php ucwords(the_author_meta('display_name')); ?></a></li>
                                        <?php endif; ?>
                                        <?php if ($show_categories == 'true'): ?>
                                            <li><i class="fa fa-bars"></i> <?php the_category(','); ?></li>
                                            <?php endif; ?>
                                    </ul>
                                <?php //endif; ?>
                                <p itemprop="description"><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_content(get_the_ID()), $limit)); ?></p>
                            </div><!-- Post Detail -->
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
                <?php $data = array('posts_per_page' => $num, 'order' => $order, 'cat' => $cat, 'show_author' => $show_author, 'limit' => $limit, 'show_date' => $show_date, 'show_button' => $show_button); ?>
                <?php echo ($show_pagination == 'true' && function_exists('lifeline2_encrypt')) ? '<a title="' . esc_html__('Load More Posts', 'lifeline2') . '" data-ajax="lifeline2_load_more_posts" data-page="1" href="javascript:void(0);" data-atts="' . lifeline2_encrypt(serialize($data)) . '" class="loadmore" itemprop="url"><i class="ti-reload"></i>' . esc_html__('LOAD MORE', 'lifeline2') . '</a>' : ''; ?>
            </div>
        <?php endif; ?>
        <?php
        if ($section == 'true')
            echo '</section>';

        if ($show_pagination == 'true') {
            $jsOutput = "jQuery(document).ready(function($){
                        $('.all-posts.list-style .loadmore').live('click', function(){
                        var parent = $(this).parent('.all-posts');
                        var page_id = $(this).data('page');
                        var action = $(this).data('ajax');
                        var data_atts = $(this).data('atts');
                        var thiss = this;
                        $.ajax({
                            url: ajaxurl,
                            type: 'json',
                            method: 'POST',
                            data: 'action='+action+'&page_num='+page_id+'&data_atts='+data_atts,
                            beforeSend: function () {
                                $('div.donation-modal-wraper').fadeIn('slow');
                                $('div.donation-modal-preloader').fadeIn('slow');
                            },
                            success: function(res){
                                $('div.donation-modal-wraper').fadeOut('slow');
                                $('div.donation-modal-preloader').fadeOut('slow');
                                var result = $.parseJSON(res);
                                $(thiss).remove();
                                var items = result.posts_list;
                                parent.append(items);
                            },
                        });
                    });
                });";
            wp_add_inline_script('lifeline2_' . 'script', $jsOutput);
        }
        $output .= ob_get_contents();
        ob_clean();
        return $output;
    }

}
