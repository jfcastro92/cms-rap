<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_our_stories_VC_ShortCode extends lifeline2_VC_ShortCode {

    static $counter = 0;

    public static function lifeline2_our_stories($atts = null) {
        if ($atts == 'lifeline2_Shortcodes_Map') {
            $return = array(
                "name" => esc_html__("Our Stories Simple Style", 'lifeline2'),
                "base" => "lifeline2_our_stories_output",
                "icon" => VC . 'about_blog.png',
                "category" => esc_html__('Webinane', 'lifeline2'),
                "params" => array(
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Grids Number', 'lifeline2'),
                        "param_name" => "cols",
                        "value" => array(esc_html__('Two Columns', 'lifeline2') => '6', esc_html__('Three Columns', 'lifeline2') => '4', esc_html__('Four Columns', 'lifeline2') => '3'),
                        "description" => esc_html__('Choose causes grid view columns to show in this section', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Number", 'lifeline2'),
                        "param_name" => "num",
                        "description" => esc_html__("Enter the number of stories to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Select Categories', 'lifeline2'),
                        "param_name" => "cat",
                        "value" => array_flip(lifeline2_Common::lifeline2_get_categories(array('taxonomy' => 'story_category', 'hide_empty' => FALSE, 'show_all' => true), true)),
                        "description" => esc_html__('Choose stories categories for which stories you want to show', 'lifeline2')
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__('Order', 'lifeline2'),
                        "param_name" => "order",
                        "value" => array(esc_html__('Ascending', 'lifeline2') => 'ASC', esc_html__('Descending', 'lifeline2') => 'DESC'),
                        "description" => esc_html__("Select sorting order ascending or descending for stories listing", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Show Location', 'lifeline2'),
                        "param_name" => "show_location",
                        "value" => array('Enable Location' => 'true'),
                        "description" => esc_html__('Enable to show location for stories listing', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Title Character Limit", 'lifeline2'),
                        "param_name" => "limit",
                        "description" => esc_html__("Enter the posts title character limit to show in this section", 'lifeline2')
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Budget Info', 'lifeline2'),
                        "param_name" => "budget",
                        "value" => array('Show Budget Info' => 'true'),
                        "description" => esc_html__('Enable to show project budget info', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__("Budget Info Label", 'lifeline2'),
                        "param_name" => "info_label",
                        "description" => esc_html__("Enter the budget info label", 'lifeline2'),
                        'dependency' => array(
                            'element' => 'budget',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Carousel', 'lifeline2'),
                        "param_name" => "carousel",
                        "value" => array(esc_html__('Enable Carousel', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable Carousel for stories listing.', 'lifeline2'),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Auto Play Timeout', 'lifeline2'),
                        "param_name" => "autoplaytimeout",
                        "description" => esc_html__('Enter the auto play timeout for stories carousel', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Smart Speed', 'lifeline2'),
                        "param_name" => "smartspeed",
                        "description" => esc_html__('Enter the smart speed time for stories carousel', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Margin', 'lifeline2'),
                        "param_name" => "margin",
                        "description" => esc_html__('Enter the margin for stories listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Mobile Items', 'lifeline2'),
                        "param_name" => "mobileitems",
                        "description" => esc_html__('Enter the number of items to display for mobile versions', 'lifeline2'),
                        "default" => "2",
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Tablet Items', 'lifeline2'),
                        "param_name" => "tabletitems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for tablet versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Ipad Items', 'lifeline2'),
                        "param_name" => "ipaditems",
                        "default" => "3",
                        "description" => esc_html__('Enter the number of items to display for ipad versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Desktop Items', 'lifeline2'),
                        "param_name" => "items",
                        "default" => "4",
                        "description" => esc_html__('Enter the number of items to display for desktop versions', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Auto Play', 'lifeline2'),
                        "param_name" => "autoplay",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable to auto play the carousel for stories listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Loop', 'lifeline2'),
                        "param_name" => "loop",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable circular loop for the carousel of stories listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Dots Navigation', 'lifeline2'),
                        "param_name" => "dots",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable dots navigation for the carousel of stories listing', 'lifeline2'),
                        'dependency' => array(
                            'element' => 'carousel',
                            'value' => array('true')
                        ),
                    ),
                    array(
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_html__('Arrows Navigation', 'lifeline2'),
                        "param_name" => "nav",
                        "value" => array(esc_html__('Enable', 'lifeline2') => 'true'),
                        "description" => esc_html__('Enable arrows navigation for the carousel of stories listing', 'lifeline2'),
                        "dependency" => array(
                            "element" => "carousel",
                            "value" => array("true")
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_our_stories_output($atts = null, $content = null) {
        $settings = lifeline2_get_theme_options();
        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if (class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $cat = explode(',', $cat);
        $args = array(
            'post_type' => 'lif_story',
            'order' => $order,
            'posts_per_page' => $num,
        );
        if (!empty($cat) && lifeline2_set($cat, 0) == 'all') {
            array_shift($cat);
        }
        if (!empty($cat) && lifeline2_set($cat, 0) != '')
            $args['tax_query'] = array(array('taxonomy' => 'story_category', 'field' => 'slug', 'terms' => (array) $cat));
        query_posts($args);
        $autoplayHoverPause = true;
        static $counter = 1;

        ob_start();
        ?>
        <?php if (have_posts()): ?>
            <div class="successful-stories">
                <div class="row">
                    <div class="masonary">
                    <?php if ($carousel == 'true') echo '<div class="stories-view' . $counter . '">'; ?>
                    <?php
                    while (have_posts()):the_post();
                        $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'story'); //printr($meta);
                        $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                        $donationNeededUsd = (int) (lifeline2_set($meta, 'project_cost')) ? lifeline2_set($meta, 'project_cost') : 0;
                        $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                        if ($cuurency_formate == 'select'):
                            $donation_needed = $donationNeededUsd;

                            //printr($donation_needed);
                        else:
                            $donation_needed = ($donationNeededUsd != 0) ?  lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                        endif;
                        if ($cols == 4) {
                            $width = 370;
                            $height = 303;
                        } else {
                            $width = 770;
                            $height = 562;
                        }
                        ?>

                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="story">
                            <div class="story-img">
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $width, $height, true)); ?>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                                <a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" title="<?php the_title(); ?>">+</a>
                            </div>
                            <div class="story-detail">
                                <?php echo ($show_location) ? '<span><i class="fa fa-map-marker"></i>' . esc_html(lifeline2_set($meta, 'location')) . '</span>' : ''; ?>
                                <h3><a itemprop="url" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_contents(get_the_title(get_the_ID()), $limit)) ?></a></h3>
                            </div>
                            <?php if ($budget): ?>
                                <div class="spent-bar">
                                    <span><?php echo ($info_label) ? $info_label : esc_html__('Money Spent', 'lifeline2') ?></span>
                                    <span class="price"><i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_needed, 0)) ?></span>
                                </div>
                            <?php endif; ?>
                        </div><!-- Story -->
                        <?php echo ($carousel != 'true') ? '</div>' : ''; ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                    <?php if ($carousel == 'true') echo '</div>'; ?>
                </div>
            </div>
            </div>
            <?php
        if($carousel != true){
            wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
        }
        endif;
        if ($carousel == 'true') {
            wp_enqueue_script('lifeline2_' . 'owl-carousel');
            $jsOutput = 'jQuery(document).ready(function($){$(".stories-view' . $counter . '").owlCarousel({';
            $jsOutput .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $jsOutput .= ($autoplayHoverPause) ? 'autoplayHoverPause:' . $autoplayHoverPause . ',' : '';
            $jsOutput .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $jsOutput .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $jsOutput .= ($loop) ? 'loop:' . $loop . ',' : '';
            $jsOutput .= ($dots) ? 'dots:' . $dots . ',' : '';
            $jsOutput .= ($nav) ? 'nav:' . $nav . ',' : '';
            $jsOutput .= ($margin) ? 'margin:' . $margin . ',' : '';
            $jsOutput .= ($margin) ? 'items:' . $items . ',' : '';
            $jsOutput .='responsive : {0 : {items : 1},';
            $jsOutput .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $jsOutput .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $jsOutput .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $jsOutput .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $jsOutput .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $jsOutput);
        }
        $init_script = '
            jQuery(document).ready(function($) {
                $(\'.owl-carousel .owl-item\').on(\'mouseenter\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'stop.owl.autoplay\');
                })
                $(\'.owl-carousel .owl-item\').on(\'mouseleave\', function (e) {
                    $(this).closest(\'.owl-carousel\').trigger(\'play.owl.autoplay\', [500]);
                });
            });';

        wp_add_inline_script( 'lifeline2_owl-carousel', $init_script );
        $output = ob_get_contents();
        ob_clean();
        $counter++;
        return $output;
    }

}
