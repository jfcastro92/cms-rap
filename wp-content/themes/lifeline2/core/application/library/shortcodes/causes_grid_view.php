<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_causes_grid_view_VC_ShortCode extends lifeline2_VC_ShortCode {
    static $counter = 0;

    public static function lifeline2_causes_grid_view( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Causes Grid View", 'lifeline2' ),
                "base"     => "lifeline2_causes_grid_view_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Grid View Listing Style', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose causes grid view columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of causes to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'causes_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose causes categories for which causes you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for causes listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Show Categories', 'lifeline2' ),
                        "param_name"  => "show_cats",
                        "value"       => array( 'Enable Categories' => 'true' ),
                        "description" => esc_html__( 'Enable to show causes categories', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Donation', 'lifeline2' ),
                        "param_name"  => "donation",
                        "value"       => array( 'Enable Donation' => 'true' ),
                        "description" => esc_html__( 'Enable to show donation for causes listing', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Carousel', 'lifeline2' ),
                        "param_name"  => "carousel",
                        "value"       => array( esc_html__( 'Enable Carousel', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable Carousal for causes listing.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play Timeout', 'lifeline2' ),
                        "param_name"  => "autoplaytimeout",
                        "description" => esc_html__( 'Enter the autoplay timeout for causes carousal', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Smart Speed', 'lifeline2' ),
                        "param_name"  => "smartspeed",
                        "description" => esc_html__( 'Enter the smart speed time for causes carousel', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Margin', 'lifeline2' ),
                        "param_name"  => "margin",
                        "description" => esc_html__( 'Enter the margin for causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Mobile Items', 'lifeline2' ),
                        "param_name"  => "mobileitems",
                        "description" => esc_html__( 'Enter the number of items to display for mobile versions', 'lifeline2' ),
                        "default"     => "2",
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Tablet Items', 'lifeline2' ),
                        "param_name"  => "tabletitems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for tablet versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Ipad Items', 'lifeline2' ),
                        "param_name"  => "ipaditems",
                        "default"     => "3",
                        "description" => esc_html__( 'Enter the number of items to display for ipad versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Desktop Items', 'lifeline2' ),
                        "param_name"  => "items",
                        "default"     => "4",
                        "description" => esc_html__( 'Enter the number of items to display for desktop versions', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Auto Play', 'lifeline2' ),
                        "param_name"  => "autoplay",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable to auto play the carousal for causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Loop', 'lifeline2' ),
                        "param_name"  => "loop",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable circular loop for the carousel of causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Dots Navigation', 'lifeline2' ),
                        "param_name"  => "dots",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable dots navigation for the carousel of causes listing', 'lifeline2' ),
                        'dependency'  => array(
                            'element' => 'carousel',
                            'value'   => array( 'true' )
                        ),
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Arrows Navigation', 'lifeline2' ),
                        "param_name"  => "nav",
                        "value"       => array( esc_html__( 'Enable', 'lifeline2' ) => 'true' ),
                        "description" => esc_html__( 'Enable arrows navigation for the carousel of causes listing', 'lifeline2' ),
                        "dependency"  => array(
                            "element" => "carousel",
                            "value"   => array( "true" )
                        ),
                    ),
                )
            );

            return $return;
        }
    }

    public static function lifeline2_causes_grid_view_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        $default = get_option('wp_donation_basic_settings', TRUE);
        $options = lifeline2_set($default, 'wp_donation_basic_settings');
        $opt  = lifeline2_get_theme_options();

        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_causes',
            'order'          => $order,
            'posts_per_page' => $num,
            );
       // printr($args);
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'causes_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="help-needed">
               <div class="row">
               <?php if ( $carousel !== 'true' ) echo '<div class="masonary">'; ?>
                    <?php if ( $carousel == 'true' ) echo '<div class="causes-grid-view' . $counter . '">'; ?>
                    <?php
                    while ( have_posts() ):the_post();
                        ?>
                        <?php echo ($carousel != 'true') ? '<div class="col-md-' . $cols . '">' : ''; ?>
                        <div class="help">
                            <div class="help-img">
                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                <?php else: ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                            </div>
                            <div class="help-detail">
                                <?php if ( $show_cats == 'true' ): ?>
                                    <div class="cats">
                                        <?php lifeline2_Common::lifeline2_get_post_categories( get_the_ID(), 'causes_category', ',' ); ?>
                                    </div>
                                <?php endif; ?>
                                <h3><a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php echo esc_html( get_the_title( get_the_ID() ) ); ?>"><?php echo esc_html( get_the_title( get_the_ID() ) ); ?></a></h3>
                                <?php if ( $donation == 'true' ): ?>
                                    <?php if(lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1):?>
                                    <?php
                                    if ( lifeline2_set( $opt, 'donation_template_type_general' ) == 'donation_page_template' ):
                                        $url         = get_page_link( lifeline2_set( $opt, 'donation_button_pageGeneral' ) );
                                        $queryParams = array( 'data_donation' => 'causes', 'postId' => get_the_id() );
                                        ?>
                                        <a class="donate-btn" itemprop="url" href="<?php echo esc_url( add_query_arg( $queryParams, $url ) ); ?>" title="">
                                            <span><img itemprop="image" src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/resource/donate-icon.png' ); ?>" alt="" /></span>
                                            <i><?php esc_html_e( 'Donate Now', 'lifeline2' ) ?></i>
                                        </a>
                                        <?php
                                    elseif ( lifeline2_set( $opt, 'donation_template_type_general' ) == 'external_link' ):
                                        $url = lifeline2_set( $opt, 'donation_button_linkGeneral' );
                                        ?>
                                        <a class="donate-btn" itemprop="url" href="<?php echo esc_url( $url ) ?>" target="_blank" title="">
                                            <span><img itemprop="image" src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/resource/donate-icon.png' ); ?>" alt="" /></span>
                                            <i><?php esc_html_e( 'Donate Now', 'lifeline2' ) ?></i>
                                        </a>
                                        <?php
                                    else:
                                        ?>

                                        <a data-modal="general" data-donation="causes" data-post="<?php echo esc_attr( get_the_id() ) ?>" itemprop="url" class="donate-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                            <?php endif; ?>
                                            <span><img itemprop="image" src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/resource/donate-icon.png' ); ?>" alt="" /></span>
                                            <i><?php esc_html_e( 'Donate Now', 'lifeline2' ) ?></i>
                                        </a>
                                    <?php
                                    endif;
                                    ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php echo ($carousel != 'true') ? '</div>' : ''; ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                    <?php if ( $carousel == 'true' ) echo '</div>'; ?>
                <?php if ( $carousel != 'true' ) echo '</div>'; ?>
               </div>
            </div>
            <?php
            if ( $carousel != 'true' ) {
                wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));
            }
            ?>
            <?php
        endif;
        if ( $carousel == 'true' ) {
            wp_enqueue_script( 'lifeline2_' . 'owl-carousel' );
            //ob_start();
            $output = ' jQuery(document).ready(function($){$(".causes-grid-view' . $counter . '").owlCarousel({';
            $output .= ($autoplay) ? 'autoplay:' . $autoplay . ',' : '';
            $output .= ($autoplaytimeout) ? 'autoplayTimeout:' . $autoplaytimeout . ',' : '';
            $output .= ($smartspeed) ? 'smartSpeed:' . $smartspeed . ',' : '';
            $output .= ($loop) ? 'loop:' . $loop . ',' : '';
            $output .= ($dots) ? 'dots:' . $dots . ',' : '';
            $output .= ($nav) ? 'nav:' . $nav . ',' : '';
            $output .= ($margin) ? 'margin:' . $margin . ',' : '';
            $output .= ($margin) ? 'items:' . $items . ',' : '';
            $output .='responsive : {0 : {items : 1},';
            $output .= ($mobileitems) ? '480 : {items :' . $mobileitems . '},' : '';
            $output .= ($tabletitems) ? '768 : {items : ' . $tabletitems . '},' : '';
            $output .= ($ipaditems) ? '980 : {items : ' . $ipaditems . '},' : '';
            $output .= ($items) ? '1200 : {items : ' . $items . '},' : '';
            $output .='}});});';
            wp_add_inline_script('lifeline2_' . 'owl-carousel', $output);
        }

        $Shortcodeoutput = ob_get_contents();
        ob_clean();
        ?>
        <?php
        $counter++;
        return $Shortcodeoutput;
    }
}