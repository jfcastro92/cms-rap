<?php
if ( !defined( "lifeline2_DIR" ) )
	die( '!!!' );

class lifeline2_custom_banner_with_contents_VC_ShortCode extends lifeline2_VC_ShortCode {

	static $counter = 0;

	public static function lifeline2_custom_banner_with_contents( $atts = null ) {
		if ( $atts == 'lifeline2_Shortcodes_Map' ) {
			$return = array(
				"name" => esc_html__( "Custom Banner With Contents", 'lifeline2' ),
				"base" => "lifeline2_custom_banner_with_contents_output",
				"icon" => VC . 'about_blog.png',
				"category" => esc_html__( 'Webinane', 'lifeline2' ),
				"params" => array(
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Title", 'lifeline2' ),
						"param_name" => "title",
						"description" => esc_html__( "Enter the title to show on banner", 'lifeline2' )
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Sub Title", 'lifeline2' ),
						"param_name" => "sub_title",
						"description" => esc_html__( "Enter the sub title to show on banner", 'lifeline2' )
					),
					array(
						"type" => "textarea",
						"class" => "",
						"heading" => esc_html__( "Description", 'lifeline2' ),
						"param_name" => "description",
						"description" => esc_html__( "Enter the description to show banner", 'lifeline2' )
					),
					array(
						"type" => "attach_image",
						"class" => "",
						"heading" => esc_html__( "Banner Image", 'lifeline2' ),
						"param_name" => "image",
						"description" => esc_html__( "Upload banner image to show in this section", 'lifeline2' ),
					),
					array(
						"type" => "checkbox",
						"class" => "",
						"heading" => esc_html__( 'Show Button', 'lifeline2' ),
						"param_name" => "more_button",
						"value" => array( 'Enable Button' => 'true' ),
						"description" => esc_html__( 'Enable to show button to redirect the user to your required link', 'lifeline2' ),
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Button Label", 'lifeline2' ),
						"param_name" => "label",
						"description" => esc_html__( "Enter the button label", 'lifeline2' ),
						'dependency' => array(
							'element' => 'more_button',
							'value' => array( 'true' )
						),
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__( "Button Link", 'lifeline2' ),
						"param_name" => "link",
						"description" => esc_html__( "Enter the button link to redirect the user", 'lifeline2' ),
						'dependency' => array(
							'element' => 'more_button',
							'value' => array( 'true' )
						),
					),
				)
			);

			return $return;
		}
	}

	public static function lifeline2_custom_banner_with_contents_output( $atts = null, $content = null ) {

		include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
		ob_start();
		?>
		<div class="volunteer">
			<?php echo wp_get_attachment_image( $image, 'full' ); ?>
			<div class="volunteer-overlay">
				<div class="volunteer-inner">
					<?php echo ($sub_title) ? '<span>' . $sub_title . '</span>' : ''; ?>
					<?php echo ($title) ? '<strong>' . $title . '</strong>' : ''; ?>
					<?php echo ($description) ? '<p>' . $description . '</p>' : ''; ?>
					<?php echo ($more_button == 'true') ? '<a itemprop="url" class="theme-btn" href="' . esc_url( $link ) . '" title="' . esc_attr( $label ) . '">' . esc_html( $label ) . '</a>' : ''; ?>
				</div>
			</div>
		</div><!-- Become A Volunteer -->
		<?php
		$output = ob_get_contents();
		ob_clean();
		?>
		<?php
		return $output;
	}

}
