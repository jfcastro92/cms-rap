<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_services_new_style_VC_ShortCode extends lifeline2_VC_ShortCode {

    public static function lifeline2_services_new_style( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            $return = array(
                "name"     => esc_html__( "Services New Style", 'lifeline2' ),
                "base"     => "lifeline2_services_new_style_output",
                "icon"     => VC . 'about_blog.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Number of Columns', 'lifeline2' ),
                        "param_name"  => "cols",
                        "value"       => array( esc_html__( 'Two Columns', 'lifeline2' ) => '6', esc_html__( 'Three Columns', 'lifeline2' ) => '4', esc_html__( 'Four Columns', 'lifeline2' ) => '3' ),
                        "description" => esc_html__( 'Choose services grid view columns to show in this section', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Number", 'lifeline2' ),
                        "param_name"  => "num",
                        "description" => esc_html__( "Enter the number of services to show in this section", 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'service_category', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose services categories for which services you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for services listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Description Word Limit", 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( "Enter the services description word limit to show in this section", 'lifeline2' ),
                    ),
                    array(
	                    "type"        => "checkbox",
	                    "class"       => "",
	                    "heading"     => esc_html__( 'Custom Button', 'lifeline2' ),
	                    "param_name"  => "custom_button",
	                    "value"       => array( esc_html__( 'Enable Custom Button', 'lifeline2' ) => 'true' ),
	                    "description" => esc_html__( 'Enable custom button.', 'lifeline2' ),
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( "Button Text", 'lifeline2' ),
                        "param_name"  => "buttontext",
                        "description" => esc_html__( 'Enter the text for button.', 'lifeline2' ),
                        'dependency'  => array(
	                        'element' => 'custom_button',
	                        'value'   => array( 'true' )
                        ),
                    ),
                    array(
	                    "type"        => "textfield",
	                    "class"       => "",
	                    "heading"     => esc_html__( "Button URL", 'lifeline2' ),
	                    "param_name"  => "buttonurl",
	                    "description" => esc_html__( 'Enter the url for button.', 'lifeline2' ),
	                    'dependency'  => array(
		                    'element' => 'custom_button',
		                    'value'   => array( 'true' )
	                    ),
                    ),

                    ),
            );

            return $return;
        }
    }

    public static function lifeline2_services_new_style_output( $atts = null, $content = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type'      => 'lif_service',
            'order'          => $order,
            'posts_per_page' => $num,
        );
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'service_category', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        query_posts( $args );
        static $counter    = 1;
        ob_start();
        ?>
        <?php if ( have_posts() ): ?>
            <div class="container-fluid">
                <div class="row merged">
                    <?php while ( have_posts() ):the_post(); ?>
                        <?php $meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' ); ?>
                        <?php echo '<div class="col-sm-' . $cols . '">'; ?>
                    <div class="services-box">
                        <i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $meta, 'serive_icon' ) ) ) . esc_attr( lifeline2_set( $meta, 'serive_icon' ) ) ?>"></i>
                        <div class="services-meta">
                            <h3><?php the_title(); ?></h3>
                            <span></span>
                            <i><?php the_title(); ?></i>
                            <?php echo ($limit) ? '<p>' . esc_html( wp_trim_words(get_the_content( get_the_ID() ), $limit, '' ) ). '...&nbsp;</p>' : ''; ?>
                            <?php if($custom_button == true) {
                                echo '<a href="' . $buttonurl . '" title="" class="button-radius"><span>'.$buttontext.'</span></a>';
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    echo '</div>';
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>


            <?php
        endif;
        $output = ob_get_contents();
        ob_clean();
        return $output;
    }
}