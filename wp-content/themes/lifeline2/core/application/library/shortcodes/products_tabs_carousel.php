<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_Products_Tabs_Carousel_VC_ShortCode extends lifeline2_VC_ShortCode {

    public static function lifeline2_products_tabs_carousel( $atts = null ) {
        if ( $atts == 'lifeline2_Shortcodes_Map' ) {
            return array(
                "name"     => esc_html__( "Products Tabs Carousel", 'lifeline2' ),
                "base"     => "lifeline2_products_tabs_carousel_output",
                "icon"     => lifeline2_URI . 'core/duffers_panel/panel/public/img/vc-icons/Product-Listing-With-Tabs-Filter.png',
                "category" => esc_html__( 'Webinane', 'lifeline2' ),
                "params"   => array(
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Number', 'lifeline2' ),
                        "param_name"  => "number",
                        "description" => esc_html__( 'Enter number of products to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "checkbox",
                        "class"       => "",
                        "heading"     => esc_html__( 'Select Categories', 'lifeline2' ),
                        "param_name"  => "cat",
                        "value"       => array_flip( lifeline2_Common::lifeline2_get_categories( array( 'taxonomy' => 'product_cat', 'hide_empty' => FALSE, 'show_all' => true ), true ) ),
                        "description" => esc_html__( 'Choose product categories for which product you want to show', 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Description Limit', 'lifeline2' ),
                        "param_name"  => "limit",
                        "description" => esc_html__( 'Enter the product description limit', 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( 'Order', 'lifeline2' ),
                        "param_name"  => "order",
                        "value"       => array( esc_html__( 'Ascending', 'lifeline2' ) => 'ASC', esc_html__( 'Descending', 'lifeline2' ) => 'DESC' ),
                        "description" => esc_html__( "Select sorting order ascending or descending for product listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "dropdown",
                        "class"       => "",
                        "heading"     => esc_html__( "Order By", 'lifeline2' ),
                        "param_name"  => "orderby",
                        "value"       => array_flip( array( 'best_seller' => esc_html__( 'Best Seller', 'lifeline2' ), 'top_rated' => esc_html__( 'Top Rated', 'lifeline2' ), 'by_price' => esc_html__( 'By Price', 'lifeline2' ), 'onsale' => esc_html__( 'On Sale', 'lifeline2' ), 'featued' => esc_html__( 'Featured', 'lifeline2' ), 'popular' => esc_html__( 'Popular', 'lifeline2' ), 'date' => esc_html__( 'Date', 'lifeline2' ), 'name' => esc_html__( 'Title', 'lifeline2' ), 'ID' => esc_html__( 'ID', 'lifeline2' ), 'rand' => esc_html__( 'Random', 'lifeline2' ) ) ),
                        "description" => esc_html__( "Select order by method for product listing", 'lifeline2' )
                    ),
                    array(
                        "type"        => "textfield",
                        "class"       => "",
                        "heading"     => esc_html__( 'Button Label', 'lifeline2' ),
                        "param_name"  => "label",
                        "description" => esc_html__( 'Enter add to cart button label', 'lifeline2' )
                    ),
                )
            );
        }
    }

    public static function lifeline2_products_tabs_carousel_output( $atts, $contents = null ) {

        include lifeline2_ROOT . 'core/application/library/shortcodes/shortcode_atts.php';
        if(class_exists('lifeline2_Resizer'))
            $img_obj = new lifeline2_Resizer();
        ob_start();
        global $woocommerce;
        $cat  = explode( ',', $cat );
        $args = array(
            'post_type' => 'product',
            'order'     => $order,
            'showposts' => $number
        );

        if ( $orderby == 'top_rated' ) {
            add_filter( 'posts_clauses', array( $woocommerce->query, 'order_by_rating_post_clauses' ) );
        } else {
            $args = array_merge( $args, lifeline2_Common::lifeline2_product_orderby( $orderby ) );
        }

        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) == 'all' ) {
            array_shift( $cat );
        }
        if ( !empty( $cat ) && lifeline2_set( $cat, 0 ) != '' ) $args['tax_query'] = array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => ( array ) $cat ) );
        $output            = '';

        query_posts( $args );
        $limit = ($limit != '') ? $limit : 150;
        if ( have_posts() ):
            ?>
            <div class="special-deals">
                <div class="row">
                    <div class="col-md-10 column">
                        <div class="deal-detail-carousel">
                            <?php while ( have_posts() ):the_post(); ?>
                                <?php global $product; ?>
                                <div class="deal-detail" itemscope itemtype="http://schema.org/Product">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 570, 644, true)); ?>
                                            <?php else: ?>
                                                <?php the_post_thumbnail('full'); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="deal-desc">
                                                <ul>
                                                    <?php //lifeline2_Common::lifeline2_get_post_categories( get_the_ID(), 'product_cat', '', 'list' );  ?>
                                                </ul>
                                                <h4 itemprop="name"><a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                                <span itemtype="http://schema.org/Offer" itemscope itemprop="offers"><?php echo wp_kses( $product->get_price_html(), true ); ?></span>
                                                <p itemprop="description"><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content(), $limit ) ) ?></p>
                                                <?php
                                                $add_to_cart = '';
                                                if ( $product->is_purchasable() && $product->is_in_stock() ) {
                                                    if ( get_option( 'woocommerce_enable_ajax_add_to_cart' ) == 'yes' ) {
                                                        $add_to_cart = 'add_to_cart_button ajax_add_to_cart';
                                                    } else {
                                                        $add_to_cart = 'add_to_cart_button';
                                                    }
                                                }
                                                echo ($label) ? apply_filters( 'woocommerce_loop_add_to_cart_link', sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="theme-btn %s product_type_%s">' . $label . '</a>', esc_url( $product->add_to_cart_url() ), esc_attr( $product->get_id() ), esc_attr( $product->get_sku() ), $add_to_cart, esc_attr( $product->get_type() ), esc_html( $product->add_to_cart_text() )
                                                        ), $product ) : '';
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Deal Detail -->
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="col-md-2 column">
                        <div class="deal-selector">
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="deal">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 270, 270, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                    
                                    <h5>
                                        <a itemprop="url" href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" title="<?php the_title(); ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_title( get_the_ID() ), 15 ) ); ?></a>
                                    </h5>
                                </div><!-- Deal -->
                                <?php
                            endwhile;
                            wp_reset_query();
                            ?>
                        </div><!-- Deal Selector -->
                    </div>
                </div>
            </div>
            <?php
        endif;
        wp_enqueue_script( 'lifeline2_' . 'slick' );
        $jsOutput = "jQuery(document).ready(function ($) {
                $('.deal-selector').slick({
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    vertical: true,
                    autoplay: false,
                    arrows: false,
                    focusOnSelect: true,
                    draggable: false,
                    asNavFor: '.deal-detail-carousel'
                });
                $('.deal-detail-carousel').slick({
                    infinite: true,
                    autoplay: true,
                    pauseOnHover:true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    asNavFor: '.deal-selector'
                });
            });";
        wp_add_inline_script( 'lifeline2_' . 'slick', $jsOutput );
        $output   = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}