<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_Custom_Video_Banner_Widget extends WP_Widget {

    function __construct() {
        parent::__construct( /* Base ID */'lifeline2-custom-video-banner', /* Name */ esc_html__( 'Video Banner', 'lifeline2' ), array( 'description' => esc_html__( 'This widget is used to show video banner', 'lifeline2' ) ) );
    }

    function widget( $args, $instance ) {
        extract( $args );
        $title = apply_filters(
            'widget_title', (lifeline2_set( $instance, 'title' ) == '') ? '' : lifeline2_set( $instance, 'title' ), array( 'subtitle' => lifeline2_set( $instance, 'banner_custom_title' ) ), $this->id_base
        );
        echo wp_kses( $before_widget, true );

        $noTitle = '';
        if ( $title && lifeline2_set( $instance, 'title' ) != '' ) {
            echo wp_kses( $before_title, true ) . html_entity_decode( $title ) . wp_kses( $after_title, true );
        } else {
            $noTitle = 'no-title';
        }
        wp_enqueue_script( array( 'lifeline2_' . 'jquery-poptrox' ) );
        ?>
        <div class="video-widget">
            <?php echo '<img src="' . esc_url( lifeline2_set( $instance, 'banner' ) ) . '" alt="" />'; ?>
            <div class="video-layer lightbox">
                <?php if ( lifeline2_set( $instance, 'video_link' ) ): ?>
                    <a itemprop="url"  href="<?php echo esc_url( lifeline2_set( $instance, 'video_link' ) ); ?>" title="">
                        <img itemprop="image" src="<?php echo esc_url( lifeline2_URI . 'assets/images/video-icon.png' ); ?>" alt="" />
                    </a>
                <?php endif; ?>
                <?php echo (lifeline2_set( $instance, 'banner_custom_title' )) ? '<h6>' . lifeline2_set( $instance, 'vtitle' ) . '</h6>' : ''; ?>
            </div>
        </div>
        <?php
        if ( lifeline2_set( $instance, 'video_link' ) ):
            $jsOutput = "jQuery(document).ready(function () {
                    var foo = jQuery('.video-layer.lightbox');
                    foo.poptrox({
                        usePopupCaption: false
                    });
                });";
            wp_add_inline_script( 'lifeline2_' . 'jquery-poptrox', $jsOutput );
        endif;
        echo wp_kses( $after_widget, true );
    }

    function update( $new_instance, $old_instance ) {
        $instance                        = $old_instance;
        $instance['title']               = lifeline2_set( $new_instance, 'title' );
        $instance['banner_custom_title'] = lifeline2_set( $new_instance, 'banner_custom_title' );
        $instance['banner']              = lifeline2_set( $new_instance, 'banner' );
        $instance['video_link']          = lifeline2_set( $new_instance, 'video_link' );
        $instance['vtitle']              = lifeline2_set( $new_instance, 'vtitle' );

        return $instance;
    }

    function form( $instance ) {
        //printr($instance);
        $title               = ($instance) ? lifeline2_set( $instance, 'title' ) : '';
        $banner_custom_title = ($instance) ? lifeline2_set( $instance, 'banner_custom_title' ) : '';
        $banner              = (lifeline2_set( $instance, 'banner' )) ? lifeline2_set( $instance, 'banner' ) : get_template_directory_uri() . '/assets/images/no-image.jpg';
        $video_link          = ($instance) ? lifeline2_set( $instance, 'video_link' ) : '';
        $vtitle              = ($instance) ? lifeline2_set( $instance, 'vtitle' ) : '';
        //wp_enqueue_style( 'thickbox' );
        //wp_enqueue_script( 'media-upload' );
        //wp_enqueue_script( 'thickbox' );
        wp_enqueue_media();
        ?>
        <p>    
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr( $this->get_field_id( 'banner_custom_title' ) ); ?>"><?php esc_html_e( 'Banner Custom Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'banner_custom_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'banner_custom_title' ) ); ?>" type="text" value="<?php echo esc_attr( $banner_custom_title ); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr( $this->get_field_id( 'video_link' ) ); ?>"><?php esc_html_e( 'Video URL:', 'lifeline2' ); ?></label>
            <input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'video_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'video_link' ) ); ?>" value="<?php echo esc_attr( $video_link ); ?>" />
        </p>
        <p>    
            <label for="<?php echo esc_attr( $this->get_field_id( 'vtitle' ) ); ?>"><?php esc_html_e( 'Video Title:', 'lifeline2' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'vtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'vtitle' ) ); ?>" type="text" value="<?php echo esc_attr( $vtitle ); ?>" /> 
        </p>
        <div style=" width: 100%; min-height:100px; float: left;margin-bottom: 10px; margin-top: 10px;">

            <label style="margin-bottom:10px; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>"><?php esc_html_e( 'Banner Image:', 'lifeline2' ); ?></label>

            <div class="img-box">
                <img width="100" height="100" style="float: right;" class="banner-image" src="<?php echo esc_url( $banner ); ?>">
                <span data-bind="<?php echo get_template_directory_uri() . '/assets/images/no-image.jpg'; ?>"><?php esc_html_e( 'Remove(x)', 'lifeline2' ); ?></span>
            </div>  
            <input type="hidden" class="img" name="<?php echo esc_attr( $this->get_field_name( 'banner' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>" value="<?php echo esc_attr( $banner ); ?>" />
            <input type="button" class="select-img button button-primary " value="<?php esc_attr_e( 'Select Image', 'lifeline2' ); ?>" />
        </div>
        <style type="text/css">
            .img-box {
                float: right;
                height: 100px;
                position: relative;
                width: 100px;
            }
            .img-box:hover > span{
                display: block;
            }
            .img-box > span{
                display: none;
                background: #eee none repeat scroll 0 0;
                padding-bottom: 3px;
                padding-top: 3px;
                position: absolute;
                text-align: center;
                width: 100%;
                cursor: pointer;
            }
        </style>
        <?php
    }
}