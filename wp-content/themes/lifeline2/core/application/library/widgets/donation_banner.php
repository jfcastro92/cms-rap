<?php
if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_Donation_Banner_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(/* Base ID */'lifeline2-donation-banner', /* Name */ esc_html__('Donation Banner', 'lifeline2'), array('description' => esc_html__('This widget is used to show banner with donation button', 'lifeline2')));
    }

    function widget($args, $instance) {

        extract($args);
        $opt = lifeline2_get_theme_options();

        $title = apply_filters(
                'widget_title', (lifeline2_set($instance, 'titles') == '') ? '' : lifeline2_set($instance, 'titles'), array('subtitle' => lifeline2_set($instance, 'subtitle')), $this->id_base
        );

        echo wp_kses($before_widget, true);

        if ($title && lifeline2_set($instance, 'titles') != '') {
            echo wp_kses($before_title, true) . html_entity_decode($title) . wp_kses($after_title, true);
        }
        ?>
        <div class="volunteer">
            <?php
            $mediaId = lifleline2_get_attachment_id_from_url(lifeline2_set($instance, 'banner'));
            $mediaSrc = wp_get_attachment_image_src($mediaId, 'full');
            $default = get_option('wp_donation_basic_settings', TRUE);
            $options = lifeline2_set($default, 'wp_donation_basic_settings');
            if (!empty($mediaSrc)) {
                echo '<img src="' . esc_url(lifeline2_set($mediaSrc, '0')) . '" alt="" />';
            }
            ?>
            <div class="volunteer-overlay">
                <div class="volunteer-inner">
                    <?php echo (lifeline2_set($instance, 'banner_custom_sub_title')) ? '<span>' . lifeline2_set($instance, 'banner_custom_sub_title') . '</span>' : ''; ?>
                    <?php echo (lifeline2_set($instance, 'banner_custom_title')) ? '<strong>' . lifeline2_set($instance, 'banner_custom_title') . '</strong>' : ''; ?>
                    <?php echo (lifeline2_set($instance, 'description')) ? '<p>' . lifeline2_set($instance, 'description') . '</p>' : ''; ?>
                    <?php
                    if (lifeline2_set($instance, 'donation') == 'yes') {
                        if (lifeline2_set($opt, 'donation_template_type_general') == 'donation_page_template'):
                            $url = get_page_link(lifeline2_set($opt, 'donation_button_pageGeneral'));
                            $queryParams = array('data_donation' => 'general', 'postId' => '');
                            ?>
                            <a class="theme-btn" itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                <?php echo esc_html(lifeline2_set($instance, 'button_label')) ?>
                            </a>
                            <?php
                        elseif (lifeline2_set($opt, 'donation_template_type_general') == 'external_link'):
                            $url = lifeline2_set($opt, 'donation_button_linkGeneral');
                            ?>
                            <a class="theme-btn" itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title="">
                                <?php echo esc_html(lifeline2_set($instance, 'button_label')) ?>
                            </a>
                            <?php
                        else:
                            ?>
                            <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                <a data-modal="general" data-donation="general" data-post="" itemprop="url" class="theme-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                    <?php echo esc_html(lifeline2_set($instance, 'button_label')) ?>
                                </a>
                            <?php endif; ?>
                        <?php
                        endif;
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        echo wp_kses($after_widget, true);
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['titles'] = lifeline2_set($new_instance, 'titles');
        $instance['subtitle'] = lifeline2_set($new_instance, 'subtitle');
        $instance['banner_custom_title'] = lifeline2_set($new_instance, 'banner_custom_title');
        $instance['banner_custom_sub_title'] = lifeline2_set($new_instance, 'banner_custom_sub_title');
        $instance['description'] = lifeline2_set($new_instance, 'description');
        $instance['banner'] = lifeline2_set($new_instance, 'banner');
        $instance['button_label'] = lifeline2_set($new_instance, 'button_label');
        $instance['donation'] = lifeline2_set($new_instance, 'donation');
        return $instance;
    }

    function form($instance) {
        //printr($instance);
        $titles = ($instance) ? lifeline2_set($instance, 'titles') : '';
        $subtitle = ($instance) ? lifeline2_set($instance, 'subtitle') : '';
        $banner_custom_title = ($instance) ? lifeline2_set($instance, 'banner_custom_title') : '';
        $banner_custom_sub_title = ($instance) ? lifeline2_set($instance, 'banner_custom_sub_title') : '';
        $description = ($instance) ? lifeline2_set($instance, 'description') : '';
        $banner = (lifeline2_set($instance, 'banner')) ? lifeline2_set($instance, 'banner') : get_template_directory_uri() . '/assets/images/no-image.jpg';
        $button_label = ($instance) ? lifeline2_set($instance, 'button_label') : '';
        $donation = ($instance) ? lifeline2_set($instance, 'donation') : '';
        wp_enqueue_style('thickbox');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        ?>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('titles')); ?>"><?php esc_html_e('Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('titles')); ?>" name="<?php echo esc_attr($this->get_field_name('titles')); ?>" type="text" value="<?php echo esc_attr($titles); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php esc_html_e('Sub Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('banner_custom_title')); ?>"><?php esc_html_e('Banner Custom Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('banner_custom_title')); ?>" name="<?php echo esc_attr($this->get_field_name('banner_custom_title')); ?>" type="text" value="<?php echo esc_attr($banner_custom_title); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('banner_custom_sub_title')); ?>"><?php esc_html_e('Banner Custom Sub Title:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('banner_custom_sub_title')); ?>" name="<?php echo esc_attr($this->get_field_name('banner_custom_sub_title')); ?>" type="text" value="<?php echo esc_attr($banner_custom_sub_title); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php esc_html_e('Description:', 'lifeline2'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>"><?php echo esc_html($description); ?></textarea>
        </p>
        <div style=" width: 100%; min-height:100px; float: left;margin-bottom: 10px; margin-top: 10px;">

            <label style="margin-bottom:10px; display: inline-block;" for="<?php echo esc_attr($this->get_field_id('banner')); ?>"><?php esc_html_e('Banner Image:', 'lifeline2'); ?></label>

            <div class="img-box">
                <img width="100" height="100" style="float: right;" class="banner-image" src="<?php echo esc_url($banner); ?>">
                <span data-bind="<?php echo get_template_directory_uri() . '/assets/images/no-image.jpg'; ?>"><?php esc_html_e('Remove(x)', 'lifeline2'); ?></span>
            </div> 
            <input type="hidden" class="img" name="<?php echo esc_attr($this->get_field_name('banner')); ?>" id="<?php echo esc_attr($this->get_field_id('banner')); ?>" value="<?php echo esc_attr($banner); ?>" />
            <input type="button" class="select-img button button-primary " value="<?php esc_attr_e('Select Image', 'lifeline2'); ?>" />
        </div>
        <p>    
            <label><input type="checkbox" id="<?php echo esc_attr($this->get_field_id('donation')); ?>" name="<?php echo esc_attr($this->get_field_name('donation')); ?>" <?php echo ($donation == 'yes') ? 'checked="checked"' : ''; ?>    value="yes" /><?php esc_html_e('Enable Donation', 'lifeline2'); ?></label><br /> <?php esc_html_e('Enable to show donation button on banner', 'lifeline2'); ?>
        </p>
        <p>    
            <label for="<?php echo esc_attr($this->get_field_id('button_label')); ?>"><?php esc_html_e('Button Label:', 'lifeline2'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('button_label')); ?>" name="<?php echo esc_attr($this->get_field_name('button_label')); ?>" type="text" value="<?php echo esc_attr($button_label); ?>" /> 
        </p>
        <style type="text/css">
            .img-box {
                float: right;
                height: 100px;
                position: relative;
                width: 100px;
            }
            .img-box:hover > span{
                display: block;
            }
            .img-box > span{
                display: none;
                background: #eee none repeat scroll 0 0;
                padding-bottom: 3px;
                padding-top: 3px;
                position: absolute;
                text-align: center;
                width: 100%;
                cursor: pointer;
            }
        </style>
        <?php
    }

}
