<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_Widgets {

    static protected $_widgets = array(
        'about_us',
        'recent_posts',
        'flickr_feed',
        'donation_banner',
        'upcoming_events',
        'custom_video_banner',
        'twitter_tweets',
        'sponsers'
        
    );

    static public function register() {
        $widgets_ = array();
        foreach (self::$_widgets as $widget) {
            $widgets_[lifeline2_ROOT . 'core/application/library/widgets/' . strtolower($widget) . '.php'] = $widget;
        }

        $widgets_ = apply_filters('lifeline2_extend_widgets_', $widgets_);
        foreach ($widgets_ as $path => $register) {
            require_once( $path );
            $widget_class = 'lifeline2_' . $register . '_Widget';
            register_widget($widget_class);
        }
    }

}
