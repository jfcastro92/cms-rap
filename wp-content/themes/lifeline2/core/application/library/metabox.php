<?php
if ( !defined( "lifeline2_DIR" ) )
	die( '!!!' );

class lifeline2_Metabox {

	public $meta = array(
            'page',
            'causes',
            'projects',
            'story',
            'team',
            'event',
            'gallery',
            'sponsor',
            'service'
	);

	public function __construct() {
		//(new unload_Helper() )->unload_check( $this->meta );
		asort( $this->meta );
		foreach ( $this->meta as $m ) {
                        //printr('core/aplication/library/metabox/' . $m . '.php');
			locate_template( 'core/application/library/metabox/'. $m .'.php', true, true );
			$class = 'lifeline2_' . $m  . '_Meta';
			new $class();
		}
	}

}

new lifeline2_Metabox();
