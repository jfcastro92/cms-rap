<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_profile_Meta {

    static public $options = array();
    static public $title = 'Profile Options';
    static public $type = array('dim_profile');
    static public $priority = 'high';

    static public function init() {
        self::$options = array(
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_info',
                'title' => esc_html__('Author Info', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'select',
                        'name' => 'what_author',
                        'label' => esc_html__('Select Profile Author:', 'lifeline2'),
                        'description' => esc_html__('Select the Author for this profile.', 'lifeline2'),
                        'items' => array(
                            'data' => array(
                                array(
                                    'source' => 'function',
                                    'value' => 'vp_get_users',
                                ),
                            ),
                        ),
                        'default' => array(
                            '{{first}}',
                        ),
                    ),
                    array(
                        'type' => 'upload',
                        'name' => 'author_pic',
                        'label' => esc_html__('Upload author image', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'author_title',
                        'label' => esc_html__('Title:', 'lifeline2'),
                        'description' => esc_html__('Enter the title for author.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'author_sub_title',
                        'label' => esc_html__('Sub Title:', 'lifeline2'),
                        'description' => esc_html__('Enter the sub title for author.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textarea',
                        'name' => 'about_auhtor',
                        'label' => esc_html__('About Author:', 'lifeline2'),
                        'description' => esc_html__('Enter the short description note for author.', 'lifeline2'),
                    ),
                ),
            ),
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_contact_info',
                'title' => esc_html__('Author Contact Info', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'textbox',
                        'name' => 'author_contact_name',
                        'label' => esc_html__('Contact Name:', 'lifeline2'),
                        'description' => esc_html__('Enter the contact name.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'author_contact_email',
                        'label' => esc_html__('Email:', 'lifeline2'),
                        'description' => esc_html__('Enter the contact email.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'author_contact_no',
                        'label' => esc_html__('Contact No:', 'lifeline2'),
                        'description' => esc_html__('Enter the contact number.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'author_contact_address',
                        'label' => esc_html__('Address:', 'lifeline2'),
                        'description' => esc_html__('Enter the contact address.', 'lifeline2'),
                    ),
                ),
            ),
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_about',
                'title' => esc_html__('Author About Note', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'textbox',
                        'name' => 'author_about_title',
                        'label' => esc_html__('Title:', 'lifeline2'),
                        'description' => esc_html__('Enter the title.', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textarea',
                        'name' => 'author_about_note',
                        'label' => esc_html__('Author Note:', 'lifeline2'),
                        'description' => esc_html__('Enter the author about note.', 'lifeline2'),
                    ),
                ),
            ),
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_social',
                'title' => esc_html__('Author Social Profile', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'group',
                        'repeating' => true,
                        'sortable' => true,
                        'length' => 1,
                        'name' => 'dim_author_social_profile',
                        'title' => esc_html__('Author Social Links:', 'lifeline2'),
                        'fields' => array(
                            array(
                                'type' => 'textbox',
                                'name' => 'social_link',
                                'label' => esc_html__('Link', 'lifeline2'),
                                'description' => esc_html__('Enter the Link for Social Media.', 'lifeline2'),
                                'default' => '#',
                            ),
                            array(
                                'type' => 'color',
                                'name' => 'social_color',
                                'label' => esc_html__('Color', 'lifeline2'),
                                'description' => esc_html__('Select color for this social media.', 'lifeline2'),
                            ),
                            array(
                                'type' => 'fontawesome',
                                'name' => 'au_social_icon',
                                'label' => esc_html__('Icon', 'lifeline2'),
                                'description' => esc_html__('Choose Icon for Social Media.', 'lifeline2'),
                                'default' => '',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_exp',
                'title' => esc_html__('Author Experience', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'group',
                        'repeating' => true,
                        'sortable' => true,
                        'length' => 1,
                        'name' => 'dim_author_exp_',
                        'title' => esc_html__('Author Experience:', 'lifeline2'),
                        'fields' => array(
                            array(
                                'type' => 'textbox',
                                'name' => 'author_exp_company',
                                'label' => esc_html__('Company Name:', 'lifeline2'),
                                'description' => esc_html__('Enter the company name.', 'lifeline2'),
                            ),
                            array(
                                'type' => 'textbox',
                                'name' => 'author_exp_designation',
                                'label' => esc_html__('Designation:', 'lifeline2'),
                                'description' => esc_html__('Enter Your Designation.', 'lifeline2'),
                            ),
                            array(
                                'type' => 'textarea',
                                'name' => 'author_exp_desc',
                                'label' => esc_html__('Job Description:', 'lifeline2'),
                                'description' => esc_html__('Enter Your Job Description.', 'lifeline2'),
                            ),
                            array(
                                'type' => 'date',
                                'name' => 'author_exp_start_dt',
                                'label' => esc_html__('Select Date:', 'lifeline2'),
                                'description' => esc_html__('Select Your Hearing Date.', 'lifeline2'),
                                'format' => 'yy-mm-dd',
                            ),
                            array(
                                'type' => 'date',
                                'name' => 'author_exp_end_dt',
                                'label' => esc_html__('Select Date:', 'lifeline2'),
                                'description' => esc_html__('Select Your Leaving Date.', 'lifeline2'),
                                'format' => 'yy-mm-dd',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'type' => 'group',
                'repeating' => false,
                'length' => 1,
                'name' => 'dim_author_skills',
                'title' => esc_html__('Author Skills', 'lifeline2'),
                'fields' =>
                array(
                    array(
                        'type' => 'group',
                        'repeating' => true,
                        'sortable' => true,
                        'length' => 1,
                        'name' => 'dim_author_skills_',
                        'title' => esc_html__('Author Skills:', 'lifeline2'),
                        'fields' => array(
                            array(
                                'type' => 'textbox',
                                'name' => 'author_skill_name',
                                'label' => esc_html__('Skill Name:', 'lifeline2'),
                                'description' => esc_html__('Enter the skill nama.', 'lifeline2'),
                            ),
                            array(
                                'type' => 'textbox',
                                'name' => 'author_skill_percent',
                                'label' => esc_html__('Percentage Skill:', 'lifeline2'),
                                'description' => esc_html__('Enter Your Skill Percentage.', 'lifeline2'),
                                'validation' => 'numeric',
                            ),
                        ),
                    ),
                ),
            ),
        );

        return apply_filters('lifeline2_extend_profile_meta_', self::$options);
    }

}
