<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_post_setting_menu {
    public $id          = 'post_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Single Post Settings', 'lifeline2');
        $this->description = esc_html__('Post detail page settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'      => 's_post_date',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Post Date', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show post publish date on post detail page', 'lifeline2' ),
                'default' => true,
            ),
            array(
                'id'      => 's_post_author',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Post Author', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show post author name on post detail page', 'lifeline2' ),
                'default' => true,
            ),
            array(
                'id'      => 's_post_cat',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Post Categories', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show post categories list on post detail page', 'lifeline2' ),
                'default' => true,
            ),
            array(
                'id'    => 's_share_icons',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Social Share Icons', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to allow post sharing to scial media websites', 'lifeline2' ),
            ),
            array(
                'id'       => 'social_sharing_section_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Social Media Section Title', 'lifeline2' ),
                'desc'     => esc_html__( 'Enter the title for social sharing section to show on post detail page', 'lifeline2' ),
                'required' => array( 's_share_icons', '=', true )
            ),
            array(
                'id'       => 'single_post_social_share',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Post Sharing Icons', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select icons to activate social sharing icons in post detail page', 'lifeline2' ),
                'required' => array( 's_share_icons', '=', true ),
                'mode'     => 'checkbox',
                'options'  => array(
                    'facebook'    => esc_html__( 'Facebook', 'lifeline2' ),
                    'twitter'     => esc_html__( 'Twitter', 'lifeline2' ),
                    'gplus'       => esc_html__( 'Google Plus', 'lifeline2' ),
                    'digg'        => esc_html__( 'Digg Digg', 'lifeline2' ),
                    'reddit'      => esc_html__( 'Reddit', 'lifeline2' ),
                    'linkedin'    => esc_html__( 'Linkedin', 'lifeline2' ),
                    'pinterest'   => esc_html__( 'Pinterest', 'lifeline2' ),
                    'stumbleupon' => esc_html__( 'Sumbleupon', 'lifeline2' ),
                    'tumblr'      => esc_html__( 'Tumblr', 'lifeline2' ),
                    'email'       => esc_html__( 'Email', 'lifeline2' ),
                ),
            ),
            array(
                'id'      => 's_post_tags',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Post Tags', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show post tags list on post detail page', 'lifeline2' ),
                'default' => true,
            )
        );
        return apply_filters( 'lifeline2_vp_opt_single_post_', $return );
    }
}