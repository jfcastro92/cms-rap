<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_event_setting_menu {

    public $id = 'event_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Events Settings', 'lifeline2');
        $this->description = esc_html__('Events templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'event_cat_settings',
                'type' => 'section',
                'title' => esc_html__('Event Category Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the events category page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'event_cat_show_title_section',
                'type' => 'switch',
                'title' => esc_html__('Show Title Section', 'lifeline2'),
                'desc' => esc_html__('Show title banner section', 'lifeline2'),
            ),
            array(
                'id' => 'event_cat_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'lifeline2'),
                'desc' => esc_html__('Enter the title', 'lifeline2'),
                'required' => array('event_cat_show_title_section', '=', true),
            ),
            array(
                'id' => 'event_cat_title_section_bg',
                'type' => 'background',
                'title' => esc_html__('Title Section Background', 'lifeline2'),
                'desc' => esc_html__('Upload background image for events category page title section', 'lifeline2'),
                'required' => array('event_cat_show_title_section', '=', true),
                'background-color' => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position' => false,
                'transparent' => false,
                'background-size' => false
            ),
            array(
                'id' => 'event_cat_layout',
                'type' => 'image_select',
                'title' => esc_html__('Page Layout', 'lifeline2'),
                'subtitle' => esc_html__('Select main content and sidebar alignment.', 'lifeline2'),
                'options' => array(
                    'full' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left' => array(
                        'alt' => esc_html__('2 Column Left', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__('2 Column Right', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default' => 'full'
            ),
            array(
                'id' => 'event_cat_sidebar',
                'type' => 'select',
                'title' => esc_html__('Sidebar', 'lifeline2'),
                'desc' => esc_html__('Select sidebar to show on event category page', 'lifeline2'),
                'required' => array(
                    array('event_cat_layout', '=', array('left', 'right'))
                ),
                'select2' => array('allowClear' => true),
                'width' => '60%',
                'data' => 'sidebars',
            ),
            array(
                'id' => 'event_cat_column',
                'type' => 'image_select',
                'title' => esc_html__('Select Column', 'lifeline2'),
                'subtitle' => esc_html__('Select the events listing style', 'lifeline2'),
                'options' => array(
                    'col-md-12' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-1-col.jpg'
                    ),
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Events.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'event_cat_time_count',
                'type' => 'switch',
                'title' => esc_html__('Show Timer', 'lifeline2'),
                'desc' => esc_html__('Enable to show event countdown timer on event listing', 'lifeline2'),
            ),
            array(
                'id' => 'event_cat_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' => esc_html__('Enter the value of events title character limit to show in events listing', 'lifeline2'),
                
            ),
            array(
                'id' => 'event_template_settings',
                'type' => 'section',
                'title' => esc_html__('Event Template Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the events template page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'event_template_columns',
                'type' => 'image_select',
                'title' => esc_html__('Select Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the events listing style', 'lifeline2'),
                'options' => array(
                    'col-md-12' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-1-col.jpg'
                    ),
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Events.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/event-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'event_template_time_count',
                'type' => 'switch',
                'title' => esc_html__('Show Timer', 'lifeline2'),
                'desc' => esc_html__('Enable to show event countdown timer on event listing', 'lifeline2'),
            ),
            array(
	            'id' => 'event_template_breadcrumb',
	            'type' => 'switch',
	            'title' => esc_html__('Show BreadCrumb', 'lifeline2'),
	            'desc' => esc_html__('Enable to show breadcrumb on event listing', 'lifeline2'),
            ),
            array(
                'id' => 'event_template_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' => esc_html__('Enter the value of events title character limit to show in events listing', 'lifeline2'),
                
            ),
            array(
                'id' => 'event_template_pagination',
                'type' => 'switch',
                'title' => esc_html__('Show Pagination', 'lifeline2'),
                'desc' => esc_html__('Enable to show events listing pagination', 'lifeline2'),
            ),
            array(
                'id' => 'event_template_pagination_num',
                'type' => 'text',
                'title' => esc_html__('Events Per Page', 'lifeline2'),
                'desc' => esc_html__('Enter the number events to show per page', 'lifeline2'),
                'required' => array('event_template_pagination', '=', true),
            ),
            array(
                'id' => 'event_detail_settings',
                'type' => 'section',
                'title' => esc_html__('Event Detail Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the event detail page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'event_detail_show_date',
                'type' => 'switch',
                'title' => esc_html__('Show Date', 'lifeline2'),
                'desc' => esc_html__('Show or hide event date', 'lifeline2'),
            ),
            array(
                'id' => 'event_detail_show_location',
                'type' => 'switch',
                'title' => esc_html__('Show Location', 'lifeline2'),
                'desc' => esc_html__('Show or hide event location', 'lifeline2'),
            ),
            array(
                'id' => 'event_detail_show_author',
                'type' => 'switch',
                'title' => esc_html__('Show Author', 'lifeline2'),
                'desc' => esc_html__('Show or hide post author', 'lifeline2'),
            ),
            array(
                'id' => 'event_detail_show_event_time',
                'type' => 'switch',
                'title' => esc_html__('Show Event Time', 'lifeline2'),
                'desc' => esc_html__('Show or hide event date time', 'lifeline2'),
            ),
            array(
                'id' => 'event_detail_show_org_info',
                'type' => 'switch',
                'title' => esc_html__('Show Organization Info', 'lifeline2'),
                'desc' => esc_html__('Show or hide ever organization information', 'lifeline2'),
            ),
            array(
                'id' => 'organizer_info_section_label',
                'type' => 'text',
                'title' => esc_html__('Title', 'lifeline2'),
                'desc' => esc_html__('Enter the title for event organizer section', 'lifeline2'),
                'required' => array('event_detail_show_org_info', '=', true),
            ),
            array(
                'id' => 'event_detail_show_social_share',
                'type' => 'switch',
                'title' => esc_html__('Show Social Media', 'lifeline2'),
                'desc' => esc_html__('Enable social sharing media for events', 'lifeline2'),
            ),
            array(
                'id' => 'event_detail_show_social_share_title',
                'type' => 'text',
                'title' => esc_html__('Title Text', 'lifeline2'),
                'desc' => esc_html__('Enter the social sharing section', 'lifeline2'),
                'default' => esc_html__('Share This Post:', 'lifeline2'),
                'required' => array('event_detail_show_social_share', '=', true),
            ),
            array(
                'id' => 'event_detail_social_media',
                'type' => 'sortable',
                'title' => esc_html__('Event Social Media', 'lifeline2'),
                'subtitle' => esc_html__('Select icons to activate social sharing icons in event detail page', 'lifeline2'),
                'required' => array('event_detail_show_social_share', '=', true),
                'mode' => 'checkbox',
                'options' => array(
                    'facebook' => esc_html__('Facebook', 'lifeline2'),
                    'twitter' => esc_html__('Twitter', 'lifeline2'),
                    'gplus' => esc_html__('Google Plus', 'lifeline2'),
                    'digg' => esc_html__('Digg Digg', 'lifeline2'),
                    'reddit' => esc_html__('Reddit', 'lifeline2'),
                    'linkedin' => esc_html__('Linkedin', 'lifeline2'),
                    'pinterest' => esc_html__('Pinterest', 'lifeline2'),
                    'stumbleupon' => esc_html__('Sumbleupon', 'lifeline2'),
                    'tumblr' => esc_html__('Tumblr', 'lifeline2'),
                    'email' => esc_html__('Email', 'lifeline2'),
                ),
            )
        );
        return apply_filters('lifeline2_vp_opt_event_settings_', $return);
    }

}
