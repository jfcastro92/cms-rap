<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_under_construction_setting_menu {
    public $id          = 'under_construction_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-compass';

    public function __construct(){
        $this->title = esc_html__('Under Construction Settings', 'lifeline2');
        $this->description = esc_html__('Coming Soon Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'      => 'under_construction_status',
                'type'    => 'switch',
                'title'   => esc_html__( 'Under Construction', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to make the site Under Construction', 'lifeline2' ),
                'default' => false,
            ),
            array(
                'id'      => 'under_construction_date',
                'type'    => 'date',
                'title'   => esc_html__( 'Date', 'lifeline2' ),
                'desc'    => esc_html__( 'Give date for Under Construction', 'lifeline2' ),
                'placeholder' => 'Click to enter a date',
                'required'      => array( 'under_construction_status', '=', '1' ),
                'default' => '',
            ),
            array(
                'id'      => 'under_construction_title',
                'type'    => 'text',
                'title'   => esc_html__( 'Welcome Title', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the Welcome Title to show on under construction page.', 'lifeline2' ),
                'placeholder' => 'Enter the Welcome Title',
                'required'      => array( 'under_construction_status', '=', '1' ),
                'default' => '',
            ),
            array(
                'id'      => 'under_construction_titletext',
                'type'    => 'text',
                'title'   => esc_html__( 'Welcome Text', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the Welcome Text to show on under construction page.', 'lifeline2' ),
                'placeholder' => 'Enter the Welcome Text',
                'required'      => array( 'under_construction_status', '=', '1' ),
                'default' => '',
            ),
            array(
                'id'      => 'under_construction_bgimg',
                'type'    => 'media',
                'url'     => true,
                'title'   => esc_html__( 'Background Image', 'lifeline2' ),
                'desc'    => esc_html__( 'Choose the background image', 'lifeline2' ),
                'required'      => array( 'under_construction_status', '=', '1' ),
                'default' => '',
            ),

        );

        return apply_filters( 'lifeline2_vp_opt_under_construction_', $return );
    }
}