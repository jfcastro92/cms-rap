<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_menu_font_setting_menu {

    public $id = 'menu_font_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';

    public function __construct(){
        $this->title = esc_html__('Menu Font Settings', 'lifeline2');
        $this->description = esc_html__('Menu font typography settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'menu_custom_fonts',
                'type' => 'switch',
                'title' => esc_html__('Use Menu Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme menu font', 'lifeline2'),
            ),
            array(
                'id' => 'menu_typography',
                'type' => 'typography',
                'title' => esc_html__('Menu Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('.menu'),
                'compiler' => true,
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the menu fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('menu_custom_fonts', '=', true),
            ),
        );
        return apply_filters('lifeline2_vp_opt_menu_font_', $return);
    }

}
