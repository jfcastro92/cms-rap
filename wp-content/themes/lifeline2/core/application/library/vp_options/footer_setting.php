<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_footer_setting_menu {
    public $id          = 'footer_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-hand-down';
    
    public function __construct(){
        $this->title = esc_html__('Footer Settings', 'lifeline2');
        $this->description = esc_html__('Customize the footer as per your requirements', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'                    => 'footer_bg',
                'type'                  => 'background',
                'title'                 => esc_html__( 'Footer Background Image', 'lifeline2' ),
                'desc'                  => esc_html__( 'Insert the background image for footer section', 'lifeline2' ),
                'background-color'      => false,
                'background-repeat'     => false,
                'background-attachment' => false,
                'background-position'   => false,
                'transparent'           => false,
                'background-size'       => false
            ),
            array(
	            'id'          => 'footer_color_scheme',
	            'type'        => 'color',
	            'output'      => array( '.site-title' ),
	            'title'       => esc_html__( 'Footer Background Layer Color', 'lifeline2' ),
	            'transparent' => false
            ),
            array(
                'id' => 'f_col',
                'type' => 'select',
                'title' => esc_html__('Select Number of columns', 'lifeline2'),
                'desc' => esc_html__('Select number of columns to show widgets in footer section', 'lifeline2'),
                'options' => array(12 => 'One', 6 => 'Two', 4 => 'Three', 3 => 'Four', 2 => 'Six', 1 => 'Twelve'),
                'default' => 3,

            ),
            array(
                'id'    => 'show_newsletter_section',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Newsletter Section', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show newsletter in footer section', 'lifeline2' ),
            ),
            array(
                'id'       => 'newsletter_section',
                'type'     => 'section',
                'title'    => esc_html__( 'Newsletter Section', 'lifeline2' ),
                'desc'     => esc_html__( 'This section is used to add newsletter form in footer section', 'lifeline2' ),
                'indent'   => true,
                'required' => array( 'show_newsletter_section', '=', true ),
            ),
            array(
                'id'      => 'subc_title',
                'type'    => 'text',
                'title'   => esc_html__( 'Form Title', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the title of email subscription section', 'lifeline2' ),
                'default' => '',
            ),
            array(
                'id'      => 'email_placehold',
                'type'    => 'text',
                'title'   => esc_html__( 'Field Placeholder', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the placeholder text for newsletter field', 'lifeline2' ),
                'default' => esc_html__( 'Enter Your Email', 'lifeline2' ),
            ),
            array(
                'id'      => 'subc_btn_txt',
                'type'    => 'text',
                'title'   => esc_html__( 'Button Text', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the label for the button', 'lifeline2' ),
                'default' => esc_html__( 'SUBSCRIBE', 'lifeline2' ),
            ),
            array(
                'id'    => 'footer_social_icons',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Social Media Icons', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show social media in footer section', 'lifeline2' ),
            ),
            array(
                'id'       => 'social_icons_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Social Media Section Title', 'lifeline2' ),
                'default'  => '',
                'required' => array( 'footer_social_icons', '=', true ),
            ),
            array(
                'id'       => 'footer_social_media',
                'type'     => 'social_media',
                'title'    => 'Social Profiles',
                'subtitle' => 'Click an icon to activate it to show social profile icons in footer section',
                'required' => array( 'footer_social_icons', '=', true ),
            ),
            array(
                'id'       => 'newsletter_section_end',
                'type'     => 'section',
                'indent'   => false,
                'required' => array( 'show_newsletter_section', '=', true ),
            ),
            array(
                'id'      => 'show_footer_bottom',
                'type'    => 'switch',
                'title'   => esc_html__( 'Show Footer Bottom Section', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to show footer bottom section', 'lifeline2' ),
                'default' => true,
            ),
            array(
                'id'       => 'footer_bottom_section',
                'type'     => 'section',
                'title'    => esc_html__( 'Footer Bottom Section', 'lifeline2' ),
                'desc'     => esc_html__( 'This section is used to show footer bottom section', 'lifeline2' ),
                'indent'   => true,
                'required' => array( 'show_footer_bottom', '=', true ),
            ),
            array(
                'id'    => 'show_footer_menu',
                'type'  => 'switch',
                'title' => esc_html__( 'Show Footer Menu', 'lifeline2' ),
                'desc'  => esc_html__( 'Enable to show footer menu', 'lifeline2' ),
            ),
            array(
                'id'      => 'copyright_text',
                'type'    => 'textarea',
                'title'   => esc_html__( 'Copyright Text', 'lifeline2' ),
                'desc'    => esc_html__( 'Enter the Copyright Text', 'lifeline2' ),
                'default' => esc_html__( '&copy; 2016 lifeline2 - All Rights Reserved - Made By Webinane', 'lifeline2' ),
            ),
            
        );
        return apply_filters( 'lifeline2_vp_opt_footer_', $return );
    }
}