<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_heading_setting_menu {

    public $id = 'heading_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Headings Settings', 'lifeline2');
        $this->description = esc_html__('Headings typography settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'use_custom_heading_style_h1',
                'type' => 'switch',
                'title' => esc_html__('Use H1 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h1 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h1_typography',
                'type' => 'typography',
                'title' => esc_html__('H1 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h1.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h1 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h1', '=', true),
            ),
            array(
                'id' => 'use_custom_heading_style_h2',
                'type' => 'switch',
                'title' => esc_html__('Use H2 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h2 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h2_typography',
                'type' => 'typography',
                'title' => esc_html__('H2 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h2.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h2 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h2', '=', true),
            ),
            array(
                'id' => 'use_custom_heading_style_h3',
                'type' => 'switch',
                'title' => esc_html__('Use H3 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h3 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h3_typography',
                'type' => 'typography',
                'title' => esc_html__('H3 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h3.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h3 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h3', '=', true),
            ),
            array(
                'id' => 'use_custom_heading_style_h4',
                'type' => 'switch',
                'title' => esc_html__('Use H4 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h4 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h4_typography',
                'type' => 'typography',
                'title' => esc_html__('H4 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h4.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h4 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h4', '=', true),
            ),
            array(
                'id' => 'use_custom_heading_style_h5',
                'type' => 'switch',
                'title' => esc_html__('Use H5 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h5 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h5_typography',
                'type' => 'typography',
                'title' => esc_html__('H5 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h5.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h5 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h5', '=', true),
            ),
            array(
                'id' => 'use_custom_heading_style_h6',
                'type' => 'switch',
                'title' => esc_html__('Use H6 Custom Font', 'lifeline2'),
                'desc' => esc_html__('Enable to customize the theme heading h6 tag font', 'lifeline2'),
            ),
            array(
                'id' => 'h6_typography',
                'type' => 'typography',
                'title' => esc_html__('H6 Font Typography', 'lifeline2'),
                'google' => true,
                'font-backup' => true,
                'output' => array('h6.site-description'),
                'units' => 'px',
                'subtitle' => esc_html__('Apply options to customize the h6 heading fonts for the theme', 'lifeline2'),
                'default' => array(
                    'color' => '#333',
                    'font-style' => '700',
                    'font-family' => 'Abel',
                    'google' => true,
                    'font-size' => '33px',
                    'line-height' => '40'
                ),
                'required' => array('use_custom_heading_style_h6', '=', true),
            )
        );
        return apply_filters('lifeline2_vp_opt_heading_', $return);
    }

}
