<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_Project_setting_menu {

    public $id = 'project_settings';
    public $title = '';
    public $description = '';
    public $icon = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('Projects Settings', 'lifeline2');
        $this->description = esc_html__('Projects templates & listing settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id' => 'products_cat_settings',
                'type' => 'section',
                'title' => esc_html__('Project Category Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the project category page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'project_cat_show_title_section',
                'type' => 'switch',
                'title' => esc_html__('Show Title Section', 'lifeline2'),
                'desc' => esc_html__('Show title banner section if empty then show from page meta', 'lifeline2'),
            ),
            array(
                'id' => 'project_cat_title',
                'type' => 'text',
                'title' => esc_html__('Title', 'lifeline2'),
                'desc' =>  esc_html__('Enter the title', 'lifeline2'),
                'required' => array('project_cat_show_title_section', '=', true),
            ),
            array(
                'id' => 'project_cat_title_section_bg',
                'type' => 'background',
                'title' => esc_html__('Title Section Background', 'lifeline2'),
                'desc' => esc_html__('Upload background image for project category page title section', 'lifeline2'),
                'required' => array('project_cat_show_title_section', '=', true),
                'background-color' => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position' => false,
                'transparent' => false,
                'background-size' => false 
            ),
            array(
                'id' => 'project_cat_column',
                'type' => 'image_select',
                'title' => esc_html__('Projects Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the projects listing style', 'lifeline2'),
                'options' => array(
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/project-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Project.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/project-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'project_cat_layout',
                'type' => 'image_select',
                'title' => esc_html__('Page Layout', 'lifeline2'),
                'subtitle' => esc_html__('Select main content and sidebar alignment.', 'lifeline2'),
                'options' => array(
                    'full' => array(
                        'alt' => esc_html__('1 Column', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'left' => array(
                        'alt' => esc_html__('2 Column Left', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array(
                        'alt' => esc_html__('2 Column Right', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    
                ),
                'required' => array('project_cat_column', '=', array('col-md-6','col-md-4')), 

                'default' => 'full'
            ),
            array(
                'id' => 'project_cat_sidebar',
                'type' => 'select',
                'title' => esc_html__('Sidebar', 'lifeline2'),
                'desc' => esc_html__('Select sidebar to show at project category page', 'lifeline2'),
                'required' => array(
                    array('project_cat_column', '=', array('col-md-6','col-md-4')),
                    array('project_cat_layout', '=', array('left','right'))
                ),
                'select2' => array('allowClear' => true),
                'width' => '60%',
                'data' => 'sidebars',
            ),
            array(
                'id' => 'project_cat_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the character limit for project title to show on project category pages', 'lifeline2'),
            ),
            array(
                'id' => 'project_cat_bar',
                'type' => 'switch',
                'title' => esc_html__('Progress Bar', 'lifeline2'),
                'desc' => esc_html__('Enable to show progress bar on project category page', 'lifeline2'),
            ),
            array(
                'id' => 'project_cat_donation',
                'type' => 'switch',
                'title' => esc_html__('Projects Donation', 'lifeline2'),
                'desc' => esc_html__('Enable project donation on project category page', 'lifeline2'),
            ),
            array(
                'id' => 'project_cat_btn_label',
                'type' => 'text',
                'title' => esc_html__('Donation Button Label', 'lifeline2'),
                'desc' =>  esc_html__('Enter the label text for donation button to show on project category pages', 'lifeline2'),
                'required' => array('project_cat_donation', '=', true),
            ),
            array(
                'id' => 'projects_template_settings',
                'type' => 'section',
                'title' => esc_html__('Projects Template Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the project template', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'project_template_column',
                'type' => 'image_select',
                'title' => esc_html__('Projects Listing Style', 'lifeline2'),
                'subtitle' => esc_html__('Select the projects listing style for project template', 'lifeline2'),
                'options' => array(
                    'col-md-6' => array(
                        'alt' => esc_html__('2 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/project-2-col.jpg'
                    ),
                    'col-md-4' => array(
                        'alt' => esc_html__('3 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/Project.jpg'
                    ),
                    'col-md-3' => array(
                        'alt' => esc_html__('4 Columns', 'lifeline2'),
                        'img' => ReduxFramework::$_url . 'assets/img/layout/project-4-col.jpg'
                    ),
                ),
                'default' => 'col-md-4'
            ),
            array(
                'id' => 'project_template_pagination',
                'type' => 'switch',
                'title' => esc_html__('Show Pagination', 'lifeline2'),
                'desc' => esc_html__('Enable to show project listing pagination on project template', 'lifeline2'),
            ),
            array(
                'id' => 'project_template_pagination_num',
                'type' => 'text',
                'title' => esc_html__('Projects Per Page', 'lifeline2'),
                'desc' =>  esc_html__('Enter the number projects to show per page', 'lifeline2'),
                'required' => array('project_template_pagination', '=', true),
            ),
            array(
                'id' => 'project_template_title_limit',
                'type' => 'text',
                'title' => esc_html__('Title Character Limit', 'lifeline2'),
                'desc' =>  esc_html__('Enter the character limit for project title to show on project template pages', 'lifeline2'),
            ),
            array(
                'id' => 'project_template_bar',
                'type' => 'switch',
                'title' => esc_html__('Progress Bar', 'lifeline2'),
                'desc' => esc_html__('Enable to show progress bar on project template pages', 'lifeline2'),
            ),
            array(
                'id' => 'project_template_donation',
                'type' => 'switch',
                'title' => esc_html__('Projects Donation', 'lifeline2'),
                'desc' => esc_html__('Enable project donation on project template', 'lifeline2'),
            ),
            array(
                'id' => 'project_template_btn_label',
                'type' => 'text',
                'title' => esc_html__('Donation Button Label', 'lifeline2'),
                'desc' =>  esc_html__('Enter the label text for donation button to show on project template pages', 'lifeline2'),
                'required' => array('project_template_donation', '=', true),
            ),
            
            array(
                'id' => 'project_detail_settings',
                'type' => 'section',
                'title' => esc_html__('Project Detail Settings', 'lifeline2'),
                'desc' => esc_html__('This section is used to customize the project detail page', 'lifeline2'),
                'indent' => true,
            ),
            array(
                'id' => 'project_detail_show_date',
                'type' => 'switch',
                'title' => esc_html__('Show Date', 'lifeline2'),
                'desc' => esc_html__('Show or hide post date', 'lifeline2'),
            ),
            array(
                'id' => 'project_detail_show_location',
                'type' => 'switch',
                'title' => esc_html__('Show Location', 'lifeline2'),
                'desc' => esc_html__('Show or hide project location', 'lifeline2'),
            ),
             array(
                'id' => 'project_detail_show_category',
                'type' => 'switch',
                'title' => esc_html__('Show Category', 'lifeline2'),
                'desc' => esc_html__('Show or hide post category', 'lifeline2'),
            ),
            array(
                'id' => 'project_detail_show_author',
                'type' => 'switch',
                'title' => esc_html__('Show Author', 'lifeline2'),
                'desc' => esc_html__('Show or hide post author', 'lifeline2'),
            ),
            array(
                'id' => 'project_detail_show_donation',
                'type' => 'switch',
                'title' => esc_html__('Show Donation', 'lifeline2'),
                'desc' => esc_html__('Show or hide post donation box', 'lifeline2'),
            ),
            array(
                'id' => 'project_detail_show_social_share',
                'type' => 'switch',
                'title' => esc_html__('Show Social Media', 'lifeline2'),
                'desc' => esc_html__('Enable social sharing media for project', 'lifeline2'),
            ),
            array(
                'id' => 'project_detail_show_social_share_title',
                'type' => 'text',
                'title' => esc_html__('Title Text', 'lifeline2'),
                'desc' =>  esc_html__('Enter the social sharing section', 'lifeline2'),
                'default' => esc_html__('Share This Post:', 'lifeline2'),
                'required' => array('project_detail_show_social_share', '=', true),
            ),
            array(
                'id' => 'project_detail_social_media',
                'type' => 'sortable',
                'title' => esc_html__('Project Social Media', 'lifeline2'),
                'subtitle' => esc_html__('Select icons to activate social sharing icons in project detail page', 'lifeline2'),
                'required' => array('project_detail_show_social_share', '=', true),
                'mode' => 'checkbox',
                'options' => array(
                    'facebook' => esc_html__('Facebook', 'lifeline2'),
                    'twitter' => esc_html__('Twitter', 'lifeline2'),
                    'gplus' => esc_html__('Google Plus', 'lifeline2'),
                    'digg' => esc_html__('Digg Digg', 'lifeline2'),
                    'reddit' => esc_html__('Reddit', 'lifeline2'),
                    'linkedin' => esc_html__('Linkedin', 'lifeline2'),
                    'pinterest' => esc_html__('Pinterest', 'lifeline2'),
                    'stumbleupon' => esc_html__('Sumbleupon', 'lifeline2'),
                    'tumblr' => esc_html__('Tumblr', 'lifeline2'),
                    'email' => esc_html__('Email', 'lifeline2'),
                ),
            )
        );
        return apply_filters('lifeline2_vp_opt_projects_settings_', $return);
    }

}
