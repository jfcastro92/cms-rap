<?php

if (!defined("lifeline2_DIR"))
    die('!!!');

class lifeline2_pagination_setting_menu {

    public $title = '';
    public $icon = 'fa-th-large';
    public $description = '';
    
    public function __construct(){
        $this->title = esc_html__('Pagination Settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'type' => 'section',
                'title' => esc_html__('Pagination Settings', 'lifeline2'),
                'name' => 'pagination_settings',
                'fields' => array(
                    array(
                        'type' => 'textbox',
                        'name' => 'pagi_label',
                        'label' => esc_html__('Pagination Label:', 'lifeline2'),
                        'description' => esc_html__('The text/HTML to display before the list of pages.', 'lifeline2'),
                        'default' => esc_html__('Pages:', 'lifeline2'),
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'pagi_previous',
                        'label' => esc_html__('Previous Page:', 'lifeline2'),
                        'description' => esc_html__('The text/HTML to display for the previous page link.', 'lifeline2'),
                        'default' => '&laquo;',
                    ),
                    array(
                        'type' => 'textbox',
                        'name' => 'pagi_next',
                        'label' => esc_html__('Next Page:', 'lifeline2'),
                        'description' => esc_html__('The text/HTML to display for the next page link.', 'lifeline2'),
                        'default' => '&raquo;',
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'pagi_range',
                        'label' => esc_html__('Page Range:', 'lifeline2'),
                        'description' => esc_html__('The number of page links to show before and after the current page. Recommended value: 3', 'lifeline2'),
                        'items' => array(
                            'data' => array(
                                array(
                                    'source' => 'function',
                                    'value' => 'lifeline2_pagi_range',
                                ),
                            ),
                        ),
                        'default' => '3'
                    ),
                ),
            )
        );
        return apply_filters('lifeline2_vp_opt_pagination_', $return);
    }

}
