<?php
if ( !defined( "lifeline2_DIR" ) ) die( '!!!' );

class lifeline2_general_setting_menu {
    public $id          = 'general_settings';
    public $title       = '';
    public $description = '';
    public $icon        = 'el-cog';
    
    public function __construct(){
        $this->title = esc_html__('General Settings', 'lifeline2');
        $this->description = esc_html__('Lifeline2 theme general settings', 'lifeline2');
    }

    public function lifeline2_menu() {
        $return = array(
            array(
                'id'      => 'theme_rtl',
                'type'    => 'switch',
                'title'   => esc_html__( 'RTL', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable theme RTL', 'lifeline2' ),
                'default' => false,
            ),
            array(
                'id'          => 'theme_color_scheme',
                'type'        => 'color',
                'output'      => array( '.site-title' ),
                'title'       => esc_html__( 'Color Scheme', 'lifeline2' ),
                'default'     => '#cf3a35',
                'transparent' => false
            ),
            array(
                'id'      => 'donationCurrency',
                'type'    => 'select',
                'title'   => esc_html__( 'Select Currency', 'lifeline2' ),
                'select2' => array( 'allowClear' => true ),
                'width'   => '60%',
                'desc'    => esc_html__( 'Please set the currency which you want to set for your site', 'lifeline2' ),
                'options' => lifeline2_Common::lifeline2_currencyList(),
            ),
            array(
                'id'    => 'optCurrencySymbol',
                'type'  => 'text',
                'title' => esc_html__( 'Currency Symbol', 'lifeline2' ),
                'desc'  => esc_html__( 'Enter the currency symbol to set for your site', 'lifeline2' )
            ),
            array(
                'id' => 'donation_cuurency_formate',
                'type' => 'button_set',
                'title' => esc_html__('Currency Formate', 'lifeline2'),
                'desc' => esc_html__('Select the option in which formate you want to enter currency', 'lifeline2'),
                'options' => array(
                    'dollar' => esc_html__('USD Dollar', 'lifeline2'),
                    'select' => esc_html__('Your Selected Currency', 'lifeline2'),
                ),
                'default' => 'dollar'
            ),
            array(
                'id'      => 'time_zone',
                'type'    => 'select',
                'title'   => esc_html__( 'Select Your Time Zone', 'lifeline2' ),
                'select2' => array( 'allowClear' => true ),
                'width'   => '60%',
                'desc'    => esc_html__( 'Please set the timezone for the country which you want to set for your site', 'lifeline2' ),
                'options' => lifeline2_php_timezone(),
            ),
            array(
                'id'       => 'site_favicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Site Favicon', 'lifeline2' ),
                'desc'     => esc_html__( 'Insert site logo image with adjustable size for the logo section', 'lifeline2' ),
                'default'  => array(
                    'url' => lifeline2_URI . 'assets/images/icon.png'
                ),
                'required' => array( 'logo_type', '=', 'image' ),
            ),
            array(
                'id'     => 'layout_settings',
                'type'   => 'section',
                'title'  => esc_html__( 'Layout Settings', 'lifeline2' ),
                'indent' => true
            ),
            array(
                'id'      => 'grid_layout_status',
                'type'    => 'switch',
                'title'   => esc_html__( 'Grid Layout', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to make the site layout in grid version', 'lifeline2' ),
                'default' => false,
            ),
            array(
                'id'            => 'grid_left',
                'type'          => 'slider',
                'title'         => esc_html__( 'Left Padding', 'lifeline2' ),
                'subtitle'      => esc_html__( 'Enter left padding for grid layout', 'lifeline2' ),
                'desc'          => esc_html__( 'Add value for left padding of grid layout', 'lifeline2' ),
                'required'      => array( 'grid_layout_status', '=', '1' ),
                "default"       => 0,
                "min"           => 0,
                "step"          => 5,
                "max"           => 100,
                'display_value' => 'text'
            ),
            array(
                'id'            => 'grid_right',
                'type'          => 'slider',
                'title'         => esc_html__( 'Right Padding', 'lifeline2' ),
                'subtitle'      => esc_html__( 'Enter right padding for grid layout', 'lifeline2' ),
                'desc'          => esc_html__( 'Add value for right padding of grid layout', 'lifeline2' ),
                'required'      => array( 'grid_layout_status', '=', '1' ),
                "default"       => 0,
                "min"           => 0,
                "step"          => 5,
                "max"           => 100,
                'display_value' => 'text'
            ),
            array(
                'id'      => 'boxed_layout_status',
                'type'    => 'switch',
                'title'   => esc_html__( 'Boxed Layout', 'lifeline2' ),
                'desc'    => esc_html__( 'Enable to make the site layout in boxed version', 'lifeline2' ),
                'default' => false,
            ),
            array(
                'id'            => 'boxed_top',
                'type'          => 'slider',
                'title'         => esc_html__( 'Top Margin', 'lifeline2' ),
                'subtitle'      => esc_html__( 'Enter top margin for boxed layout', 'lifeline2' ),
                'desc'          => esc_html__( 'Add value for top maring of boxed layout', 'lifeline2' ),
                'required'      => array( 'boxed_layout_status', '=', '1' ),
                "default"       => 0,
                "min"           => 0,
                "step"          => 5,
                "max"           => 200,
                'display_value' => 'text'
            ),
            array(
                'id'       => 'background_settings',
                'type'     => 'section',
                'title'    => esc_html__( 'Background Settings', 'lifeline2' ),
                'indent'   => true,
                'required' => array( 'boxed_layout_status', '=', '1' ),
            ),
            array(
                'id'      => 'background_type',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Background Type', 'lifeline2' ),
                'desc'    => esc_html__( 'Please select background type to show', 'lifeline2' ),
                //Must provide key => value pairs for options
                'options' => array(
                    'default' => esc_html__( 'Default Patterns', 'lifeline2' ),
                    'custom'  => esc_html__( 'Custom Background', 'lifeline2' ),
                ),
                'default' => 'default'
            ),
            array(
                'id'       => 'patterns',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Select Patterns', 'lifeline2' ),
                'subtitle' => esc_html__( 'Select the pattern image to apply on background for boxed layout', 'lifeline2' ),
                'options'  => array(
                    'none'       => array(
                        'alt' => esc_html__( 'None', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/none.png'
                    ),
                    'pattern-1'  => array(
                        'alt' => esc_html__( 'Pattern 1', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-1.png'
                    ),
                    'pattern-2'  => array(
                        'alt' => esc_html__( 'Pattern 2', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-2.png'
                    ),
                    'pattern-3'  => array(
                        'alt' => esc_html__( 'Pattern 3', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-3.png'
                    ),
                    'pattern-4'  => array(
                        'alt' => esc_html__( 'Pattern 4', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-4.png'
                    ),
                    'pattern-5'  => array(
                        'alt' => esc_html__( 'Pattern 5', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-5.png'
                    ),
                    'pattern-6'  => array(
                        'alt' => esc_html__( 'Pattern 6', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-6.png'
                    ),
                    'pattern-7'  => array(
                        'alt' => esc_html__( 'Pattern 7', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-7.png'
                    ),
                    'pattern-8'  => array(
                        'alt' => esc_html__( 'Pattern 8', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-8.png'
                    ),
                    'pattern-9'  => array(
                        'alt' => esc_html__( 'Pattern 9', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-9.png'
                    ),
                    'pattern-10' => array(
                        'alt' => esc_html__( 'Pattern 10', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-10.png'
                    ),
                    'pattern-11' => array(
                        'alt' => esc_html__( 'Pattern 11', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-11.png'
                    ),
                    'pattern-12' => array(
                        'alt' => esc_html__( 'Pattern 12', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-12.png'
                    ),
                    'pattern-13' => array(
                        'alt' => esc_html__( 'Pattern 13', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-13.png'
                    ),
                    'pattern-14' => array(
                        'alt' => esc_html__( 'Pattern 14', 'lifeline2' ),
                        'img' => ReduxFramework::$_url . 'assets/img/pattern/pattern-14.png'
                    ),
                ),
                'required' => array( 'background_type', '=', 'default' ),
                'default'  => 'none'
            ),
            array(
                'id'       => 'background_options',
                'type'     => 'background',
                'title'    => esc_html__( 'Body Background', 'lifeline2' ),
                'subtitle' => esc_html__( 'Body background with image, color, etc.', 'lifeline2' ),
                'desc'     => esc_html__( 'Body background options to set image or color for boxed layout version.', 'lifeline2' ),
                'required' => array( 'background_type', '=', 'custom' ),
                'default'  => array(
                    'background-color' => '#1e73be',
                )
            ),

            
        );
        return $return;
    }
}