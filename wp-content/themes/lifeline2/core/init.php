<?php

class lifeline2_Module_Init
{

    static public function lifeline2_Init()
    {
        self::lifeline2_Constants();

        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/admin.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/ajax.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/common.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/core.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/format_metaboxes.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/headers.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/helpers.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/panel.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/sidebar.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/vc_settings.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/view.php');
        require_once(trailingslashit(lifeline2_ROOT) . 'core/application/classes/volunteer.php');
        self::lifeline2_Support();
        $GLOBALS['opt'] = lifeline2_get_theme_options();
        //
        lifeline2_View::get_instance()->library('Widgets', false);
        lifeline2_View::get_instance()->library('Shortcodes', false);
        lifeline2_View::get_instance()->library('gallery', false);
        new lifeline2_volunteer();

        lifeline2_Gallery::init();
        lifeline2_ajax::lifeline2_init();
        $opt = lifeline2_get_theme_options();
        if (lifeline2_set($opt, 'time_zone') != "") {
            date_default_timezone_set(lifeline2_set($opt, 'time_zone'));
        }

        if (is_admin()) {
            lifeline2_Admin::lifeline2_Init();

            $files = glob(lifeline2_ROOT . "core/application/library/update/*.php");
            foreach ($files as $file) {
                if (!is_dir($file)) {
                    $info = pathinfo($file);
                    locate_template("core/application/library/update/" . $info['basename'], true, true);
                }
            }
        }

        self::lifeline2_actions();
        add_filter('body_class', array('lifeline2_Module_Init', 'lifeline2_body_classes'));
        add_filter('wp_list_categories', array('lifeline2_Module_Init', 'lifeline2_categories_postcount_filter'));
    }

    static public function lifeline2_Constants()
    {
        $theme = wp_get_theme();
        define('APP', 'lifeline2');
        define('TH', 'Lifeline2');
        if (!defined('WPALCHEMY_MODE_ARRAY')) define('WPALCHEMY_MODE_ARRAY', 'array');

        if (is_child_theme() === false) {
            define('lifeline2_ROOT', get_stylesheet_directory() . '/');
            if (!defined('lifeline2_URI')) {
                define('lifeline2_URI', get_stylesheet_directory_uri() . '/');
            }
        } else {
            define('lifeline2_ROOT', get_template_directory() . '/');
            if (!defined('lifeline2_URI')) {
                define('lifeline2_URI', get_template_directory_uri() . '/');
            }
        }

        define('DOMAIN', strtolower(strip_tags($theme->Author) . '_' . str_replace(' ', '_', $theme->Name)));
        define('lifeline2_VERSION', $theme->Version);
        define('VM', lifeline2_ROOT . 'images/vc/');
        define('lifeline2_DIR', lifeline2_ROOT . 'core/application');
        define('VC', lifeline2_URI . 'assets/images/vc/');
        define('lifeline2_LANG_DIR', lifeline2_ROOT . 'languages');
        define('lifeline2_PAT', lifeline2_URI . 'core/duffers_panel/panel/public/img/patterns/');
        define('lifeline2_FOR', lifeline2_ROOT . 'core/application/library/formats/');
    }

    static protected function lifeline2_Support()
    {
        load_theme_textdomain('lifeline2', get_template_directory() . '/languages');
        add_editor_style();
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('widgets');
        add_theme_support("custom-header");
        add_theme_support("woocommerce");

        add_image_size( 'homepage-thumb size', 370, 270 );

        register_nav_menu('primary-menu', esc_html__('Primary Menu', 'lifeline2'));
        register_nav_menu('footer-menu', esc_html__('Footer Menu', 'lifeline2'));

        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ));
        add_theme_support('post-formats', array(
            'gallery', 'image', 'video', 'audio'
        ));

        add_theme_support('custom-background', apply_filters('sh_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        if (!isset($content_width)) $content_width = 960;
    }

    static public function lifeline2_categories_postcount_filter($variable)
    {
        $variable = str_replace('(', '<span> ', $variable);
        $variable = str_replace(')', ' </span>', $variable);
        return $variable;
    }

    static public function lifeline2_wp_title($title, $sep)
    {

        if (is_feed()) {
            return $title;
        }
        global $page, $paged;
        $title .= esc_attr(get_bloginfo('name', 'display'));
        $site_description = esc_attr(get_bloginfo('description', 'display'));
        if ($site_description && (is_home() || is_front_page())) {
            $title .= " $sep $site_description";
        }
        if (($paged >= 2 || $page >= 2) && !is_404()) {
            $title .= " $sep " . sprintf(esc_html__('Page %s', 'lifeline2'), max($paged, $page));
        }
        return $title;
    }

    static public function lifeline2_body_classes($classes)
    {
        if (is_multi_author()) {
            $classes[] = 'group-blog';
        }
        return $classes;
    }

    static public function lifeline2_actions()
    {
        add_action('widgets_init', array('lifeline2_Sidebars', 'register'));
        add_action('widgets_init', array('lifeline2_Widgets', 'register'));
        add_action('init', array('lifeline2_Module_Init', 'lifeline2_init_actions'));
        add_action('init', array('lifeline2_Shortcodes', 'init'));
        add_action('lifeline2_custom_header', array('lifeline2_Header', 'lifeline2_headers'));
    }

    static public function lifeline2_init_actions()
    {
        add_action('wp_enqueue_scripts', array(lifeline2_Core::get_instance()->view, 'lifeline2_render_styles'));
        add_action('wp_enqueue_scripts', array(lifeline2_Core::get_instance()->view, 'lifeline2_render_scripts'));
        add_action('wp_head', array(lifeline2_Core::get_instance()->view, 'lifeline2_additional_head'));
        add_action('wp_footer', array(lifeline2_Core::get_instance()->view, 'lifeline2_additional_foot'));
    }
}