<?php
get_header();
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
$sidebar        = lifeline2_set( $settings, 'event_cat_sidebar' );
$position       = lifeline2_set( $settings, 'event_cat_layout' );
$span           = (!empty( $sidebar ) && $position != 'full' ) ? 'col-md-9' : 'col-md-12';
$inner_col      = (lifeline2_set( $settings, 'event_cat_column' )) ? lifeline2_set( $settings, 'event_cat_column' ) : 'col-md-6';
$background     = (lifeline2_set( lifeline2_set( $settings, 'event_cat_title_section_bg' ), 'background-image' )) ? 'style="background: url(' . lifeline2_set( lifeline2_set( $settings, 'event_cat_title_section_bg' ), 'background-image' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$title          = (lifeline2_set( $settings, 'event_cat_title' )) ? lifeline2_set( $settings, 'event_cat_title' ) : $queried_object->name;
if ( lifeline2_set( $settings, 'event_cat_show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $title, $background, true ) );
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <div class="charity-events">
                        <div class="row">
                            <div class="masonary">
                            <?php
                            $counter = 0;
                            wp_enqueue_script( array( 'lifeline2_' . 'downcount','lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize' ) );
                            while ( have_posts() ): the_post();
                                $page_meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'event' ); //printr($page_meta);
                                ?>
                                <div class="<?php echo esc_attr( $inner_col ) ?>">
                                    <div class="upcoming-event">
                                        <div class="upcoming-img">
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                            <?php else: ?>
                                                <?php the_post_thumbnail('full'); ?>
                                            <?php endif; ?>
                                            <?php if ( lifeline2_set( $settings, 'event_cat_time_count' ) ): ?>
                                                <div class="overlay-countdown">
                                                    <ul class="countdown-row countdown<?php echo esc_attr( $counter ) ?>">
                                                        <li class="countdown-section">
                                                            <span class="days countdown-amount">00</span>
                                                            <p class="days_ref countdown-period"><?php esc_html_e( 'days', 'lifeline2' ); ?></p>
                                                        </li>
                                                        <li class="countdown-section">
                                                            <span class="hours countdown-amount">00</span>
                                                            <p class="hours_ref countdown-period"><?php esc_html_e( 'hours', 'lifeline2' ); ?></p>
                                                        </li>
                                                        <li class="countdown-section">
                                                            <span class="minutes countdown-amount">00</span>
                                                            <p class="minutes_ref countdown-period"><?php esc_html_e( 'minutes', 'lifeline2' ); ?></p>
                                                        </li>
                                                        <li class="countdown-section">
                                                            <span class="seconds countdown-amount">00</span>
                                                            <p class="seconds_ref countdown-period"><?php esc_html_e( 'seconds', 'lifeline2' ); ?></p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <?php
                                                $jsOutput = "jQuery(document).ready(function ($) {
                                                        $('.countdown" . esc_js( $counter ) . "').downCount({
                                                            date: '" . date( 'm/d/Y h:i:s', lifeline2_set( $page_meta, 'start_date' ) ) . "',
                                                            offset: " . get_option( 'gmt_offset' ) . "
                                                        });
                                                    });";
                                                wp_add_inline_script( 'lifeline2_' . 'downcount', $jsOutput );
                                            endif;
                                            ?>
                                        </div>
                                        <div class="upcoming-detail">
                                            <h4><a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), lifeline2_set( $settings, 'event_cat_title_limit', 30 ) ) ) ?> </a></h4>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $counter++;
                            endwhile;
                            ?>
                        </div>
                        </div>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
