<?php
get_header();
$queried_object = get_queried_object();
$settings       = lifeline2_get_theme_options();
$sidebar        = lifeline2_set( $settings, 'service_cat_sidebar' );
$position       = lifeline2_set( $settings, 'service_cat_layout' );
$span           = (!empty( $sidebar ) && $position != 'full' ) ? 'col-md-9' : 'col-md-12';
$background     = (lifeline2_set( lifeline2_set( $settings, 'service_cat_title_section_bg' ), 'background-image' )) ? 'style="background: url(' . lifeline2_set( lifeline2_set( $settings, 'service_cat_title_section_bg' ), 'background-image' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
$title          = lifeline2_set( $queried_object, 'name' );
if ( lifeline2_set( $settings, 'service_cat_show_title_section' ) ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $title, $background, true ) );
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <div class="services-listing">
                        <?php
                        while ( have_posts() ): the_post();
                            $meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'service' );
                            ?>
                            <div class="service">
                                <div class="service-img">
                                    <?php if ( lifeline2_set( $settings, 'service_cat_icon' ) ): ?>
                                        <span>
                                            <i class="<?php echo esc_attr( lifeline2_Header::lifeline2_get_icon( lifeline2_set( $meta, 'serive_icon' ) ) . esc_attr( lifeline2_set( $meta, 'serive_icon' ) ) ) ?>"></i>
                                        </span>
                                    <?php endif; ?>
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 370, 303, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="service-detail">
                                    <?php if ( lifeline2_set( $settings, 'service_cat_tag_line' ) ): ?>
                                        <span><?php echo esc_html( lifeline2_set( $meta, 'tag_line' ) ) ?></span>
                                    <?php endif; ?>
                                    <h3><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h3>
                                    <p><?php echo balanceTags( lifeline2_Common::lifeline2_contents( get_the_content(), 150 ) ) ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
