<?php
// Template Name: Projects Listing

get_header();
$default = get_option('wp_donation_basic_settings', TRUE);
$options = lifeline2_set($default, 'wp_donation_basic_settings');
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page'); //printr($page_meta);
$settings = lifeline2_get_theme_options();
$sidebar = lifeline2_set($page_meta, 'metaSidebar');
$position = lifeline2_set($page_meta, 'layout');
$span = ( $sidebar && lifeline2_set($settings, 'project_template_column') != 'col-md-3') ? 'col-md-9' : 'col-md-12';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
$inner_col = (lifeline2_set($settings, 'project_template_column')) ? lifeline2_set($settings, 'project_template_column') : 'col-md-3';
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
if (lifeline2_set($page_meta, 'show_title_section')) {
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, true));
}
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
$posts_per_page = (lifeline2_set($settings, 'project_template_pagination_num') && lifeline2_set($settings, 'project_template_pagination')) ? lifeline2_set($settings, 'project_template_pagination_num') : -1;
$args = array(
    'post_type' => 'lif_project',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged
);
$query = new WP_Query($args);
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left' && lifeline2_set($settings, 'project_template_column') != 'col-md-3') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <div class="all-projects">
                        <?php
                        while ($query->have_posts()): $query->the_post();
                            $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'projects');
                            $symbol = lifeline2_set($settings, 'optCurrencySymbol', '$');
                            $donationNeededUsd = (int) (lifeline2_set($meta, 'donation_needed')) ? lifeline2_set($meta, 'donation_needed') : 0;
                            $cuurency_formate = lifeline2_set($settings, 'donation_cuurency_formate');
                            if ($cuurency_formate == 'select'):
                                $donation_needed = $donationNeededUsd;
                            else:
                                $donation_needed = ($donationNeededUsd != 0) ? lifeline2_Common::lifeline2_currencyConvert('usd', $donationNeededUsd) : 0;
                            endif;
                            $donation_collected = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'projects', false);
                            $percent = lifeline2_Common::lifeline2_getDonationTotal(get_the_ID(), 'projects', true);
                            $donation_percentage = $percent;
                            ?>
                            <div class="<?php echo esc_attr($inner_col) ?>">
                                <div class="welfare-project">
                                    <?php if (class_exists('Lifeline2_Resizer')): ?>
                                        <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 638, 638, true)); ?>
                                    <?php else: ?>
                                        <?php the_post_thumbnail('full'); ?>
                                    <?php endif; ?>
                                    <div class="project-info">
                                        <h3><a itemprop="url" href="<?php echo get_the_permalink(get_the_ID()) ?>" title="<?php the_title() ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'project_template_title_limit', 30))) ?></a></h3>
                                        <?php if (lifeline2_set($settings, 'project_template_bar')): ?>
                                            <div class="urgent-progress">
                                                <div class="progress-border">
                                                    <div class="progress">
                                                        <div style="width: <?php echo esc_attr($donation_percentage) ?>%;" class="progress-bar">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="goal raised">
                                                    <?php esc_html_e('Goal', 'lifeline2') ?><span><i><?php echo esc_html($symbol) ?></i><?php echo esc_html($donation_needed) ?></span><i><?php echo esc_attr($donation_percentage) ?>%</i></div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="project-hover">
                                        <h3><a itemprop="url" href="<?php echo get_the_permalink(get_the_ID()) ?>" title="<?php the_title() ?>"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), lifeline2_set($settings, 'project_template_title_limit', 30))) ?></a></h3>
                                        <?php if (lifeline2_set($settings, 'project_template_donation')): ?>
                                            <div class="goal raised">
                                                <?php esc_html_e('Raised ', 'lifeline2') ?>
                                                <span>
                                                    <i><?php echo esc_html($symbol) ?></i><?php echo esc_html(round($donation_collected, 0)) ?>
                                                </span>
                                            </div>
                                            <?php
                                            if (lifeline2_set($settings, 'donation_template_type_general') == 'donation_page_template'):
                                                $url = get_page_link(lifeline2_set($settings, 'donation_button_pageGeneral'));
                                                $queryParams = array('data_donation' => 'projects', 'postId' => get_the_id());
                                                ?>
                                                <a itemprop="url" href="<?php echo esc_url(add_query_arg($queryParams, $url)); ?>" title="">
                                                    <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                                </a>
                                                <?php
                                            elseif (lifeline2_set($settings, 'donation_template_type_general') == 'external_link'):
                                                $url = lifeline2_set($settings, 'donation_button_linkGeneral');
                                                ?>
                                                <a itemprop="url" href="<?php echo esc_url($url) ?>" target="_blank" title="">
                                                    <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                                </a>
                                                <?php
                                            else:
                                                ?>
                                                <?php if (lifeline2_set($options, 'recuring') == 1 || lifeline2_set($options, 'single') == 1): ?>
                                                    <a data-modal="general" data-donation="projects" data-post="<?php echo esc_attr(get_the_id()) ?>" itemprop="url" class="theme-btn donation-modal-box-caller" href="javascript:void(0)" title="">
                                                        <?php echo (lifeline2_set($settings, 'project_template_btn_label')) ? lifeline2_set($settings, 'project_template_btn_label') : esc_html__('DONATE NOW', 'lifeline2'); ?>
                                                    </a>
                                                <?php endif; ?>
                                            <?php
                                            endif;
                                            ?>
                                        <?php endif; ?>
                                    </div>  
                                </div><!-- Welfare Project -->
                            </div>
                            <?php
                        endwhile;
                        if (lifeline2_set($settings, 'project_template_pagination')) {
                            lifeline2_Common::lifeline2_pagination($query->max_num_pages);
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <?php if ($sidebar && $position == 'right' && lifeline2_set($settings, 'project_template_column') != 'col-md-3') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
