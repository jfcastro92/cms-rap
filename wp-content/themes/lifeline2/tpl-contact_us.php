<?php
// Template Name: Contact Us
get_header();
$opt = lifeline2_get_theme_options();
?>
<?php if ( lifeline2_set( $opt, 'show_info_area' ) == 1 ): ?>
    <section>
        <div class="block gray">
            <div class="container">
                <div class="row">
                    <?php if ( lifeline2_set( $opt, 'contact_phone' ) != '' ): ?>
                        <div class="col-md-4 column">
                            <div class="contact-box">
                                <span><i class="fa fa-phone"></i></span>
                                <p><?php echo esc_attr( lifeline2_set( $opt, 'contact_phone' ) ); ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ( lifeline2_set( $opt, 'contact_email' ) != '' || lifeline2_set( $opt, 'contact_website' ) != '' ): ?>
                        <div class="col-md-4 column">
                            <div class="contact-box">
                                <span><i class="fa fa-envelope-o"></i></span>
                                <?php
                                echo (lifeline2_set( $opt, 'contact_email' ) != '') ? '<p>' . esc_attr( lifeline2_set( $opt, 'contact_email' ) ) . '</p>' : '';
                                echo (lifeline2_set( $opt, 'contact_website' ) != '') ? '<p>' . esc_attr( lifeline2_set( $opt, 'contact_website' ) ) . '</p>' : '';
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ( lifeline2_set( $opt, 'contact_address' ) != '' ): ?>
                        <div class="col-md-4 column">
                            <div class="contact-box">
                                <span><i class="fa fa-map-marker"></i></span>
                                <p><?php echo esc_attr( lifeline2_set( $opt, 'contact_address' ) ); ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ( lifeline2_set( $opt, 'show_google_map' ) == 1 ): ?>
    <section>
        <div class="block remove-gap gray">
            <div class="row">
                <div class="col-md-12 column">
                    <div class="map">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    lifeline2_View::get_instance()->lifeline2_enqueue( array( 'lifeline2_' . 'maps' ) );
    $google   = lifeline2_set( $opt, 'contact_google' );
    $exp      = explode( ',', $google, 2 );
    $jsOutput = "jQuery(document).ready(function () {
			lifeline2_google_map(" . esc_attr( lifeline2_set( $exp, '0' ) ) . ", " . esc_attr( lifeline2_set( $exp, '1' ) ) . ");
		});";
    wp_add_inline_script( 'lifeline2_' . 'maps', $jsOutput );
endif;
?>


<?php if ( lifeline2_set( $opt, 'show_contact_form' ) == 1 ): ?>
    <section>
        <div class="block remove-gap gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 column">
                        <div class="contact">
                            <h4 class="simple-title"><?php echo get_the_title() ?></h4>
                            <div id="messages"></div>
                            <form id="lifeline2_contact_form" class="contact-form">
                                <?php wp_nonce_field( LIFELINE2_KEY, 'form_key' ); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input id="wp_lifeline" type="text" placeholder="<?php esc_html_e( 'Name', 'lifeline2' ) ?>" />
                                        <input id="lifeline2_mail" type="email" placeholder="<?php esc_html_e( 'Email', 'lifeline2' ) ?>" />
                                        <input id="lifeline2_subject" type="text" placeholder="<?php esc_html_e( 'Subject', 'lifeline2' ) ?>" />
                                        <input type="hidden" id="lifeline2_reciver_mail" value="<?php echo esc_attr( lifeline2_set( $opt, 'contact_email_form' ) ) ?>" />
                                    </div>
                                    <div class="col-md-6">
                                        <textarea id="lifeline2_msg" placeholder="<?php esc_html_e( 'Message', 'lifeline2' ) ?>"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <input id="widget_contact_form" type="submit" value="<?php esc_html_e( 'Send Message', 'lifeline2' ) ?>" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php endif; ?>
<?php get_footer(); ?>
