<?php
// Template Name: Gallery Listing

get_header();
$page_meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'page'); //printr($page_meta);
$settings = lifeline2_get_theme_options();
$sidebar = lifeline2_set($page_meta, 'metaSidebar');
$position = lifeline2_set($page_meta, 'layout');
$span = (!empty($sidebar) && $position != 'full' ) ? 'col-md-9' : 'col-md-12';
$background = (lifeline2_set($page_meta, 'title_section_bg')) ? 'style="background: url(' . lifeline2_set($page_meta, 'title_section_bg') . ') repeat scroll 50% 422.28px transparent;"' : '';
$page_title = (lifeline2_set($page_meta, 'banner_title')) ? lifeline2_set($page_meta, 'banner_title') : get_the_title(get_the_ID());
if (lifeline2_set($page_meta, 'show_title_section')) {
    echo balanceTags(lifeline2_Common::lifeline2_page_top_section($page_title, $background, true));
}
if (class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
$cols = (lifeline2_set($settings, 'gallery_template_columns')) ? lifeline2_set($settings, 'gallery_template_columns') : 'col-md-4';
$size = array(array('width' => 387, 'height' => 415), array('width' => 770, 'height' => 562), array('width' => 387, 'height' => 415), array('width' => 387, 'height' => 415), array('width' => 770, 'height' => 562), array('width' => 770, 'height' => 562));
wp_enqueue_script(array('lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize'));

$posts_per_page = (lifeline2_set($settings, 'gallery_template_pagination_num') && lifeline2_set($settings, 'gallery_template_pagination')) ? lifeline2_set($settings, 'gallery_template_pagination_num') : -1;
$args = array(
    'post_type' => 'lif_gallery',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged' => $paged
);
$query = new WP_Query($args);
?>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ($sidebar && $position == 'left') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr($span); ?> column">
                    <div class="gallery-page<?php echo (lifeline2_set($settings, 'gallery_template_style') == 'grid-view-title' || lifeline2_set($settings, 'gallery_template_style') == 'grid-view-notitle') ? ' masonary' : ''; ?>">
                        <?php
                        $i = 0;
                        while ($query->have_posts()): $query->the_post();
                            $meta = lifeline2_Common::lifeline2_post_data(get_the_ID(), 'gallery');
                            $gal_video = get_post_meta(get_the_ID(), 'lifeline2_videos', false);
                            $gal_img = get_post_meta(get_the_ID(), 'lifeline2_gal_ids', true);
                            $converArray = explode(',', $gal_img);
                            ?>
                            <?php if (lifeline2_set($settings, 'gallery_template_style') == 'grid-view-title'): ?>
                                <?php $title_limit = (lifeline2_set($settings, 'gallery_template_title_limit')) ? lifeline2_set($settings, 'gallery_cat_title_limit') : 30; ?>
                                <div class="<?php echo esc_attr($cols); ?>">
                                    <div class="gallery">
                                        <div class="gallery-img">
                                            <a title="<?php the_title(); ?>" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url">
                                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $size[$i]['width'], $size[$i]['height'], true)); ?>
                                                <?php else: ?>
                                                    <?php the_post_thumbnail('full'); ?>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="gallery-detail">
                                            <?php if (lifeline2_set($settings, 'gallery_template_categories')): ?>
                                                <ul>
                                                    <?php lifeline2_Common::lifeline2_get_post_categories(get_the_ID(), 'gallery_category', '', 'list'); ?>
                                                </ul>
                                            <?php endif; ?>
                                            <h3><a title="" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), $title_limit)); ?></a></h3>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif (lifeline2_set($settings, 'gallery_template_style') == 'grid-view-notitle'): ?>
                                <div class="<?php echo esc_attr($cols); ?>">
                                    <div class="gallery">
                                        <div class="gallery-img">
                                            <a title="<?php the_title(); ?>" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url">
                                                <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                    <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $size[$i]['width'], $size[$i]['height'], true)); ?>
                                                <?php else: ?>
                                                    <?php the_post_thumbnail('full'); ?>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif (lifeline2_set($settings, 'gallery_template_style') == 'col-md-6' || lifeline2_set($settings, 'gallery_template_style') == 'col-md-4' || lifeline2_set($settings, 'gallery_template_style') == 'col-md-3'): ?>
                                <?php $title_limit = (lifeline2_set($settings, 'gallery_template_title_limit')) ? lifeline2_set($settings, 'gallery_cat_title_limit') : 30; ?>
                                <?php
                                if (lifeline2_set($settings, 'gallery_template_style') == 'col-md-6'):
                                    $grids_cols = 'col-md-6';
                                    $img_width = 570;
                                    $img_height = 360;
                                elseif (lifeline2_set($settings, 'gallery_template_style') == 'col-md-3'):
                                    $grids_cols = 'col-md-3';
                                    $img_width = 270;
                                    $img_height = 415;
                                elseif (lifeline2_set($settings, 'gallery_template_style') == 'col-md-4'):
                                    $grids_cols = 'col-md-4';
                                     $img_width = 370;
                                    $img_height = 415;
                                endif;
                                ?>
                                <div class="<?php echo esc_attr($grids_cols); ?>">
                                    <div class="gallery">

                                        <a title="<?php the_title(); ?>" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url">
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                                <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), $img_width, $img_height, true)); ?>
                                            <?php else: ?>
                                                <?php the_post_thumbnail('full'); ?>
                                            <?php endif; ?>
                                        </a>

                                        <div class="gallery-detail">
                                            <?php if (lifeline2_set($settings, 'gallery_template_categories')): ?>
                                                <ul>
                                                    <?php lifeline2_Common::lifeline2_get_post_categories(get_the_ID(), 'gallery_category', '', 'list'); ?>
                                                </ul>
                                            <?php endif; ?>
                                            <h3><a title="" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" itemprop="url"><?php echo balanceTags(lifeline2_Common::lifeline2_character_limiter(get_the_title(get_the_ID()), $title_limit)); ?></a></h3>
                                        </div>
                                    </div>
                                </div>                            
                            <?php else: ?>
                                <div class="modern-gallery">
                                    <div class="mod-gallery-img">
                                        <?php if (class_exists('Lifeline2_Resizer')): ?>
                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 768, 356, true)); ?>
                                        <?php else: ?>
                                            <?php the_post_thumbnail('full'); ?>
                                        <?php endif; ?>

                                        <?php if (lifeline2_set($settings, 'gallery_template_video_count') || lifeline2_set($settings, 'gallery_template_image_count')): ?>
                                            <div class="mod-gallery-info">
                                                <?php if (lifeline2_set($settings, 'gallery_template_image_count')): ?>
                                                    <a title=""><i class="ti-instagram"></i><?php echo count($converArray) ?> <?php esc_html_e('Images', 'lifeline2') ?></a>
                                                <?php endif; ?>
                                                <?php if (lifeline2_set($settings, 'gallery_template_video_count') && count($gal_video) != 0 ) : ?>
                                                    <a title=""><i class="fa fa-play-circle"></i><?php echo count($gal_video) ?> <?php esc_html_e('Videos', 'lifeline2') ?></a>
                                              <?php //printr($settings); ?>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="mod-gallery-detail">
                                        <h3><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h3>
                                        <?php if (lifeline2_set($settings, 'gallery_template_date')): ?>    
                                            <ul class="meta">
                                                <?php if (lifeline2_set($settings, 'gallery_template_date')) : ?><li content="<?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr(get_the_date(get_option('date_format', get_the_ID()))); ?></li><?php endif; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $i++; ?>
                            <?php if ($i == 6) $i = 0; ?>
                        <?php endwhile; ?>
                        <?php
                        if (lifeline2_set($settings, 'gallery_template_pagination')) {
                            lifeline2_Common::lifeline2_pagination($query->max_num_pages);
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <?php if ($sidebar && $position == 'right') : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar($sidebar); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
