jQuery(document).ready(function ($) {
    $('#newsletter-form-submit').on('click', function () {
        var thiss = this;
        var email = jQuery("#newsletter-email").val();
        var data = 'email=' + email + '&action=lifeline2_newsletter_module';
        var button_id = '#' + $(this).attr('id');
        jQuery.ajax({
            type: "post",
            url: admin_url,
            data: data,
            beforeSend: function () {
                $(button_id).after('<img src="' + lifeline2_url + 'assets/images/ajax-loader.gif" class="loader" />').attr('disabled', 'disabled');
            },
            success: function (response) {
                $('form#newsletter-form img.loader').fadeOut('slow', function () {
                    $(this).remove();
                });
                $(button_id).removeAttr('disabled');
                $('form#newsletter-form #response-message').html(response);
                setTimeout(function(){ 
                   $("#response-message").slideUp(100); }, 3000);
            }
        });
        return false;
    });
});