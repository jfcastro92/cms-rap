

(function ($) {
    "use strict";
    var lifeline2 = {
	count: 0,
	tweets: function (options, selector) {

	    options.action = 'lifeline2_ajax_callback';

	    $.ajax({
		url: ajaxurl,
		type: 'POST',
		data: options,
		dataType: "json",
		success: function (res) {

		    var reply = res;
		    var html = '';
                if (options.template === 'blockquote') {
                    $.each(reply, function (k, element) {
                        if(element.user != undefined) {
                        var date = new Date(element.created_at);
                        html += '<div class="tweet"><blockquote><i class="fa fa-twitter"></i>';html += '<a href="' + element.user.url + '">@' + element.user.name + '</a>';
                        html += element.text;
                        html += '</blockquote></div>';
                        } else {
                        html += '<div class="tweeterror alert alert-danger">Invalid Twitter API Details</div>';
                        }
                });
            }
		    $(selector).html(html);


		}
	    });

	},
    };

    $.fn.tweets = function (options) {

	var settings = {
	    screen_name: 'wordpress',
	    count: 3,
	    template: 'blockquote'
	};
	options = $.extend(settings, options);

	lifeline2.tweets(options, this);
    };

    function timeSince(date) {

	var seconds = Math.floor((new Date() - date) / 1000);

	var interval = Math.floor(seconds / 31536000);

	if (interval > 1) {
	    return interval + " years";
	}
	interval = Math.floor(seconds / 2592000);
	if (interval > 1) {
	    return interval + " months";
	}
	interval = Math.floor(seconds / 86400);
	if (interval > 1) {
	    return interval + " days";
	}
	interval = Math.floor(seconds / 3600);
	if (interval > 1) {
	    return interval + " hours";
	}
	interval = Math.floor(seconds / 60);
	if (interval > 1) {
	    return interval + " minutes";
	}
	return Math.floor(seconds) + " seconds";
    }

    function link_conversion(text) {
	var replaceText, replacedText, replacePattern1, replacePattern2;

	replacePattern1 = /(\b(https?):\/\/[-A-Z0-9+&amp;@#\/%?=~_|!:,.;]*[-A-Z0-9+&amp;@#\/%=~_|])/ig;
	replacedText = text.replace(replacePattern1, '</h3><a class="colored-link-1" title="$1" href="$1" target="_blank">$1</a>');

	replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
	replacedText = replacedText.replace(replacePattern2, '$1</h3><a class="colored-link-1" href="http://$2" target="_blank">$2</a>');


	return replacedText;
    }

    function link_conversion1(text) {
	var replaceText, replacedText, replacePattern1, replacePattern2;

	replacePattern1 = /(\b(https?):\/\/[-A-Z0-9+&amp;@#\/%?=~_|!:,.;]*[-A-Z0-9+&amp;@#\/%=~_|])/ig;
	replacedText = text.replace(replacePattern1, '<a class="primary-color" title="$1" href="$1" target="_blank">$1</a>');

	replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
	replacedText = replacedText.replace(replacePattern2, '$1<a class="primary-color" href="http://$2" target="_blank">$2</a>');


	return replacedText;
    }


})(jQuery);




