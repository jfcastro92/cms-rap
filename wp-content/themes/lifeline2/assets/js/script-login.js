jQuery(document).ready(function ($) {

    $('form#loginfom').on('submit', function (e) {

        $('form#loginfom .message-box').show().html('<p>' + lifeline2_login_form_object.loadingmessage + '</p>');
        var redirect_url = $(this).children('.redirect_url').val();
        var action = $(this).attr('action');
        var button = $(this).find('input[type="submit"]');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: action,
            data: $(this).serialize(),
            beforeSend: function () {
                $(button).prop('disable', true);
            },
            success: function (data) {
                $(button).prop('disable', false);
                $('form#loginfom .message-box').html(data.message);
                if (data.loggedin === true) {
                    window.location.href = redirect_url;
                }
            }
        });
        e.preventDefault();
    });

    $('form#registration-form').on('submit', function (e) {
        $('form#registration-form .message-box').show().html('<p>' + lifeline2_login_form_object.loadingmessage + '</p>');
        var action = $(this).attr('action');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: action,
            data: $(this).serialize(),
            success: function (data) {
                $('form#registration-form .message-box').html(data.message);
            }
        });
        e.preventDefault();
    });


});
