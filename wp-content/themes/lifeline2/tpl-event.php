<?php
// Template Name: Event Listing
get_header();
$page_meta  = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'page' ); //printr($page_meta);
$settings   = lifeline2_get_theme_options();
$sidebar    = lifeline2_set( $page_meta, 'metaSidebar' );
$position   = lifeline2_set( $page_meta, 'layout' );
$span       = ( $sidebar && lifeline2_set( $settings, 'event_template_columns' ) != 'col-md-3' ) ? 'col-md-9' : 'col-md-12';
$inner_col  = (lifeline2_set( $settings, 'event_template_columns' )) ? lifeline2_set( $settings, 'event_template_columns' ) : 'col-md-3';
$page_title = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
$background = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
if ( lifeline2_set( $page_meta, 'show_title_section' ) ) {
    if(lifeline2_set( $settings, 'event_template_breadcrumb' )){
	    echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, TRUE ) );
    } else {
	    echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
    }
}
if(class_exists('lifeline2_Resizer'))
    $img_obj = new lifeline2_Resizer();
$posts_per_page = (lifeline2_set( $settings, 'event_template_pagination_num' ) && lifeline2_set( $settings, 'event_template_pagination' )) ? lifeline2_set( $settings, 'event_template_pagination_num' ) : -1;
$args           = array(
    'post_type'      => 'lif_event',
    'post_status'    => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged'          => $paged
);
$query          = new WP_Query( $args );
$title_limit    = (lifeline2_set( $settings, 'event_template_title_limit' )) ? lifeline2_set( $settings, 'event_template_title_limit' ) : 30;
?>
<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <?php if ( $sidebar && $position == 'left' && lifeline2_set( $settings, 'event_template_columns' ) != 'col-md-3' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
                <div class="<?php echo esc_attr( $span ); ?> column">
                    <div class="charity-events">
                        <div class="row">
                            <div class="masonary">
                            <?php
                            $counter = 0;
                            wp_enqueue_script( array( 'lifeline2_' . 'downcount','lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize' ) );
                            while ( $query->have_posts() ): $query->the_post();

                                $event_meta = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'event' );
                                ?>
                                <div class="<?php echo esc_attr( $inner_col ) ?>">
                                    <div class="upcoming-event">
                                        <div class="upcoming-img">
                                            <?php if (class_exists('Lifeline2_Resizer')): ?>
                                            <a href="<?php echo get_permalink();?>">
                                            <?php echo balanceTags($img_obj->lifeline2_resize(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full'), 770, 562, true)); ?>
                                            <?php else: ?>
                                                <?php the_post_thumbnail('full'); ?>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ( lifeline2_set( $settings, 'event_template_time_count' ) ): ?>
                                                <div class="overlay-countdown">
                                                    <ul class="countdown-row countdown<?php echo esc_attr( $counter ); ?>">
                                                        <li class="countdown-section">
                                                            <span class="days countdown-amount">00</span>
                                                            <p class="days_ref countdown-period"><?php esc_html_e( 'days', 'lifeline2' ); ?></p>
                                                        </li>

                                                        <li class="countdown-section">
                                                            <span class="hours countdown-amount">00</span>
                                                            <p class="hours_ref countdown-period"><?php esc_html_e( 'hours', 'lifeline2' ); ?></p>
                                                        </li>

                                                        <li class="countdown-section">
                                                            <span class="minutes countdown-amount">00</span>
                                                            <p class="minutes_ref countdown-period"><?php esc_html_e( 'minutes', 'lifeline2' ); ?></p>
                                                        </li>

                                                        <li class="countdown-section">
                                                            <span class="seconds countdown-amount">00</span>
                                                            <p class="seconds_ref countdown-period"><?php esc_html_e( 'seconds', 'lifeline2' ); ?></p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <?php
                                                if ( lifeline2_set( $event_meta, 'start_date' ) ):
                                                    $jsOutput = "jQuery(document).ready(function ($) {
                                                            $('.countdown-row.countdown" . esc_js( $counter ) . "').downCount({
                                                                date: '" . date( 'm/d/Y h:i:s', lifeline2_set( $event_meta, 'start_date' ) ) . "',
                                                                offset:  '-7' 
                                                            });
                                                        });";
                                                    wp_add_inline_script( 'lifeline2_' . 'downcount', $jsOutput );
                                                endif;
                                            endif;
                                            ?>
                                        </div><!-- Upcoming Event -->
                                        <div class="upcoming-detail">
                                            <h4><a itemprop="url" href="<?php echo esc_url( get_the_permalink() ) ?>" title="<?php the_title() ?>"><?php echo balanceTags( lifeline2_Common::lifeline2_character_limiter( get_the_title( get_the_ID() ), $title_limit ) ); ?> </a></h4>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $counter++;
                            endwhile;
                            if ( lifeline2_set( $settings, 'event_template_pagination' ) ) {
                                lifeline2_Common::lifeline2_pagination( $query->max_num_pages );
                            }
                            wp_reset_postdata();
                            ?>
                        </div>
                        </div>
                    </div>
                </div>
                <?php if ( $sidebar && $position == 'right' && lifeline2_set( $settings, 'event_template_columns' ) != 'col-md-3' ) : ?>
                    <div class="col-md-3 sidebar">
                        <?php dynamic_sidebar( $sidebar ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
