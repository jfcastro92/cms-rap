<?php
get_header();
while ( have_posts() ) : the_post();
    $settings    = lifeline2_get_theme_options();
    $page_meta   = lifeline2_Common::lifeline2_post_data( get_the_ID(), 'gallery' );
    $sidebar     = (lifeline2_set( $page_meta, 'metaSidebar' )) ? lifeline2_set( $page_meta, 'metaSidebar' ) : '';
    $layout      = (lifeline2_set( $page_meta, 'layout' )) ? lifeline2_set( $page_meta, 'layout' ) : '';
    $page_title  = (lifeline2_set( $page_meta, 'banner_title' )) ? lifeline2_set( $page_meta, 'banner_title' ) : get_the_title( get_the_ID() );
    $background  = (lifeline2_set( $page_meta, 'title_section_bg' )) ? 'style="background: url(' . lifeline2_set( $page_meta, 'title_section_bg' ) . ') repeat scroll 50% 422.28px transparent;"' : '';
    $top_section = lifeline2_set( $page_meta, 'show_title_section' );
	$breadcrumb_section = lifeline2_set( $page_meta, 'banner_breadcrumb' );
    $span        = ($sidebar && ($layout == 'left' || $layout == 'right')) ? 'col-md-9' : 'col-md-12';
    //$settings = lifeline2_Common::lifeline2_array_opt('lifeline2_template_settings');
    if (class_exists('lifeline2_Resizer'))
        $img_obj = new lifeline2_Resizer();
    $gal_video   = get_post_meta( get_the_ID(), 'lifeline2_videos', true );
    $gal_img     = get_post_meta( get_the_ID(), 'lifeline2_gal_ids', true );
    $converArray = ($gal_img) ? explode( ',', $gal_img ) : array();
    wp_enqueue_script( array( 'lifeline2_' . 'isotope', 'lifeline2_' . 'isotope-initialize', 'lifeline2_' . 'jquery-poptrox' ) );
    if ( $top_section ) echo balanceTags( lifeline2_Common::lifeline2_page_top_section( $page_title, $background, $breadcrumb_section, true ) );
    ?>
    <section itemscope itemtype="http://schema.org/BlogPosting">
        <div class="block gray">
            <div class="container">
                <div class="row">
                    <?php if ( $sidebar && $layout == 'left' && is_active_sidebar( $sidebar ) ) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    <?php endif; ?>
                    <div class="<?php echo esc_attr( $span ); ?> column">
                        <div class="gallery-detail-page">
                            <div class="gallery-detail-top">
                                <?php if ( lifeline2_set( $settings, 'gallery_detail_show_date' ) || lifeline2_set( $settings, 'gallery_detail_show_author' ) ): ?>
                                    <ul class="meta">
                                        <?php if ( lifeline2_set( $settings, 'gallery_detail_show_date' ) ): ?><li content="<?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?>" itemprop="datePublished"><i class="ti-calendar"></i><?php echo esc_attr( get_the_date( get_option( 'date_format', get_the_ID() ) ) ); ?></li><?php endif; ?>
                                        <?php if ( lifeline2_set( $settings, 'gallery_detail_location' ) && lifeline2_set( $page_meta, 'location' ) ) : ?><li><i class="ti-location-pin"></i><?php echo esc_html( lifeline2_set( $page_meta, 'location' ) ); ?></li><?php endif; ?>
                                        <?php if ( lifeline2_set( $settings, 'gallery_detail_show_author' ) ): ?><li itemprop="author"><i class="ti-user"></i> <?php esc_html_e( 'By', 'lifeline2' ); ?> <a title="<?php ucwords( the_author_meta( 'display_name' ) ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" itemprop="url"><?php ucwords( the_author_meta( 'display_name' ) ); ?></a></li><?php endif; ?>
                                    </ul>
                                <?php endif; ?>
                                <h2 class="gallery-detail-title"><?php the_title(); ?></h2>
                                <?php if (lifeline2_set($settings, 'gallery_detail_show_social_share')): ?>
                                    <?php $social_icons = lifeline2_set($settings, 'gallery_detail_social_media'); ?>
                                    <?php if (!empty($social_icons)): ?>
                                        <div class="share-this">
                                            <?php echo (lifeline2_set($settings, 'gallery_detail_show_social_share_title')) ? '<span>' . lifeline2_set($settings, 'gallery_detail_show_social_share_title') . '</span>' : ''; ?>
                                            <?php
                                            foreach ($social_icons as $k => $v) {
                                                if ($v == '')
                                                    continue;
                                                lifeline2_Common::lifeline2_social_share_output($k);
                                            }
                                            ?>

                                        </div><!-- Share This -->
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div><!-- Gallery Detail Top -->
                            <?php if ( count( $gal_video ) > 0 && count( $converArray ) > 0 ): ?>
                                <div class="options">
                                    <div class="option-combo">
                                        <ul id="filter" class="option-set" data-option-key="filter">
                                            <?php if ( !empty( $gal_video ) && !empty( $converArray ) ): ?>
                                                <li><a href="#showall" data-option-value="*" class="selected"><?php esc_html_e( 'Show all', 'lifeline2' ) ?></a></li>
                                                <?php
                                            endif;
                                            if ( !empty( $gal_video ) ):
                                                $selected = (empty( $converArray )) ? 'selected' : '';
                                                ?>
                                                <li><a href="#devepoments" class="<?php echo esc_attr( $selected ) ?>" data-option-value=".videos"><?php esc_html_e( 'Videos', 'lifeline2' ) ?></a></li>
                                            <?php endif; ?>
                                            <?php
                                            if ( !empty( $converArray ) ):
                                                $selected = (empty( $gal_video )) ? 'selected' : '';
                                                ?>
                                                <li><a href="#mockup" class="<?php echo esc_attr( $selected ) ?>" data-option-value=".images"><?php esc_html_e( 'Images', 'lifeline2' ) ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="gallery-filters">
                                <div class="row">
                                    <div class="masonary">
                                        <?php
                                   if (class_exists('Lifeline2_Resizer')):
                                        foreach ( $converArray as $img ) {
                                            $imgSrc  = wp_get_attachment_image_src( $img, 'homepage-thumb size' );
                                            $fullSrc = wp_get_attachment_image_src( $img, 'full' );
                                            //printr($imgSrc);
                                            ?>
                                            <div class="col-md-4 images">
                                                <div class="gallery-box lightbox">
                                                    <span><i class="ti-instagram"></i></span>
                                                    <img src="<?php echo esc_url( lifeline2_set( $imgSrc, '0' ) ) ?>" alt="" />
                                                    <a href="<?php echo esc_url( lifeline2_set( $fullSrc, '0' ) ) ?>" title="">+</a>
                                                </div><!-- Gallery Box -->
                                            </div>
                                            <?php
                                        }
                                        endif;
                                        if ( !empty( $gal_video ) ) {
                                            foreach ( $gal_video as $v ) {
                                                $video_id = lifeline2_set( $v, 'id' );
                                               // printr($gal_video);
                                                $link     = '';
                                                switch ( lifeline2_set( $v, 'source' ) ) {
                                                    case 'dailymotion': {
                                                            $link = "http://dailymotion.com/" . $video_id;
                                                            break;
                                                        }
                                                    case 'vimeo': {
                                                            $link = "http://vimeo.com/" . $video_id;
                                                            break;
                                                        }
                                                    case 'youtube': {
                                                            $link = "http://youtu.be/" . $video_id;
                                                            break;
                                                        }
                                                }
                                                ?>
                                                <div class="col-md-4 videos">
                                                    <div class="gallery-box lightbox">
                                                        <span><i class="fa fa-play-circle"></i></span>
                                                        <img src="<?php echo esc_url( lifeline2_set( $v, 'thumb' ) ) ?>" alt="" />
                                                        <a href="<?php echo esc_url( $link ); ?>" title="">+</a>
                                                    </div><!-- Gallery Box -->
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div><!-- Gallery Filters -->

                            <?php the_content() ?>
                        </div><!-- Gallery Detail -->

                    </div>
                    <?php if ( $sidebar && $layout == 'right' && is_active_sidebar( $sidebar ) ) : ?>
                        <aside class="col-md-3 column sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    $jsOutput = "jQuery(document).ready(function ($) {
            $('.gallery-filters').poptrox({
                usePopupCaption: false,
                usePopupNav:true
                
            });
        });";
    wp_add_inline_script( 'lifeline2_' . 'jquery-poptrox', $jsOutput );
endwhile;
get_footer();
