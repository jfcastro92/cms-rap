<?php
/*
  Plugin Name: WordPress Simple Donation System
  Description: Pin Images in your posts when you publish it
  Author: Webinane
  Author URI: http://themeforest.net/user/webinane
  Version: 3.6
  Text Domain: sh_pinto
 */

/* Copyright 2012-2014  WordPress Simple Donation System (email : qaisarali789@gmail.com) */

include 'functions.php';

$default = get_option('wp_donation_basic_settings', TRUE);
$options = WPD_Common::wpd_set($default, 'wp_donation_basic_settings');
if (WPD_Common::wpd_set($options, 'lang_bar') == 1) {

    function sh_theme_localized($locale)
    {
        $options = get_option('wp_donation_basic_settings');
        $lang = WPD_Common::wpd_set($options, 'wp_donation_basic_settings');
        $file = WPD_Common::wpd_set($lang, 'lang_');
        $locale = ($file) ? $file : $locale;

        if (isset($_GET['l'])) {
            printr(esc_attr($_GET['l']));
        }

        return $locale;
    }

    add_filter('locale', 'sh_theme_localized', 10);
}

add_action('plugins_loaded', 'sh_wp_simple_donation_setup');

function sh_wp_simple_donation_setup()
{
    load_theme_textdomain(WPD_NAME, WPD_ROOT . '/languages');
}

//register_activation_hook(__FILE__, 'wpd_donation_table');
add_action('plugins_loaded', 'wpd_donation_table');
function wpd_donation_table($networkwide)
{
    global $wpdb;
    $networkwide = 1;
    if (function_exists('is_multisite') && is_multisite()) {
        if ($networkwide) {
            $old_blog = $wpdb->blogid;
            $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
            foreach ($blogids as $blog_id) {
                switch_to_blog($blog_id);
                wpd_donation_table_structure();
            }
            switch_to_blog($old_blog);

            return;
        }
    } else {
        wpd_donation_table_structure();
    }
}

function wpd_donation_table_structure()
{
    global $wpdb;
    global $charset_collate;
    $table_name = $wpdb->prefix . "wpd_easy_donation";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql_create_table = "CREATE TABLE IF NOT EXISTS $table_name (
							  `id` int(11) NOT NULL AUTO_INCREMENT,
							  `amount` varchar(50) NOT NULL,
							  `cycle` varchar(50) NOT NULL,
							  `cycle_time` varchar(50) NOT NULL,
							  `f_name` varchar(50) NOT NULL,
							  `l_name` varchar(50) NOT NULL,
							  `email` varchar(50) NOT NULL,
							  `contact` varchar(50) NOT NULL,
							  `address` varchar(200) NOT NULL,
							  `type` varchar(50) NOT NULL,
							  `source` varchar(50) NOT NULL,
							  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `transaction_id` varchar(100) NOT NULL,
                              `currency` varchar(10) NOT NULL,
                              `donation_for` varchar(50) NOT NULL,
                              `post_id` varchar(50) NOT NULL,
							  PRIMARY KEY (`id`)
						 ) $charset_collate; ";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql_create_table);
    }
}