<?php

class WPD_Donation_Module_Init
{

    static public function WPD_Init()
    {
        self::WPD_Constants();

        $files__[] = __DIR__ . "/application/classes/admin.php";
        $files__[] = __DIR__ . "/application/classes/ajax.php";
        $files__[] = __DIR__ . "/application/classes/common.php";
        $files__[] = __DIR__ . "/application/classes/currencies.php";
        $files__[] = __DIR__ . "/application/classes/shortcode.php";
        $files__[] = __DIR__ . "/application/classes/view.php";
        foreach ($files__ as $file) {
            if (!is_dir($file)) {
                require_once($file);
            }
        }
        WPD_View::get_instance()->library('Donations', FALSE);
        require_once WPD_ROOT . 'application/library/Stripe/vendor/autoload.php';
        //require_once WPD_ROOT . 'application/library/authorized/autoload.php';
        require_once WPD_ROOT . "application/library/2checkout/Twocheckout.php";
        require_once WPD_ROOT . "application/library/braintree/Braintree.php";
        //require_once WPD_ROOT . "application/library/bluepay/Bluepay.php";
        WPD_View::get_instance()->library('Stripe_', FALSE);
        add_action('init', array(WPD_Shortcode::wpd_singleton(), 'init'));

        if (is_admin()) {
            WPD_Admin::WPD_Init();
            self::WPD_actions();

            return;
        }

        self::WPD_actions();
    }

    static public function WPD_Constants()
    {
        if (!function_exists('get_plugin_data')) {
            include(ABSPATH . "wp-admin/includes/plugin.php");
        }

        $plugin = get_plugin_data(__DIR__ . '/wp-simple-donation-system.php');
        define('PLUGIN_PREFIX', 'WPD_');
        define('WPD_DOMAIN', strtolower(strip_tags($plugin['Author']) . '_' . $plugin['Name']));
        define('WPD_NAME', strtolower(strip_tags($plugin['Author']) . '_' . $plugin['Name']));
        define('WPD_VERSION', $plugin['Version']);
        define('WPD_ROOT', plugin_dir_path(__FILE__));
        define('WPD_URI', plugin_dir_url(__FILE__));
        define('WPD_DIR', dirname(__FILE__) . '/application');
        define('WPD_LANG_DIR', WPD_ROOT . 'languages');
    }

    static public function WPD_actions()
    {
        add_action('init', array(__CLASS__, 'init_actions'));
    }

    static public function init_actions()
    {
        add_action('wp_enqueue_scripts', array(WPD_View::get_instance(), 'render_styles'));
        add_action('wp_enqueue_scripts', array(WPD_View::get_instance(), 'render_scripts'));
        add_action('wp_head', array(WPD_View::get_instance(), 'additional_head'));
    }
}