jQuery(document).ready(function ($) {
    "use strict";


    $('.date-range input').datepick();
    var wpdonation_button = $(".wpdonation-button");

    $(".wpdonation-button").click(function () {
        $("#donation_amount").val("");
        $(wpdonation_button).removeClass("active");
        $(this).addClass("active");
        var amount_val = $(this).html();
        $("#donation_amount").val(amount_val);
        return false;
    });

    jQuery(".wp-donation-system select").select2();

    function donationPreIn() {
        jQuery('div.donation-modal-wraper').fadeIn(500);
        jQuery('div.donation-modal-preloader').fadeIn(500);
    }

    function donationPreOut() {
        jQuery('div.donation-modal-wraper').fadeOut(500);
        jQuery('div.donation-modal-preloader').fadeOut(500);
    }
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="images/' + state.element.value.toLowerCase() + '.jpg" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
    }
    ;

    jQuery(".language-selector select").select2({
        templateResult: formatState,
        templateSelection: formatState
    });


    /* donate now */
    $('a#reccuring_, a#payment_').on('click', function () {
        setTimeout(function () {
            $('a#credit_, a#paypal_').attr('aria-expanded', 'false');
        }, 1000);
    });

    $('button#wpd_donate_now', '').on('click', function () {
        jQuery("#rec_alerts").empty();
        jQuery("div.wpdonation-box > div#no_selection").empty();

        var amount = $('#donation_amount').val();

        var recuring = $('a#reccuring_').attr("aria-expanded");
        var one_time = $('a#payment_').attr('aria-expanded');

        var rec_credit = $('#credit_').attr('aria-expanded');
        var rec_paypal = $('#paypal_').attr('aria-expanded');

        var single_credit = $('#credit2_').attr('aria-expanded');
        var single_authorized = $('#credit2_').attr('aria-expanded');
        var single_bank = $('#transfer2_').attr('aria-expanded');
        var single_paypal = $('#paypal2_').attr('aria-expanded');
        var single_paymaster = $('#paymaster_').attr('aria-expanded');
        var single_yandex = $('#yandex_').attr('aria-expanded');
        var single_braintree = $('#braintree_').attr('aria-expanded');
        var single_bluepay = $('#bluepay_').attr('aria-expanded');
        var single_conekta = $('#conekta_').attr('aria-expanded');
        var single_paystack = $('#paystack_').attr('aria-expanded');
        var single_payumoney = $('#payumoney_').attr('aria-expanded');
        var single_quickpay = $('#quickpay_').attr('aria-expanded');
        var single_cardcom = $('#cardcom_').attr('aria-expanded');


        if (typeof recuring === 'undefined' && typeof one_time === 'undefined') {
            $('div.wpdonation-box > div#no_selection').html("<div class='alert alert-warning'>Please Select Reccuring Or One Time Payment</div>");
            setTimeout(function () {
                jQuery("div.wpdonation-box > div#no_selection").fadeOut('slow').empty();
            }, 15000);
        } else {
            if (recuring == 'true' && rec_credit == 'true') {
                if (amount != '') {
                    jQuery('div.overlay').fadeIn(500);
                    if (stripe_key != '') {
                        jQuery('div.overlay').fadeIn(500);

                        var rec_cycle = $('#rec_cycle').val();
                        var rec_cycle_time = $('#rec_cycle_time').val();
                        var card_no = $('#rec_card_no').val();
                        var exp_month = $('#rec_card-expiry-month').val();
                        var exp_year = $('#rec_card-expiry-year').val();
                        var cvc = $('#rec_card_cvc').val();

                        var f_name = $('#f_name').val();
                        var l_name = $('#l_name').val();
                        var email = $('#email').val();
                        var contact_no = $('#contact_no').val();
                        var address = $('#address').val();
                        var curr = jQuery('#currency_').val();

                        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                        var messages = ['Select Payment Cycle', 'Select Number Of Cycle', 'Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                        var fields = [rec_cycle, rec_cycle_time, card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                        var alerts = [];
                        var counter = 0;

                        $(fields).each(function (index, element) {
                            if (element == '') {
                                alerts.push('Please ' + messages[counter] + '.');
                            }
                            counter++;
                        });

                        if (filter.test(email) === false) {
                            alerts.push('Please Enter Valid Email Address');
                        }

                        if (alerts.length === 0) {

                            Stripe.setPublishableKey(stripe_key);
                            var generateToken = function (e) {
                                var form = jQuery('div.easy-donation');
                                form.find('button#wpd_donate_now').prop('disabled', true);
                                Stripe.create(form, stripeResponseHandler);
                                e.preventDefault();
                            };

                            var stripeResponseHandler = function (status, response) {
                                var form = jQuery('div.easy-donation');
                                if (response.error) {
                                    form.find('div#rec_alerts').html("<div class='alert alert-warning'>" + response.error.message + "</div>");
                                    form.find('button#submit-button').prop('disabled', false);
                                    jQuery('div.overlay').fadeOut(500);
                                } else {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'stripeToken',
                                        'value': response.id,
                                        'id': 'payment_access_tocken'
                                    }).appendTo(form);

                                    var token = jQuery('#payment_access_tocken').val();
                                    var amount = jQuery('#donation_amount').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var dPost = '';
                                    if (jQuery.getUrlVar('postId') != 'undefined') {
                                        dPost = jQuery.getUrlVar('postId');
                                    }
                                    var email = jQuery('#email').val();
                                    var data = 'interval=' + rec_cycle + '&interval_count=' + rec_cycle_time + '&mail=' + email + '&donationType=' + dType + '&donationPost=' + dPost + '&token=' + token + '&currency=' + curr + '&amout=' + amount + '&action=wpd_credit_card_subscription';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (credi_respnse) {
                                            jQuery("div#single_payment").empty();
                                            if (credi_respnse.status === true) {
                                                wpd_create_invoice_single(credi_respnse.msg, true)
                                            } else if (credi_respnse.status === false) {
                                                jQuery('div.overlay').fadeOut(500);
                                                alert(credi_respnse.msg)
                                            }
                                        }
                                    });
                                    return false;
                                    jQuery('div.overlay').fadeOut(500);
                                }
                            }

                            var tocken = Stripe.createToken({
                                number: card_no,
                                cvc: cvc,
                                exp_month: exp_month,
                                exp_year: exp_year,
                            }, stripeResponseHandler);
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#rec_alerts").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#rec_alerts").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#rec_alerts").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#rec_alerts").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Stripe Account Information in Plugin Options</div>");
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#rec_alerts").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (recuring == 'true' && rec_paypal == 'true') {
                var amount = jQuery('#donation_amount').val();
                var rec_cycle = jQuery('#rec_cycle_paypal').val();
                var f_name = jQuery('#f_name').val();
                var l_name = jQuery('#l_name').val();
                var email = jQuery('#email').val();
                var contact_no = jQuery('#contact_no').val();
                var address = jQuery('#address').val();
                var currency = jQuery('#currency_').val();
                var data = 'amount=' + amount + '&currency=' + currency + '&rec_cycle=' + rec_cycle + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_recuring_details_paypal';
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                var messages = ['Enter or Select Amount', 'Select Payment Cycle', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                var fields = [amount, rec_cycle, f_name, l_name, email, contact_no, address]
                var alerts = [];
                var counter = 0;
                $(fields).each(function (index, element) {
                    if (element == '') {
                        alerts.push('Please ' + messages[counter] + '.');
                    }
                    counter++;
                });

                if (filter.test(email) === false) {
                    alerts.push('Please Enter Valid Email Address');
                }
                if (alerts.length === 0) {
                    if (wpd_create_invoice_paypal() == 'ok') {
                        jQuery.ajax({
                            type: "post",
                            url: adminurl,
                            data: data,
                            beforeSend: function () {
                                jQuery('div.overlay').fadeIn(500);
                            },
                            success: function (credi_respnse) {
                                jQuery("div#rec_alerts_paypal").empty();
                                jQuery("div#rec_alerts_paypal").html(credi_respnse);
                                jQuery("div#rec_alerts_paypal").show();
                                jQuery('div.overlay').fadeOut(500);
                            }
                        });
                    }
                } else {
                    //jQuery('div.overlay').fadeOut(500);
                    jQuery("#rec_alerts_paypal").fadeIn('slow');
                    var newHTML = jQuery.map(alerts, function (value) {
                        return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                    });

                    jQuery("#rec_alerts_paypal").html(newHTML.join(""));
                    setTimeout(function () {
                        jQuery("#rec_alerts_paypal").fadeOut('slow').empty();
                    }, 15000);
                }
            } else if (one_time == 'true' && $("#credit2_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                if (amount != '') {
                    var card_no = $('#single_card_no').val();
                    var exp_month = $('#card-expiry-month').val();
                    var exp_year = $('#card-expiry-year').val();
                    var cvc = $('#card_cvc').val();

                    var f_name = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var email = $('#email').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();

                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    var messages = wp_localize_script['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                    var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                    var alerts = [];
                    var counter = 0;
                    //wp_localize_script( messages );
                    $(fields).each(function (index, element) {
                        if (element == '') {
                            alerts.push('Please ' + messages[counter] + '.');
                        }
                        counter++;
                    });

                    if (filter.test(email) === false) {
                        alerts.push('Please Enter Valid Email Address');
                    }

                    if (alerts.length === 0) {
                        Stripe.setPublishableKey(stripe_key);
                        var generateToken = function (e) {
                            var form = jQuery('div.easy-donation');
                            form.find('button#wpd_donate_now').prop('disabled', true);
                            Stripe.create(form, stripeResponseHandler);
                            e.preventDefault();
                        };

                        var stripeResponseHandler = function (status, response) {
                            var form = jQuery('div.easy-donation');
                            if (response.error) {
                                form.find('div#single_payment').html("<div class='alert alert-warning'>" + response.error.message + "</div>");
                                form.find('button#submit-button').prop('disabled', false);
                                jQuery('div.overlay').fadeOut(500);
                            } else {
                                jQuery('<input>', {
                                    'type': 'hidden',
                                    'name': 'stripeToken',
                                    'value': response.id,
                                    'id': 'payment_access_tocken'
                                }).appendTo(form);

                                var token = jQuery('#payment_access_tocken').val();
                                var amount = jQuery('#donation_amount').val();
                                var dType = jQuery.getUrlVar('data_donation');
                                var dPost = '';
                                if (jQuery.getUrlVar('postId') != 'undefined') {
                                    dPost = jQuery.getUrlVar('postId');
                                }
                                var curr = jQuery('#currency_').val();
                                var data = 'donationType=' + dType + '&donationPost=' + dPost + '&token=' + token + '&currency=' + curr + '&amout=' + amount + '&action=wpd_credit_card_payment';
                                jQuery.ajax({
                                    type: "post",
                                    url: adminurl,
                                    data: data,
                                    dataType: 'json',
                                    beforeSend: function () {
                                        jQuery('div.overlay').fadeIn(500);
                                    },
                                    success: function (credi_respnse) {
                                        jQuery("div#single_payment").empty();
                                        if (credi_respnse.status === true) {
                                            wpd_create_invoice_single(credi_respnse.msg, false)
                                        } else if (credi_respnse.status === false) {
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(credi_respnse.msg)
                                        }
                                    }
                                });
                                return false;
                                jQuery('div.overlay').fadeOut(500);
                            }
                        }

                        var tocken = Stripe.createToken({
                            number: card_no,
                            cvc: cvc,
                            exp_month: exp_month,
                            exp_year: exp_year,
                        }, stripeResponseHandler);
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").fadeIn('slow');
                        var newHTML = jQuery.map(alerts, function (value) {
                            return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                        });

                        jQuery("#single_payment").html(newHTML.join(""));
                        setTimeout(function () {
                            jQuery("#single_payment").fadeOut('slow').empty();
                        }, 15000);
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#authorized_").parent().hasClass('active')) {
                if (amount != '') {
                    var card_no = $('#single_authorized_no').val();
                    var exp_month = $('#authorized-expiry-month').val();
                    var exp_year = $('#authorized-expiry-year').val();
                    var cvc = $('#authorized_cvc').val();

                    var f_name = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var email = $('#email').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();

                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                    var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                    var alerts = [];
                    var counter = 0;

                    $(fields).each(function (index, element) {
                        if (element == '') {
                            alerts.push('Please ' + messages[counter] + '.');
                        }
                        counter++;
                    });

                    if (filter.test(email) === false) {
                        alerts.push('Please Enter Valid Email Address');
                    }

                    if (alerts.length === 0) {
                        var curr = jQuery('#currency_').val();
                        var dType = jQuery.getUrlVar('data_donation');
                        var data = 'donationType=' + dType +
                            '&donationPost=' + dPost +
                            '&card_no=' + card_no +
                            '&exp_month=' + exp_month +
                            '&exp_year=' + exp_year +
                            '&cvc=' + cvc +
                            '&f_name=' + f_name +
                            '&l_name=' + l_name +
                            '&email=' + email +
                            '&contact_no=' + contact_no +
                            '&address=' + address +
                            '&currency=' + curr +
                            '&amout=' + amount +
                            '&action=wpd_authorized_payment';
                        jQuery.ajax({
                            type: "post",
                            url: adminurl,
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                jQuery('div.overlay').fadeIn(500);
                            },
                            success: function (authorized_respnse) {
                                jQuery("div#single_payment").empty();
                                jQuery("div#single_payment").html(authorized_respnse.msg);
                                jQuery("div#single_payment").show();
                                jQuery('div.overlay').fadeOut(500);
                                alert(authorized_respnse.msg);
                                if (authorized_respnse.status === true) {
                                    window.location.replace(homeUrl);
                                }
                            }
                        });
                        return false;
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").fadeIn('slow');
                        var newHTML = jQuery.map(alerts, function (value) {
                            return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                        });

                        jQuery("#single_payment").html(newHTML.join(""));
                        setTimeout(function () {
                            jQuery("#single_payment").fadeOut('slow').empty();
                        }, 15000);
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#twocheckout_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (twocheckout_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_twocheckout_no').val();
                            var exp_month = $('#twocheckout-expiry-month').val();
                            var exp_year = $('#twocheckout-expiry-year').val();
                            var cvc = $('#twocheckout_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'twocheckoutToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_twocheckout_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: twocheckout_sellerid,
                                    publishableKey: twocheckout_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your 2Checkout Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }
            else if (one_time == 'true' && $("#transfer2_").parent().hasClass('active')) {
                if (amount != '') {
                    var f_name = $('#f_name').val();
                    var l_name = $('#l_name').val();
                    var email = $('#email').val();
                    var contact_no = $('#contact_no').val();
                    var address = $('#address').val();

                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    var messages = ['Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                    var fields = [f_name, l_name, email, contact_no, address]
                    var alerts = [];
                    var counter = 0;

                    $(fields).each(function (index, element) {
                        if (element == '') {
                            alerts.push('Please ' + messages[counter] + '.');
                        }
                        counter++;
                    });

                    if (filter.test(email) === false) {
                        alerts.push('Please Enter Valid Email Address');
                    }

                    if (alerts.length === 0) {
                        var amount = jQuery('#donation_amount').val();
                        var currency = jQuery('#currency_').val();
                        var dType = jQuery.getUrlVar('data_donation');
                        var dPost = '';
                        if (jQuery.getUrlVar('postId') != 'undefined') {
                            dPost = jQuery.getUrlVar('postId');
                        }
                        var data = 'donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&amout=' + amount + '&action=wpd_bank_transaction';
                        jQuery.ajax({
                            type: "post",
                            url: adminurl,
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                donationPreIn();
                               // jQuery('div.overlay').fadeIn(500);
                            },
                            success: function (credi_respnse) {
                                if (credi_respnse.status === true) {
                                    jQuery("div#bank_transcation").empty();
                                    jQuery('div.overlay').fadeIn(500);
                                    jQuery("div#bank_transcation").html(credi_respnse.msg);
                                    $('div.donation-modal-preloader').fadeOut('slow');
                                    jQuery("div#bank_transcation").show();
                                } else if (credi_respnse.status === false) {
                                    $("div#step2-error").empty();
                                    $("div#step2-error").html(credi_respnse.msg);
                                    $("div#step2-error").show();
                                    donationPreOut();
                                    sTop();
                                }
                            }
                        });
                        return false;
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").fadeIn('slow');
                        var newHTML = jQuery.map(alerts, function (value) {
                            return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                        });

                        jQuery("#rec_alerts").html(newHTML.join(""));
                        setTimeout(function () {
                            jQuery("#single_payment").fadeOut('slow').empty();
                        }, 15000);
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#paypal2_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                if (amount != '') {
                    var amount = jQuery('#donation_amount').val();
                    var curre = jQuery('#currency_').val();
                    var f_name = jQuery('#f_name').val();
                    var l_name = jQuery('#l_name').val();
                    var email = jQuery('#email').val();
                    var contact_no = jQuery('#contact_no').val();
                    var address = jQuery('#address').val();
                    var returnUrl = jQuery('#return_url').val();
                    var dType = jQuery.getUrlVar('data_donation');
                    var dPost = '';
                    if (jQuery.getUrlVar('postId') != 'undefined') {
                        dPost = jQuery.getUrlVar('postId');
                    }
                    var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&rec_cycle=' + rec_cycle + '&cycle_time=' + rec_cycle_time + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_paypal_single_payment';
                    jQuery.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        beforeSend: function () {
                            jQuery('div.overlay').fadeIn(500);
                        },
                        success: function (credi_respnse) {
                            jQuery("div#single_payment").empty();
                            jQuery("div#single_payment").html(credi_respnse);
                            jQuery("div#single_payment").show();
                            jQuery('div.overlay').fadeOut(500);
                            wpd_confirm_popup();
                            //jQuery('div#f_overlay').fadeIn(500);
                        }
                    });
                    return false;
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#paymaster_").parent().hasClass('active')) {

                jQuery('div.overlay').fadeIn(500);
                if (amount != '') {
                    var amount = jQuery('#donation_amount').val();
                    var curre = jQuery('#currency_').val();
                    var f_name = jQuery('#f_name').val();
                    var l_name = jQuery('#l_name').val();
                    var email = jQuery('#email').val();
                    var contact_no = jQuery('#contact_no').val();
                    var address = jQuery('#address').val();
                    var returnUrl = jQuery('#return_url').val();
                    var dType = jQuery.getUrlVar('data_donation');
                    var dPost = '';
                    if (jQuery.getUrlVar('postId') != 'undefined') {
                        dPost = jQuery.getUrlVar('postId');
                    }
                    //var data = 'data=testdata';
                    var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&rec_cycle=' + rec_cycle + '&cycle_time=' + rec_cycle_time + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_paymaster_single_payment';
                    jQuery.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        beforeSend: function () {
                            jQuery('div.overlay').fadeIn(500);
                        },
                        success: function (credi_respnse) {
                            jQuery("div#single_payment").empty();
                            jQuery("div#single_payment").html(credi_respnse);
                            jQuery("div#single_payment").show();
                            jQuery('div.overlay').fadeOut(500);
                            wpd_confirm_popup();
                            //jQuery('div#f_overlay').fadeIn(500);
                        }
                    });
                    return false;

                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#yandex_").parent().hasClass('active')) {

                jQuery('div.overlay').fadeIn(500);
                if (amount != '') {
                    var amount = jQuery('#donation_amount').val();
                    var curre = jQuery('#currency_').val();
                    var f_name = jQuery('#f_name').val();
                    var l_name = jQuery('#l_name').val();
                    var email = jQuery('#email').val();
                    var contact_no = jQuery('#contact_no').val();
                    var address = jQuery('#address').val();
                    var returnUrl = jQuery('#return_url').val();
                    var dType = jQuery.getUrlVar('data_donation');
                    var dPost = '';
                    if (jQuery.getUrlVar('postId') != 'undefined') {
                        dPost = jQuery.getUrlVar('postId');
                    }
                    //var data = 'data=testdata';
                    var data = 'donationType=' + dType + '&donationPost=' + dPost + '&return_url=' + returnUrl + '&amount=' + amount + '&currency=' + curre + '&rec_cycle=' + rec_cycle + '&cycle_time=' + rec_cycle_time + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_yandex_single_payment';
                    jQuery.ajax({
                        type: "post",
                        url: adminurl,
                        data: data,
                        beforeSend: function () {
                            jQuery('div.overlay').fadeIn(500);
                        },
                        success: function (credi_respnse) {

                            jQuery("div#single_payment").empty();
                            jQuery("div#single_payment").html(credi_respnse);
                            jQuery("div#single_payment").show();
                            jQuery('div.overlay').fadeOut(500);
                            wpd_confirm_popup();
                            //jQuery('div#f_overlay').fadeIn(500);
                        }
                    });
                    return false;

                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Select or Enter Payment Value</div>");
                }
            } else if (one_time == 'true' && $("#braintree_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (braintree_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_braintree_no').val();
                            var exp_month = $('#braintree-expiry-month').val();
                            var exp_year = $('#braintree-expiry-year').val();
                            var cvc = $('#braintree_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'braintreeToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_braintree_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: braintree_sellerid,
                                    publishableKey: braintree_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Braintree Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }

         else if (one_time == 'true' && $("#bluepay_").parent().hasClass('active')) {
            jQuery('div.overlay').fadeIn(500);
            TCO.loadPubKey('sandbox', function () {
                if (bluepay_key != '') {
                    if (amount != '') {
                        var errorCallback = function (data) {
                            if (data.errorCode === 200) {
                                tokenRequest();
                            } else {
                                jQuery('div.overlay').fadeOut(500);
                                jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                setTimeout(function () {
                                    $('#single_payment').fadeOut('slow');
                                }, 5000);
                                return false;
                            }
                        };
                        var card_no = $('#single_bluepay_no').val();
                        var exp_month = $('#bluepay-expiry-month').val();
                        var exp_year = $('#bluepay-expiry-year').val();
                        var cvc = $('#bluepay_cvc').val();

                        var f_name = $('#f_name').val();
                        var l_name = $('#l_name').val();
                        var email = $('#email').val();
                        var contact_no = $('#contact_no').val();
                        var address = $('#address').val();

                        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                        var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                        var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                        var alerts = [];
                        var counter = 0;

                        $(fields).each(function (index, element) {
                            if (element == '') {
                                alerts.push('Please ' + messages[counter] + '.');
                            }
                            counter++;
                        });

                        if (filter.test(email) === false) {
                            alerts.push('Please Enter Valid Email Address');
                        }
                        if (alerts.length === 0) {
                            var successCallback = function (data) {
                                jQuery('<input>', {
                                    'type': 'hidden',
                                    'name': 'bluepayToken',
                                    'value': data.response.token.token,
                                    'id': 'payment_access_tocken'
                                }).appendTo('.donation-fields');
                                var curr = jQuery('#currency_').val();
                                var dType = jQuery.getUrlVar('data_donation');
                                var data = 'donationType=' + dType +
                                    '&token=' + data.response.token.token +
                                    '&donationPost=' + dPost +
                                    '&card_no=' + card_no +
                                    '&exp_month=' + exp_month +
                                    '&exp_year=' + exp_year +
                                    '&cvc=' + cvc +
                                    '&f_name=' + f_name +
                                    '&l_name=' + l_name +
                                    '&email=' + email +
                                    '&contact_no=' + contact_no +
                                    '&address=' + address +
                                    '&currency=' + curr +
                                    '&amout=' + amount +
                                    '&action=wpd_bluepay_payment';
                                jQuery.ajax({
                                    type: "post",
                                    url: adminurl,
                                    data: data,
                                    dataType: 'json',
                                    beforeSend: function () {
                                        jQuery('div.overlay').fadeIn(500);
                                    },
                                    success: function (response) {
                                        jQuery("div#single_payment").empty();
                                        jQuery("div#single_payment").html(response.msg);
                                        jQuery("div#single_payment").show();
                                        jQuery('div.overlay').fadeOut(500);
                                        alert(response.msg);
                                        if (response.status === true) {
                                            window.location.replace(homeUrl);
                                        }
                                    }
                                });
                            }
                            var args = {
                                sellerId: bluepay_sellerid,
                                publishableKey: bluepay_key,
                                ccNo: card_no,
                                cvv: cvc,
                                expMonth: exp_month,
                                expYear: exp_year
                            };
                            TCO.requestToken(successCallback, errorCallback, args);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").fadeIn('slow');
                        var newHTML = jQuery.map(alerts, function (value) {
                            return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                        });

                        jQuery("#single_payment").html(newHTML.join(""));
                        setTimeout(function () {
                            jQuery("#single_payment").fadeOut('slow').empty();
                        }, 15000);
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Bluepay Account Information in Plugin Options</div>");
                }
            });
            jQuery('div.overlay').fadeOut(500);
            return false;
        }

            else if (one_time == 'true' && $("#conekta_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (conekta_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_conekta_no').val();
                            var exp_month = $('#conekta-expiry-month').val();
                            var exp_year = $('#conekta-expiry-year').val();
                            var cvc = $('#conekta_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'conektaToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_conekta_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: conekta_sellerid,
                                    publishableKey: conekta_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Conekta Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }

            else if (one_time == 'true' && $("#paystack_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (paystack_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_paystack_no').val();
                            var exp_month = $('#paystack-expiry-month').val();
                            var exp_year = $('#paystack-expiry-year').val();
                            var cvc = $('#paystack_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'paystackToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_paystack_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: paystack_sellerid,
                                    publishableKey: paystack_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Paystack Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }

            else if (one_time == 'true' && $("#payumoney_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (payumoney_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_payumoney_no').val();
                            var exp_month = $('#payumoney-expiry-month').val();
                            var exp_year = $('#payumoney-expiry-year').val();
                            var cvc = $('#payumoney_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'payumoneyToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_payumoney_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: payumoney_sellerid,
                                    publishableKey: payumoney_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Payumoney Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }

            else if (one_time == 'true' && $("#quickpay_").parent().hasClass('active')) {
                jQuery('div.overlay').fadeIn(500);
                TCO.loadPubKey('sandbox', function () {
                    if (quickpay_key != '') {
                        if (amount != '') {
                            var errorCallback = function (data) {
                                if (data.errorCode === 200) {
                                    tokenRequest();
                                } else {
                                    jQuery('div.overlay').fadeOut(500);
                                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                    setTimeout(function () {
                                        $('#single_payment').fadeOut('slow');
                                    }, 5000);
                                    return false;
                                }
                            };
                            var card_no = $('#single_quickpay_no').val();
                            var exp_month = $('#quickpay-expiry-month').val();
                            var exp_year = $('#quickpay-expiry-year').val();
                            var cvc = $('#quickpay_cvc').val();

                            var f_name = $('#f_name').val();
                            var l_name = $('#l_name').val();
                            var email = $('#email').val();
                            var contact_no = $('#contact_no').val();
                            var address = $('#address').val();

                            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                            var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                            var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                            var alerts = [];
                            var counter = 0;

                            $(fields).each(function (index, element) {
                                if (element == '') {
                                    alerts.push('Please ' + messages[counter] + '.');
                                }
                                counter++;
                            });

                            if (filter.test(email) === false) {
                                alerts.push('Please Enter Valid Email Address');
                            }
                            if (alerts.length === 0) {
                                var successCallback = function (data) {
                                    jQuery('<input>', {
                                        'type': 'hidden',
                                        'name': 'quickpayToken',
                                        'value': data.response.token.token,
                                        'id': 'payment_access_tocken'
                                    }).appendTo('.donation-fields');
                                    var curr = jQuery('#currency_').val();
                                    var dType = jQuery.getUrlVar('data_donation');
                                    var data = 'donationType=' + dType +
                                        '&token=' + data.response.token.token +
                                        '&donationPost=' + dPost +
                                        '&card_no=' + card_no +
                                        '&exp_month=' + exp_month +
                                        '&exp_year=' + exp_year +
                                        '&cvc=' + cvc +
                                        '&f_name=' + f_name +
                                        '&l_name=' + l_name +
                                        '&email=' + email +
                                        '&contact_no=' + contact_no +
                                        '&address=' + address +
                                        '&currency=' + curr +
                                        '&amout=' + amount +
                                        '&action=wpd_quickpay_payment';
                                    jQuery.ajax({
                                        type: "post",
                                        url: adminurl,
                                        data: data,
                                        dataType: 'json',
                                        beforeSend: function () {
                                            jQuery('div.overlay').fadeIn(500);
                                        },
                                        success: function (response) {
                                            jQuery("div#single_payment").empty();
                                            jQuery("div#single_payment").html(response.msg);
                                            jQuery("div#single_payment").show();
                                            jQuery('div.overlay').fadeOut(500);
                                            alert(response.msg);
                                            if (response.status === true) {
                                                window.location.replace(homeUrl);
                                            }
                                        }
                                    });
                                }
                                var args = {
                                    sellerId: quickpay_sellerid,
                                    publishableKey: quickpay_key,
                                    ccNo: card_no,
                                    cvv: cvc,
                                    expMonth: exp_month,
                                    expYear: exp_year
                                };
                                TCO.requestToken(successCallback, errorCallback, args);
                            }
                        } else {
                            jQuery('div.overlay').fadeOut(500);
                            jQuery("#single_payment").fadeIn('slow');
                            var newHTML = jQuery.map(alerts, function (value) {
                                return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                            });

                            jQuery("#single_payment").html(newHTML.join(""));
                            setTimeout(function () {
                                jQuery("#single_payment").fadeOut('slow').empty();
                            }, 15000);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Quickpay Account Information in Plugin Options</div>");
                    }
                });
                jQuery('div.overlay').fadeOut(500);
                return false;
            }

         else if (one_time == 'true' && $("#cardcom_").parent().hasClass('active')) {
            jQuery('div.overlay').fadeIn(500);
            TCO.loadPubKey('sandbox', function () {
                if (cardcom_key != '') {
                    if (amount != '') {
                        var errorCallback = function (data) {
                            if (data.errorCode === 200) {
                                tokenRequest();
                            } else {
                                jQuery('div.overlay').fadeOut(500);
                                jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">" + data.errorMsg + "</div>");
                                setTimeout(function () {
                                    $('#single_payment').fadeOut('slow');
                                }, 5000);
                                return false;
                            }
                        };
                        var card_no = $('#single_cardcom_no').val();
                        var exp_month = $('#cardcom-expiry-month').val();
                        var exp_year = $('#cardcom-expiry-year').val();
                        var cvc = $('#cardcom_cvc').val();

                        var f_name = $('#f_name').val();
                        var l_name = $('#l_name').val();
                        var email = $('#email').val();
                        var contact_no = $('#contact_no').val();
                        var address = $('#address').val();

                        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                        var messages = ['Enter Card Number', 'Select Card Expire Month', 'Select Card Expire Year', 'Enter Card CVC Number', 'Enter Your First Name', 'Enter Your Last Name', 'Enter Your Email ID', 'Enter Your Contact No', 'Enter Your Address'];
                        var fields = [card_no, exp_month, exp_year, cvc, f_name, l_name, email, contact_no, address]
                        var alerts = [];
                        var counter = 0;

                        $(fields).each(function (index, element) {
                            if (element == '') {
                                alerts.push('Please ' + messages[counter] + '.');
                            }
                            counter++;
                        });

                        if (filter.test(email) === false) {
                            alerts.push('Please Enter Valid Email Address');
                        }
                        if (alerts.length === 0) {
                            var successCallback = function (data) {
                                jQuery('<input>', {
                                    'type': 'hidden',
                                    'name': 'cardcomToken',
                                    'value': data.response.token.token,
                                    'id': 'payment_access_tocken'
                                }).appendTo('.donation-fields');
                                var curr = jQuery('#currency_').val();
                                var dType = jQuery.getUrlVar('data_donation');
                                var data = 'donationType=' + dType +
                                    '&token=' + data.response.token.token +
                                    '&donationPost=' + dPost +
                                    '&card_no=' + card_no +
                                    '&exp_month=' + exp_month +
                                    '&exp_year=' + exp_year +
                                    '&cvc=' + cvc +
                                    '&f_name=' + f_name +
                                    '&l_name=' + l_name +
                                    '&email=' + email +
                                    '&contact_no=' + contact_no +
                                    '&address=' + address +
                                    '&currency=' + curr +
                                    '&amout=' + amount +
                                    '&action=wpd_cardcom_payment';
                                jQuery.ajax({
                                    type: "post",
                                    url: adminurl,
                                    data: data,
                                    dataType: 'json',
                                    beforeSend: function () {
                                        jQuery('div.overlay').fadeIn(500);
                                    },
                                    success: function (response) {
                                        jQuery("div#single_payment").empty();
                                        jQuery("div#single_payment").html(response.msg);
                                        jQuery("div#single_payment").show();
                                        jQuery('div.overlay').fadeOut(500);
                                        alert(response.msg);
                                        if (response.status === true) {
                                            window.location.replace(homeUrl);
                                        }
                                    }
                                });
                            }
                            var args = {
                                sellerId: cardcom_sellerid,
                                publishableKey: cardcom_key,
                                ccNo: card_no,
                                cvv: cvc,
                                expMonth: exp_month,
                                expYear: exp_year
                            };
                            TCO.requestToken(successCallback, errorCallback, args);
                        }
                    } else {
                        jQuery('div.overlay').fadeOut(500);
                        jQuery("#single_payment").fadeIn('slow');
                        var newHTML = jQuery.map(alerts, function (value) {
                            return ("<div class=\"alert alert-warning\" role=\"alert\">" + value + "</div>");
                        });

                        jQuery("#single_payment").html(newHTML.join(""));
                        setTimeout(function () {
                            jQuery("#single_payment").fadeOut('slow').empty();
                        }, 15000);
                    }
                } else {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("#single_payment").html("<div class=\"alert alert-warning\" role=\"alert\">Please Fill Your Cardcom Account Information in Plugin Options</div>");
                }
            });
            jQuery('div.overlay').fadeOut(500);
            return false;
        }

            }
        })

        return false;
    })
    ;

    jQuery("#rec_card_cvc, #rec_card_no, #donation_amount, #single_card_no,  #card_cvc").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $(this).next("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    jQuery("#paypal_confirmation").on('click', function () {
        $('#f_overlay').fadeOut();
        $('.overlay').fadeIn();
        jQuery('div.confirm_popup').fadeOut();
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: 'action=wpd_confirm_paypal_recuring',
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (paypal_respnse) {
                jQuery("div#paypal_recuring_confirm").empty();
                jQuery("div#paypal_recuring_confirm").html(paypal_respnse);
                jQuery("div#paypal_recuring_confirm").show();
                jQuery('div.overlay').fadeOut(500);
                $('#f_overlay').fadeIn(500);
            }
        });
        return false;
    }

)
;
/*=== Document.Ready Ends Here ===*/

function wpd_create_invoice_single(transID, rec) {
    var rec_cycle = jQuery('#rec_cycle').val();
    var rec_cycle_time = jQuery('#rec_cycle_time').val();
    var amount = jQuery('#donation_amount').val();
    var currency = jQuery('#currency_').val();
    var f_name = jQuery('#f_name').val();
    var l_name = jQuery('#l_name').val();
    var email = jQuery('#email').val();
    var contact_no = jQuery('#contact_no').val();
    var address = jQuery('#address').val();
    var dType = jQuery.getUrlVar('data_donation');
    var dPost = '';
    if (jQuery.getUrlVar('postId') != 'undefined') {
        dPost = jQuery.getUrlVar('postId');
    }
    var data = '';
    if (rec === true) {
        data = 'rec_cycle=' + rec_cycle + '&cycle_time=' + rec_cycle_time + '&recuring=yes&transid=' + transID + '&donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_recuring_details_single';
    } else if (rec === false) {
        data = 'transid=' + transID + '&donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&currency=' + currency + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&address=' + address + '&action=wpd_save_recuring_details_single';
    }

    jQuery.ajax({
        type: "post",
        url: adminurl,
        data: data,
        beforeSend: function () {
            jQuery('div.overlay').fadeIn(500);
        },
        success: function (credi_respnse) {
            jQuery("div#single_payment").empty();
            jQuery("div#single_payment").html(credi_respnse);
            jQuery("div#single_payment").show();
            jQuery('div.overlay').fadeOut(500);
            alert(credi_respnse);
            window.location.replace(homeUrl);
        }
    });
}

function wpd_create_invoice_paypal() {
    var amount = jQuery('#donation_amount').val();
    var rec_cycle = jQuery('#rec_cycle_paypal').val();
    var f_name = jQuery('#f_name').val();
    var l_name = jQuery('#l_name').val();
    var email = jQuery('#email').val();
    var contact_no = jQuery('#contact_no').val();
    var address = jQuery('#address').val();
    var curr = jQuery('#currency_').val();
    var dType = jQuery.getUrlVar('data_donation');
    var dPost = '';
    if (jQuery.getUrlVar('postId') != 'undefined') {
        dPost = jQuery.getUrlVar('postId');
    }
    var data = 'donationType=' + dType + '&donationPost=' + dPost + '&amount=' + amount + '&rec_cycle=' + rec_cycle + '&f_name=' + f_name + '&l_name=' + l_name + '&email=' + email + '&contact_no=' + contact_no + '&currency=' + curr + '&address=' + address + '&action=wpd_save_paypal_recuring_details';
    jQuery.ajax({
        type: "post",
        url: adminurl,
        data: data,
        beforeSend: function () {
            jQuery('div.overlay').fadeIn(500);
        },
        success: function (credi_respnse) {
            jQuery("div#rec_alerts").empty();
            jQuery("div#rec_alerts").html(credi_respnse);
            jQuery("div#rec_alerts").show();
            jQuery('div.overlay').fadeOut(500);
        }
    });
    return 'ok';
}

function wpd_confirm_popup() {
    jQuery('#f_overlay').fadeIn();
    var get_height = jQuery('div.confirm_popup').height();
    var height = get_height / 2;
    jQuery('div.confirm_popup').css({
        'margin-top': -height
    });
}

function wpd_thanku_popup() {
    var get_height = jQuery('div#paypal_recuring_confirm > div.confirm_popup').height();
    var height = get_height / 2;
    jQuery('div#paypal_recuring_confirm > div.confirm_popup').css({
        'margin-top': -height
    });
}

function wpd_single_report_popup() {
    var get_height = jQuery('div#reporting_popup > div.confirm_popup').height();
    var height = get_height / 2;
    jQuery('div#reporting_popup > div.confirm_popup').css({
        'margin-top': -height
    });
}

function wpd_close_popup() {
    jQuery("input#paypa_thank_close").live('click', function () {
        jQuery('#f_overlay').fadeOut(500);
        jQuery('.overlay').fadeOut(500);
        jQuery('div.confirm_popup').fadeOut(500);
        jQuery("div#paypal_recuring_confirm").empty();
        window.location.replace(homeUrl);
    });
}

jQuery(document).ready(function ($) {
    jQuery('button#_s_').on('click', function () {
        jQuery('#transaction_more').parent().parent().show();
        var from_date = jQuery('#date_from').val();
        var to_date = jQuery('#date_tor').val();
        var keyword = jQuery('#s_').val();
        data = 'from_date=' + from_date + '&to_date=' + to_date + '&keyword=' + keyword + '&action=wpd_date_reporting';
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (respnse) {
                if (respnse.match('Keyword') != null) {
                    jQuery("div#msg_").empty();
                    jQuery("div#msg_").html(respnse);
                    jQuery("div#msg_").show();
                    jQuery('div.overlay').fadeOut(500);
                } else {
                    jQuery("div#msg_").empty();
                    jQuery("div#history_wrap").empty();
                    jQuery("div#history_wrap").html(respnse);
                    jQuery('div.overlay').fadeOut(500);
                }
            }
        });
        setTimeout(function () {
            jQuery("div#msg_").fadeOut('slow').empty();
        }, 15000);
        return false;
    });


    jQuery('#type_all').on('change', function () {
        var type = $(this).val();
        data = 'type=' + type + '&action=wpd_change_type';
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (respnse) {
                jQuery("div#msg_").empty();
                jQuery("div#history_wrap").empty();
                jQuery("div#history_wrap").html(respnse);
                jQuery('div.overlay').fadeOut(500);
            }
        });
        return false;
    });

    jQuery('#type_numbers').on('change', function () {
        jQuery('#transaction_more').parent().parent().show();
        var type = $(this).val();
        data = 'type=' + type + '&action=wpd_change_type_number';
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (respnse) {
                jQuery("div#msg_").empty();
                jQuery("div#history_wrap").empty();
                jQuery("div#history_wrap").html(respnse);
                jQuery('div.overlay').fadeOut(500);
            }
        });
        return false;
    });

    jQuery('#transaction_more').on('click', function () {
        var length = jQuery('div#history_wrap').children().length;
        var data = 'length=' + length + '&action=reporting_load_more';
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (respnse) {
                if (respnse.match('No Record Found') != null) {
                    jQuery('#transaction_more').parent().parent().hide();
                    jQuery('div.overlay').fadeOut(500);
                } else {
                    jQuery("div#msg_").empty();
                    jQuery("div#history_wrap").append(respnse);
                    jQuery('div.overlay').fadeOut(500);
                }
            }
        });
        return false;
    });
    // jQuery(window).load(function() {
    //     //var type = 'all';
    //     data = 'type=' + 'all' + '&action=wpd_change_type';
    //     jQuery.ajax({
    //         type: "post",
    //         url: adminurl,
    //         data: data,
    //         beforeSend: function () {
    //             jQuery('div.overlay').fadeIn(500);
    //         },
    //         success: function (respnse) {
    //             jQuery("div#msg_").empty();
    //             jQuery("div#history_wrap").empty();
    //             jQuery("div#history_wrap").html(respnse);
    //             jQuery('div.overlay').fadeOut(500);
    //         }
    //     });
    //     return false;
    // });
    jQuery('#clear_filters').on('click', function () {
        var length = jQuery('div#history_wrap').children().length;
        var data = 'length=' + length + '&action=wpd_clear_filter';
        $("#type_all").val("all").trigger("change");
        $("#type_numbers").val("all").trigger("change");
        jQuery.ajax({
            type: "post",
            url: adminurl,
            data: data,
            beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
            success: function (respnse) {
                alert(respnse);
                if (respnse.indexOf('No Record Found') > 0) {
                    jQuery('#transaction_more').parent().parent().hide();
                    jQuery('div.overlay').fadeOut(500);
                } else {
                    jQuery("div#msg_").empty();
                    jQuery("div#history_wrap").append(respnse);
                    jQuery('div.overlay').fadeOut(500);
                }
            }
        });
        return false;
    });

    jQuery('div#history_wrap a#view_invoice').each(function () {
        jQuery(this).live('click', function () {
            var id = jQuery(this).data('id');
            var data = 'id=' + id + '&action=wpd_single_transaction_popup';
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('div.overlay').fadeIn(500);
                },
                success: function (respnse) {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("div#reporting_popup").empty();
                    jQuery("div#reporting_popup").html(respnse);
                    jQuery('div#f_overlay').fadeIn(500);
                    jQuery("div#reporting_popup").fadeIn(500);
                }
            });
            return false;
        });
    });

    jQuery('#single_payment_close').live('click', function () {
        jQuery('#f_overlay').fadeOut(500);
        jQuery('.overlay').fadeOut(500);
        jQuery('div.confirm_popup').fadeOut(500);
        window.location.replace(homeUrl);
    });

    jQuery('#single_payment_closes').live('click', function () {
        jQuery('#f_overlay').fadeOut(500);
        jQuery('.overlay').fadeOut(500);
        jQuery('div.confirm_popup').fadeOut(500);
        //window.location.replace(homeUrl);
    });

    $(".proceed-to-donate").on("click", function () {
        if ($('.frequency.payment-method > li > a.active').length != 0) {
            if ($('.loader-overlay').length == 0) {
                $('body').append('<div class="loader-overlay"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></div>');
            }
            var payment_method = $('.payment-box .payment-method li a.active').attr('data-bind');
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: 'payment_method=' + payment_method + '&action=wst_payment_info',
                success: function (data) {
                    $('.donation-fields .payment-info').html(data);
                    $(".select").select2();
                    $('.loader-overlay').remove();
                }
            });
        } else {
            alert('Please select payment type in which you want to make transaction!');
        }

        return false;
    });
});

jQuery('.donation_for input[type="radio"]').change(function() {

    var data_id = (this.id);
    if (data_id == 'general' && jQuery('#my-post').length == 0) {
            jQuery('#my-post').remove();
        }else{
    
    jQuery.ajax({
        url: ajaxurl,
        method: 'POST',
        data: 'action=wpd_posts_serch&data_id=' + data_id,
         beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
        success: function (res) {
            //console.log(res);
            jQuery('div.overlay').fadeOut(500);
            jQuery('.product_list').html(res);
            jQuery(".product_list select").select2();
            
        }
    });
        }

});
jQuery('.add_payment').live('click', function (e) {
    e.preventDefault();
    var post_id = jQuery("#my-post").val();
    var f_name = jQuery("#f_name").val();
    var l_name = jQuery("#l_name").val();
    var email = jQuery("#email").val();
    var phone = jQuery("#phone").val();
    var address = jQuery("#address").val();
    var amount = jQuery("#amount").val();

    var data_id = (this.id);
    
    jQuery.ajax({
        url: ajaxurl,
        method: 'POST',
        data: 'action=wpd_insert_amount&post_id=' + post_id +'&f_name=' + f_name +'&l_name=' + l_name +'&email=' + email +'&phone=' + phone +'&address=' + address +'&amount=' + amount,
        beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
        success: function (res) {
            //console.log(res);
            jQuery('div.overlay').fadeOut(500);
            jQuery('.message_box').html(res);
            
        }
    });

});
jQuery('.delet_record').live('click', function (e) {
    e.preventDefault();
    var data_id = (this.id);
    var thiss = this;
    var answer = confirm ("Are you sure you want to delete this");
if (answer)
{
  jQuery.ajax({
        url: ajaxurl,
        method: 'POST',
        data: 'action=wpd_delete_amount&data_id=' + data_id ,
        beforeSend: function () {
                jQuery('div.overlay').fadeIn(500);
            },
        success: function (res) {
            //console.log(res);
            jQuery('div.overlay').fadeOut(500);
            jQuery(thiss).parent().parent().parent().parent().hide(); //use here
            jQuery('.message_box').html(res);
           
            
        }
    });   // your ajax code
}
    

});

function wpd_reporting_popup() {
    jQuery('div#history_wrap a#view_invoice').each(function () {
        jQuery(this).live('click', function () {
            var id = jQuery(this).data('id');
            var data = 'id=' + id + '&action=wpd_single_transaction_popup';
            jQuery.ajax({
                type: "post",
                url: adminurl,
                data: data,
                beforeSend: function () {
                    jQuery('div.overlay').fadeIn(500);
                },
                success: function (respnse) {
                    jQuery('div.overlay').fadeOut(500);
                    jQuery("div#reporting_popup").empty();
                    jQuery("div#reporting_popup").html(respnse);
                    jQuery('div#f_overlay').fadeIn(500);
                    jQuery("div#reporting_popup").fadeIn(500);
                }
            });
            return false;
        });
    });
}

jQuery(document).ready(function ($) {
   
    $.extend({
        getUrlVars: function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getUrlVar: function (name) {
            return $.getUrlVars()[name];

        }
    });
});