<?php
$default2 = get_option('wp_donation_basic_settings', true);
$default = get_option( 'wp_donation_bank_settings', true );
$options2 = WPD_Common::wpd_set($default2, 'wp_donation_basic_settings');
$options = WPD_Common::wpd_set( $default, 'wp_donation_bank_settings' );
$settings = lifeline2_get_theme_options();
//print_r($options2); exit;
?>
<div id="bank_layer">X</div>
<div class="thanks">
    <div class="thanks-message" style="background-color: <?php echo WPD_Common::wpd_set( $options2, 'clrmsg' ) ?>;">

	    <?php if ( WPD_Common::wpd_set( $options2, 'custom_logo' ) ): ?>
            <?php echo '<img src="' . esc_url(lifeline2_set(lifeline2_set($settings, 'logo_image'), 'url')) . '" />'; ?>
	    <?php endif; ?>

        <?php if ( WPD_Common::wpd_set( $options2, 'smsg' ) ): ?>
        <h5 style="color: <?php echo WPD_Common::wpd_set( $options2, 'clrtext' ) ?>;">
            <?php echo _e(WPD_Common::wpd_set( $options2, 'smsg' )) ?>
        </h5>
    <?php endif; ?>
    <?php if ( WPD_Common::wpd_set( $options2, 'dmsg' ) ): ?>
        <span><?php echo _e(WPD_Common::wpd_set( $options2, 'dmsg' )) ?></span>
    <?php endif; ?>
        </div>
    <div class="success-notification">
        <h5><?php _e( 'THANKS DEAR: ', WPD_NAME ) ?> <strong><?php echo $f_name . ' ' . $l_name ?>,</strong></h5>
        <p>
        <?php if ( WPD_Common::wpd_set( $options2, 'msgdes' ) ): ?>
            <?php echo _e(WPD_Common::wpd_set( $options2, 'msgdes' )) ?>
        <?php endif; ?>
            <br  />
            <strong><?php _e( 'Check your transaction details below:', WPD_NAME ) ?></strong></p>
    </div>
    <div class="payment-method">
        <div class="payment-date"> <strong><?php echo $symbol . $amount ?></strong> <span><i class="ti-calendar"></i> <?php echo $date ?></span> </div>
        <div class="col-md-6">
            <h5><?php _e( 'Local Payment', WPD_NAME ) ?></h5>
            
            <ul>
                <?php if ( WPD_Common::wpd_set( $options, 'name' ) ): ?><li><span><?php _e( 'Payable To:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'name' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'bank_name' ) ): ?> <li><span><?php _e( 'Bank Name:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'bank_name' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'account_num' ) ): ?><li><span><?php _e( 'Account Number:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'account_num' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'branch_code' ) ): ?><li><span><?php _e( 'Branch Code:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'branch_code' ) ?></i></li><?php endif; ?>
            </ul>
        </div>
        <div class="col-md-6">
            <h5><?php _e( 'International Payment', WPD_NAME ) ?></h5>
            <ul>
                <?php if ( WPD_Common::wpd_set( $options, 'name' ) ): ?><li><span><?php _e( 'Payable To:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'name' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'bank_name' ) ): ?><li><span><?php _e( 'Bank Name:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'bank_name' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'bic' ) ): ?><li><span><?php _e( 'BIC / SWIFT Code:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'bic' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'iban' ) ): ?><li><span><?php _e( 'IBAN number/Account Number:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'iban' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'address' ) ): ?><li><span><?php _e( 'Street Address:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'address' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'postal_address' ) ): ?><li><span><?php _e( 'Postal Address:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'postal_address' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'contact_no' ) ): ?><li><span><?php _e( 'Telephone Number:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'contact_no' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'fax_no' ) ): ?><li><span><?php _e( 'Fax Number:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'fax_no' ) ?></i></li><?php endif; ?>
                <?php if ( WPD_Common::wpd_set( $options, 'branch_code' ) ): ?><li><span><?php _e( 'Branch Code:', WPD_NAME ) ?></span><i><?php echo WPD_Common::wpd_set( $options, 'branch_code' ) ?></i></li><?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
	jQuery('div#bank_layer').on('click', function () {
	    jQuery('div#bank_transcation').hide();
	    jQuery('div#bank_transcation').empty();
	    jQuery('div.overlay').fadeOut(500);
	    jQuery('div.donation-modal-preloader').fadeOut(500);
	    jQuery('div.donation-modal-box').fadeOut(500);
	    window.location.replace(homeUrl);
	});
    });
</script>