<?php
/* ==================================================================
  PayPal Express Checkout Call
  ===================================================================
 */
require_once ("paypalfunctions.php");
if ( session_id() == '' ) session_start();
$sh_express    = new WPD_Express_checkout;
$PaymentOption = "PayPal";
if ( $PaymentOption == "PayPal" ) {
    /*
      '------------------------------------
      ' The paymentAmount is the total value of
      ' the shopping cart, that was set
      ' earlier in a session variable
      ' by the shopping cart page
      '------------------------------------
     */

    $finalPaymentAmount = WPD_Common::wpd_set( $_SESSION, "Payment_Amount" );
    //printr($finalPaymentAmount);
    /*
      '------------------------------------
      ' Calls the DoExpressCheckoutPayment API call
      '
      ' The ConfirmPayment function is defined in the file PayPalFunctions.jsp,
      ' that is included at the top of this file.
      '-------------------------------------------------
     */

    //$resArray = ConfirmPayment ( $finalPaymentAmount ); //Remove comment with ontime payment.

    $resArray = $sh_express->CreateRecurringPaymentsProfile();

    $ack = strtoupper( WPD_Common::wpd_set( $resArray, "ACK" ) );

    if ( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" ) {
        $resArray2 = $sh_express->ConfirmPayment( $finalPaymentAmount );
        $ack       = strtoupper( WPD_Common::wpd_set( $resArray, "ACK" ) );

        $resArray  = array_merge( $resArray, $resArray2 );
    }

    //echo "<pre>";print_r($resArray);exit;
    if ( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" ) {

        ?>

        <div class="confirm_popup">
            <h2><?php _e( 'Transaction Complete', WPD_NAME ) ?></h2>
            <table>
                <tr>
                    <td><?php _e( 'Thank you for your payment.', WPD_NAME ) ?></td>
                </tr>
            </table>
            <form>
                <a href="<?php echo get_home_url(); ?>"><input id="paypa_thank_close" type="button" value="<?php _e( 'Close', WPD_NAME ) ?>"/></a>
            </form>
        </div>
        <?php
        //Lifeline_Web_Common::lifeline_scriptTag( 'jQuery("input#paypa_thank_close").live("click",function(){jQuery("div.confirm_popup").fadeOut(500),$("div.donation-modal-wraper").css({"z-index":999999}),$("div.donation-modal-preloader").hide(),jQuery("div#paypal_recuring_confirm").empty(),window.location.replace(homeUrl)});' );
        //Lifeline_Web_Common::lifeline_scriptTag( 'var get_height=jQuery("div#paypal_recuring_confirm > div.confirm_popup").height(),height=get_height/2;jQuery("div#paypal_recuring_confirm > div.confirm_popup").css({"margin-top":-height});' );

        $amount      = $_SESSION['amount'];
        $currency    = $_SESSION['currency'];
        $rec_cycle   = $_SESSION['Billing_Period'];
        $cycle_time  = '';
        $f_name      = $_SESSION['f_name'];
        $l_name      = $_SESSION['l_name'];
        $email       = $_SESSION['email_'];
        $contact_no  = $_SESSION['contact_no'];
        $address     = $_SESSION['address'];
        $date        = date( "Y-m-d H:i:s" );
        $type        = __( 'Reccuring', WPD_NAME );
        $source      = __( 'PayPal', WPD_NAME );
        $trans_id    = WPD_Common::wpd_set( $resArray, "TRANSACTIONID" );
        $donatioType = $_SESSION['donationType'];
        $postId      = $_SESSION['donationPost'];
        global $wpdb;
        global $charset_collate;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $table_name  = $wpdb->prefix . "wpd_easy_donation";
        $wpdb->show_errors();
        $query       = "INSERT INTO $table_name ( amount, cycle, cycle_time, f_name, l_name, email, contact, address, type, source, date, transaction_id, currency, donation_for, post_id ) VALUES ( '" . $amount . "', '" . $rec_cycle . "', '" . $cycle_time . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $contact_no . "', '" . $address . "', '" . $type . "', '" . $source . "', '" . $date . "', '" . $trans_id . "', '" . $currency . "', '" . $donatioType . "', '" . $postId . "' )";
        //print_r($query); exit();
        $result      = $wpdb->query( $query );
        //printr($result); exit();
exit;
    } else {
        echo '<div class="confirm_popup"><h2>' . __( 'Transaction Error', WPD_NAME ) . '</h2><table><tr><td>';

        //Display a user friendly Error on the page using any of the following error information returned by PayPal
        $ErrorCode         = urldecode( WPD_Common::wpd_set( $resArray, "L_ERRORCODE0" ) );
        $ErrorShortMsg     = urldecode( WPD_Common::wpd_set( $resArray, "L_SHORTMESSAGE0" ) );
        $ErrorLongMsg      = urldecode( WPD_Common::wpd_set( $resArray, "L_LONGMESSAGE0" ) );
        $ErrorSeverityCode = urldecode( WPD_Common::wpd_set( $resArray, "L_SEVERITYCODE0" ) );

        echo "GetExpressCheckoutDetails API call failed. ";
        echo "Detailed Error Message: " . $ErrorLongMsg;
        echo "Short Error Message: " . $ErrorShortMsg;
        echo "Error Code: " . $ErrorCode;
        echo "Error Severity Code: " . $ErrorSeverityCode;
        echo '</td></tr></table><form>
                    <input id="paypa_thank_close" type="button" value="' . __( "Close", WPD_NAME ) . '"/>
                </form>
			</div>';
    }
    Lifeline_Web_Common::lifeline_scriptTag( 'jQuery("input#paypa_thank_close").live("click",function(){jQuery("div.confirm_popup").fadeOut(500),$("div.donation-modal-wraper").css({"z-index":999999}),$("div.donation-modal-preloader").hide(),jQuery("div#paypal_recuring_confirm").empty(),window.location.replace(homeUrl)});' );
    Lifeline_Web_Common::lifeline_scriptTag( 'var get_height=jQuery("div#paypal_recuring_confirm > div.confirm_popup").height(),height=get_height/2;jQuery("div#paypal_recuring_confirm > div.confirm_popup").css({"margin-top":-height});' );

}
?>
