<?php
if (!defined("WPD_DIR")) die('!!!');

class WPD_Stripe_
{
    static public $error = '';
    static public $success = '';

    static public function wpd_stripe_reccuring($key, $amount, $currency, $token, $recuring = false, $data = array())
    {
        require_once WPD_ROOT . 'application/library/Stripe/vendor/autoload.php';
        \Stripe\Stripe::setApiKey($key);
        try {
            if (!$token) throw new Exception(__("The Stripe Token was not generated correctly.", WPD_NAME));
            if ($recuring === false) {
                $charge = \Stripe\Charge::create(
                    array(
                        "card" => $token,
                        "amount" => self::bcmul_alternative($amount, 100),
                        "currency" => $currency,
                    )
                );
                return 'trans' . $charge->id;
            } else {
                $id = strtolower(self::generateRandomString());
                $plan = \Stripe\Plan::create(array(
                    "amount" => self::bcmul_alternative($amount, 100),
                    "interval" => WPD_Common::wpd_set($data, 'interval'),
                    "interval_count" => ( int )WPD_Common::wpd_set($data, 'interval_count'),
                    "name" => "Plan For " . WPD_Common::wpd_set($data, 'mail'),
                    "currency" => strtolower($currency),
                    "id" => $id
                ));

                $customer = \Stripe\Customer::create(array(
                        "description" => "Customer for " . WPD_Common::wpd_set($data, 'mail'),
                        "source" => $token,
                        "email" => WPD_Common::wpd_set($data, 'mail')
                    )
                );
                $cId = $customer->id;
                $subscription = \Stripe\Subscription::create(array(
                    "customer" => $cId,
                    "plan" => $id
                ));
                return 'trans' . $subscription->id;
            }
        } catch (Exception $e) {
            return self::$error = $e->getMessage();
        }
    }

    static public function generateRandomString($length = 10)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static public function bcmul_alternative($n, $m, $dec = 0)
    {
        $value = $n * $m;
        if ($dec) {
            $value = round($value, $dec);
        }
        return $value;

    }
}