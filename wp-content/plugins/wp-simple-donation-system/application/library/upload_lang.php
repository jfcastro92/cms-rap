<?php
$fileName = $_FILES["lang_file"]["name"]; // The file name
$fileTmpLoc = $_FILES["lang_file"]["tmp_name"]; // File in the PHP tmp folder
$fileType = $_FILES["lang_file"]["type"]; // The type of file it is
$fileSize = $_FILES["lang_file"]["size"]; // File size in bytes
$fileErrorMsg = $_FILES["lang_file"]["error"]; // 0 for false... and 1 for true
if (!$fileTmpLoc) { // if file not chosen
    echo "ERROR: Please browse for a file before clicking the upload button.";
    exit();
}
$path	=	'../../languages/'.$fileName;
if(move_uploaded_file($fileTmpLoc, "$path")){
    echo "$fileName upload is complete";
} else {
    echo "move_uploaded_file function failed";
}