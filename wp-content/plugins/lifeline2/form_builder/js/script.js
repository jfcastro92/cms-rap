jQuery(document).ready(function ($) {
    var form_id = $('select#form_loader').val();
    if (typeof form_id != 'undefined') {

	var data = 'id=' + form_id + '&action=lifeline2_builder_form_load';
	jQuery.ajax({
	    type: "post",
	    cache: false,
	    url: ajaxurl,
	    data: data,
	    dataType: 'json',
	    beforeSend: function () {
		jQuery('#wp-appointment-overlap').fadeIn('slow');
		jQuery('#wp-appointment-overlap .loader').fadeIn('slow');
	    },
	    success: function (response) {
		jQuery('input.builder-name').val(response.title);
		jQuery('#formBuilder').empty();
		jQuery('#formBuilder').formBuilder({
		    save_url: ajaxurl,
		    load_url: response.d,
		    test_url: plugin_dir + '/form_builder/builder-templates/data.json',
		    onSaveForm: function () {
			return false;
		    }
		});
		jQuery('#wp-appointment-overlap .loader').fadeOut('slow');
		jQuery('#wp-appointment-overlap').fadeOut('slow');
		setTimeout(function () {
		    jQuery('button#save').attr('data-id', form_id);
		}, 2000);
	    }

	});
    } else {
	jQuery('#formBuilder').formBuilder({
	    save_url: ajaxurl,
	    test_url: plugin_dir + 'form_builder/builder-templates/default.json',
	    load_url: '',
	    onSaveForm: function () {
		return false;
	    }
	});
    }
    /*set environemtn for new form*/
    $('button#add_new_form').on('click', function () {
	jQuery('#wp-appointment-overlap').fadeIn('slow');
	jQuery('button#save').attr('data-id', '');
	jQuery('#wp-appointment-overlap .loader').fadeIn('slow');
	jQuery('div#formBuilder').empty();
	jQuery('input.builder-name').val('Untitle');
	jQuery('#formBuilder').formBuilder({
	    save_url: ajaxurl,
	    test_url: plugin_dir + 'form_builder/builder-templates/default.json',
	    load_url: '',
	    onSaveForm: function () {
		return false;
	    }
	});
	jQuery('#wp-appointment-overlap .loader').fadeOut('slow');
	jQuery('#wp-appointment-overlap').fadeOut('slow');
    });

    /*delete current form*/
    $('button#delete_current_form').live('click', function () {
	var $id = jQuery('button#save').data('id');
	jQuery('div#formBuilder').empty();
	var data = 'id=' + $id + '&action=lifeline2_builder_delete_form';

	jQuery.ajax({
	    type: "post",
	    cache: false,
	    url: ajaxurl,
	    data: data,
	    beforeSend: function () {
		jQuery('#wp-appointment-overlap').fadeIn('slow');
		jQuery('#wp-appointment-overlap .loader').fadeIn('slow');
	    },
	    success: function (response) {
		if (response != '') {
		    jQuery("select#form_loader").select2('destroy');
		    jQuery('select#form_loader').empty();
		    jQuery('select#form_loader').append(response);
		    jQuery("select#form_loader").select2();
		    jQuery("select#form_loader").select2("val", "");
		}
		jQuery('div#formBuilder').empty();
		jQuery('input.builder-name').val('Untitle');
		jQuery('#formBuilder').formBuilder({
		    save_url: ajaxurl,
		    test_url: plugin_dir + 'form_builder/builder-templates/default.json',
		    load_url: '',
		    onSaveForm: function () {
			return false;
		    }
		});
		jQuery('#wp-appointment-overlap .loader').fadeOut('slow');
		jQuery('#wp-appointment-overlap').fadeOut('slow');
	    }

	});
    });

    /*chage form data on select specific form*/
    $('select#form_loader').on('change', function () {
	var $val = $(this).val();
	jQuery('div#formBuilder').empty();
	var data = 'id=' + $val + '&action=lifeline2_builder_form_load';
	jQuery.ajax({
	    type: "post",
	    cache: false,
	    url: ajaxurl,
	    data: data,
	    dataType: 'json',
	    beforeSend: function () {
		jQuery('#wp-appointment-overlap').fadeIn('slow');
		jQuery('#wp-appointment-overlap .loader').fadeIn('slow');
	    },
	    success: function (response) {
		jQuery('input.builder-name').val(response.title);
		jQuery('#formBuilder').formBuilder({
		    save_url: ajaxurl,
		    load_url: response.d,
		    test_url: plugin_dir + 'form_builder/builder-templates/data.json',
		    onSaveForm: function () {
			return false;
		    }
		});
		jQuery('#wp-appointment-overlap .loader').fadeOut('slow');
		jQuery('#wp-appointment-overlap').fadeOut('slow');
		setTimeout(function () {
		    jQuery('button#save').attr('data-id', $val);
		}, 1000);
	    }

	});
    });
});