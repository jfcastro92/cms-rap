/**
 * jQuery form builder
 * Copyright (c) 2014 (v3.0) Shlomi Nissan, 1ByteBeta (http://www.1bytebeta.com)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 */
(function ($) {

    $.fn.formBuilder = function (options) {

        // Set default settings
        var settings = $.extend({
            test_url: '/',
            load_url: '/',
            save_url: '/'
        }, options);
        var dummy = settings.test_url;
        var form_file_data = settings.load_url;
        /*******************************************************/
        /*	Fields and Tabs
         /*******************************************************/
        var arraydata = [
            {
                add_fields: fb_add_new,
                field_setting: fb_field_setting,
                single_line_txt: fb_single_line_txt,
                paragraph_txt: fb_paragraph_txt,
                multiple_choice: fb_multiple_choice,
                number: fb_number,
                checkboxes: fb_checkbox,
                dropdown: fb_dropdown,
                email: fb_email,
                date: fb_date,
                save_form: fb_save,
                field_label: fb_field_label,
                field_choice: fb_field_choise,
                field_desc: fb_field_desc,
                add_field: fb_add_field,
                remove_field: fb_remove_field,
                loading: fb_loading,
                first_choice: fb_first_choice,
                secound_choice: fb_secound_choice,
                third_choice: fb_third_choice,
            }
        ];

        /*
         fieldAdd
         Adding a new form field on .new-element click
         */
        var fieldAdd = function () {

            // Bind new field buttons
            $('.new-element').live('click', function () {

                clearSelectedElements();
                var sortableElements = $('#sortable-elements');
                sortableElements.sortable({
                    stop: function () {
                        reorderElements();
                    }
                });

                var tpl = $(this).data('type');
                var data = {
                    'label': fb_untitle,
                    'position': $('.form-element').length
                };

                dust.render(tpl, data, function (err, out) {
                    var output_tab = attachTemplateToData(out, arraydata);
                    sortableElements.append(output_tab);
                    fieldSelect();

                    var newElement = $('#element-' + data['position']);

                    currentlySelected = newElement;

                    currentlySelected.addClass('selected');
                    tabs.showTab('#field-settings');

                    bindSettings();
                    repositionToolbox();
                    isFieldOptions();
                    jQuery('html, body').animate({
                        scrollTop: jQuery("ul#sortable-elements").find('#element-' + data['position']).offset().top - 50
                    }, 1000);
                });

            });

        }


        /*
         fieldSelect
         Show settings pan and bind fields on .form-element click
         */
        var fieldSelect = function () {

            $('.form-element').unbind();

            // Form element clicked
            $('.form-element').live('click', function () {

                // Remove selected class from all elements
                clearSelectedElements();

                // Add selected class to selected element
                $(this).addClass('selected');

                // View the settings base on element type
                if ($(this).data('type') == 'form-settings') {

                    tabs.showTab('#form-settings');

                } else {

                    tabs.showTab('#field-settings');
                    currentlySelected = $(this);
                    bindSettings();
                    repositionToolbox();
                    isFieldOptions();
                }

            });
        }


        /*
         tabSelect
         Adjust form fields based on tab selection
         */
        var tabSelect = function () {

            // Switch tabs
            $('.toolbox-tab').live('click', function () {

                clearSelectedElements();

                if ($(this).data('target') == '#form-settings') {
                    $('#form-settings-element').addClass('selected');
                }

                if ($(this).data('target') == '#field-settings') {

                    if (!currentlySelected) {
                        $('#element-0').addClass('selected');
                        currentlySelected = $('#element-0');
                    } else {
                        currentlySelected.addClass('selected');
                    }

                    bindSettings();
                    repositionToolbox();
                }

            });

        }


        /*******************************************************/
        /*	Bind controls
         /*******************************************************/


        /*
         bindTextFields
         Binds textfields in the settings panel to form textfields
         */
        var bindTextFields = function () {
            // Bind controls
            $('.bind-control').each(function () {

                var target = $(this).data('bind');

                $(this).on("keyup", function () {

                    if (currentlySelected == '') {
                        $(target).html($(this).val());
                    } else {

                        if (currentlySelected.data('type') != 'element-dropdown') {
                            currentlySelected.find(target).next('.choice-label').html($(this).val());
                        } else {

                            currentlySelected.find(target).html($(this).val());
                        }

                    }


                });

            });
        }


        /*
         bindButtons (checkboxes and radio buttons)
         Binds buttons from the settings pane to form elements
         */
        var bindButtons = function () {
            $('.option').unbind();

            $('.option').live('click', function () {

                var target = $(this).parent().next('input').data('bind');
                var value = ($(currentlySelected).data('type') != 'element-dropdown') ? 'checked' : 'selected';

                $(currentlySelected).find(target).prop(value, function (i, val) {
                    return !val;
                });

            });
        }


        /*
         bindSettings
         Binds settings controls to form elements (labels, required, choices, etc)
         */
        var bindSettings = function () {
            // Field Label
            $('#field-label').val(currentlySelected.data('label'));

            $('#field-label').on("keyup", function () {

                currentlySelected.children('label').children('.label-title').html($(this).val());
                currentlySelected.data('label', $(this).val());
            });

            // Description
            if (currentlySelected.data('type') == 'element-section-break') {
                $('#description').val(currentlySelected.children('.description').html());
            }

            $('#description').on("keyup", function () {

                currentlySelected.children('.description').html($(this).val());
                currentlySelected.data('description', $(this).val());

            });

            // Choices
            if (currentlySelected.data('type') == 'element-multiple-choice' || currentlySelected.data('type') == 'element-checkboxes' || currentlySelected.data('type') == 'element-dropdown') {

                $('#field-choices').css('display', 'block');
                $('#field-choices').html('<div class="form-group"><label>' + fb_field_choise + '</label></div>');

                var choices = [];

                var items = currentlySelected.children('.choices').children('.choice');

                if (currentlySelected.data('type') == 'element-dropdown') {
                    items = currentlySelected.children('.choices').children('option');
                }

                items.each(function (i) {

                    if (currentlySelected.data('type') != 'element-dropdown') {

                        // Radio buttons, checkboxes

                        var checked = $(this).children('label').children('input').is(':checked') ? true : false;
                        var bindingClass = $(this).children('label').children('input').attr('class');
                        var title = $(this).children('label').children('.choice-label').html();

                    } else {

                        // Dropdown

                        var title = $(this).val();
                        var bindingClass = $(this).attr('class');
                        var checked = $(this).is(':selected') ? true : false;

                    }


                    var data = {
                        'checked': checked,
                        'title': title,
                        'position': i + 1,
                        'bindingClass': bindingClass,
                    };

                    choices.push(data);

                });

                var data = {
                    "choices": choices
                }
                // Render the choices

                dust.render(currentlySelected.children('.choices').data('type'), data, function (err, out) {

                    $('#field-choices').append(out);
                    bindTextFields();
                    bindButtons();
                    controlMultipleChoice();

                });

            } else {
                $('#field-choices').css('display', 'none');
            }

            // Required
            if (currentlySelected.hasClass('required')) {
                $('#required').prop("checked", true);
            } else {
                $('#required').prop("checked", false);
            }

            $('#required').unbind();
            $('#required').change(function () {

                currentlySelected.toggleClass('required');

                var data = {};
                if (this.checked) {

                    dust.render('required', data, function (err, out) {

                        currentlySelected.children('label').append(out);

                    });

                } else {
                    currentlySelected.children('label').children('.required-star').remove();
                }

            });

        }


        /*******************************************************/
        /*	Controls
         /*******************************************************/


        /*
         controlSettings
         Attach settings control (Remove Field, Add Field)
         */
        var controlSettings = function () {

            // Remove field
            $('#control-remove-field').live('click', function () {

                if (currentlySelected !== '') {

                    if ($('.form-element').length > 1) {
                        currentlySelected.remove();
                        reorderElements();
                        tabs.showTab('#add-field');
                        clearSelectedElements();
                    } else {
                        alert(fb_delete_notic);
                    }

                }

            });

            $('#control-add-field').live('click', function () {
                tabs.showTab('#add-field');
                clearSelectedElements();
            });

        };


        /*
         controlMutlipleChoice
         Attach multiple choice controls (remove choice, ddd choice)
         */
        var controlMultipleChoice = function () {

            // Remove choice
            $('.remove-choice').unbind();
            $('.remove-choice').live('click', function () {

                if ($(this).parent().parent().children('.choice').length > 1) {

                    // Delete choice from form
                    var deleteItem = $(this).data('delete');

                    if ($(currentlySelected).data('type') == 'element-dropdown') {
                        $(currentlySelected).find(deleteItem).remove();
                    } else {
                        $(currentlySelected).find(deleteItem).parent().parent().remove();
                    }

                    // Delete choice from settings
                    $(this).parent().remove();

                    // Bind new fields
                    bindTextFields();
                    controlMultipleChoice();
                }

            });

            // Add Choice
            //$('.add-choice').unbind();
            $(document).off('click', '.add-choice');
            $(document).on('click', '.add-choice', function () {

                delete dust.cache['choice-checkbox'];
                console.log(dust.cache);
                var lastChoice = '', count = '', items = '', childs = '';
                // Dropdown
                if (currentlySelected.data('type') == 'element-dropdown') {
                    count = currentlySelected.children('.choices').children('option').length;
                    items = currentlySelected.children('.choices').children('option');
                } else {
                    count = currentlySelected.children('.choices').children('.choice').length;
                    items = currentlySelected.children('.choices').children('.choice');
                }
                $(items.get(-1)).each(function (i) {
                    if (currentlySelected.data('type') == 'element-dropdown') {
                        var choiceString = $(this).attr('class');
                    } else {
                        var choiceString = $(this).find('input').attr('class');
                    }
                    lastChoice = count;
                });

                childs = {
                    'bindingClass': 'option-' + parseInt(lastChoice + 1),
                    'title': fb_untitle
                };
                var data = {
                    "choices": childs,
                }

                // Render a new choice in settings
                delete dust.cache['choice-checkbox'];
                dust.render(currentlySelected.children('.choices').data('type'), data, function (err, out) {
                    $('#field-choices').append(out);

                    // Set template based on type
                    if (currentlySelected.data('type') == 'element-multiple-choice') {
                        template = 'choice-radio';
                    }

                    if (currentlySelected.data('type') == 'element-checkboxes') {
                        template = 'choice-checkbox';
                    }

                    if (currentlySelected.data('type') == 'element-dropdown') {
                        template = 'choice-dropdown';
                    }


                    var elementId = currentlySelected.attr('id').replace('element-', '');
                    // Load template
                    data = {
                        'title': fb_untitle,
                        'value': 'untitled',
                        'lastChoice': parseInt(lastChoice + 1),
                        'elementId': elementId
                    }
                    dust.render(template, data, function (err, out) {
                        currentlySelected.children('.choices').append(out);
                    });

                    // Bind new fields
                    bindTextFields();
                    bindButtons();
                    controlMultipleChoice();

                });

            });

        }


        /*******************************************************/
        /*	Helpers
         /*******************************************************/

        var isFieldOptions = function () {
            if (currentlySelected.data('type') == 'element-section-break') {
                $('#field-options').hide();
                $('#field-description').show();
            } else {
                $('#field-options').show();
                $('#field-description').hide();
            }
        }

        /*
         repositionToolbox
         Change the position of the toolbox based on active selection
         */
        var repositionToolbox = function () {
            topOffset = currentlySelected.position().top;
            toolboxOffset = 115;
            offset = topOffset - toolboxOffset;
            $('#field-settings').css({'margin-top': offset + 50 + 'px'});
            $('.left-col').css('height', '100%');
        }


        /*
         clearSelectedElements
         Remove currently selected element
         */
        var clearSelectedElements = function () {

            // Remove selected class from all elements
            $('.form-element').each(function () {
                $(this).removeClass('selected');
            });

        };

        /*
         reorderElements
         Update element id based on position
         */
        var reorderElements = function () {

            $('#sortable-elements').sortable({
                stop: function () {
                    reorderElements();
                }
            });

            $('#sortable-elements li').each(function (i) {
                $(this).attr('id', 'element-' + i);
            });

            fieldSelect();
        }


        /*
         serialize
         Serialize form elements into a JSON string
         */
        var serialize = function () {

            var formData = {};

            formData['title'] = $('#form-title').val();
            formData['description'] = $('#form-description').val();


            formData['fields'] = Array();

            $('#sortable-elements li').each(function (i) {

                var element = {
                    'title': $(this).data('label'),
                    'type': $(this).data('type'),
                    'required': $(this).hasClass('required') ? true : false,
                    'position': i + 1,
                    'description': $(this).data('description')
                }

                // If element has multiple choices
                if (element['type'] == 'element-multiple-choice' || element['type'] == 'element-checkboxes' || element['type'] == 'element-dropdown') {

                    var choices = [];

                    if (element['type'] == 'element-dropdown') {

                        // Collect choices for dropdown
                        $(this).find('.choices').children('option').each(function (index) {

                            var choice = {
                                'title': $(this).val(),
                                'value': $(this).val(),
                                'checked': $(this).is(':selected') ? true : false,
                            }

                            choices.push(choice);
                            element['choices'] = choices;

                        });

                    } else {

                        $(this).find('.choices').children('.choice').each(function (index) {
                            var choice = {
                                'title': $(this).children('label').children('.choice-label').html(),
                                'value': $(this).children('label').children('.choice-label').html(),
                                'checked': $(this).children('label').children('input').is(':checked') ? true : false,
                            }
                            choices.push(choice);
                            element['choices'] = choices;

                        });

                    }

                }

                formData['fields'].push(element);
            });


            var serialized = JSON.stringify(formData);

            // Process the form data here...
            return serialized;


        }


        /*******************************************************/
        /*	Entry Point
         /*******************************************************/

        // Globl vars
        var currentlySelected = '';
        var tabs = '';


        // Auto load templates
        dust.onLoad = function (name, callback) {
            $.ajax(plugin_dir + '/form_builder/builder-templates/' + name + '.tpl', {
                success: function (data) {
                    callback(undefined, data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    callback(textStatus, undefined);
                }
            });
        };

        var base = {};

        //
        var obj = this;

        dust.render('formbuilder-base', base, function (err, out) {

            var output = attachTemplateToData(out, arraydata);
            obj.append(output);
        });

        // Get data
        $.getJSON(dummy, function (data) {
            // Load the base template
            var getData = '';
            if (form_file_data == '') {
                getData = data;
            } else {
                getData = $.parseJSON(form_file_data);
            }
            base = {
                form: getData,
                fieldSettings: false,
                formSettings: false
            };

            // Render the form
            dust.render('formbuilder-fields', base, function (err, out) {
                var field_output = attachTemplateToData(out, arraydata);
                $('.loading').fadeOut();
                $('#form-col').html(field_output);
                $('#form-elements').fadeIn();
                fieldAdd();
                fieldSelect();
                tabSelect();

                bindTextFields();
                controlSettings();
                reorderElements();

                tabs = $('.nav-tabs').tabs();

                $('button#save').on('click', function (e) {
                    jQuery('div.builder-area #builder_msg').empty();

                    var $this = $(this);
                    var id = jQuery(this).data('id');
                    var form_data = serialize();
                    var title = $('input.builder-name').val();
                    var data = 'id=' + id + '&title=' + title + '&formData=' + form_data + ' +&action=lifeline2_builder_data_save';

                    jQuery.ajax({
                        type: "post",
                        url: ajaxurl,
                        data: data,
                        dataType: 'json',
                        beforeSend: function () {
                            jQuery('#wp-appointment-overlap').fadeIn('slow');
                            jQuery('#wp-appointment-overlap .loader').fadeIn('slow');
                        },
                        success: function (response) {
                            if (typeof response == 'object') {
                                if (response.update != '') {
                                    jQuery('div.builder-area #builder_msg').html(response.update);
                                }
                                if (response.insert != '') {
                                    jQuery('div.builder-area #builder_msg').html(response.insert);
                                }
                                if (response.warning !== '') {
                                    jQuery('div.builder-area #builder_msg').html(response.warning);
                                }
                                if (response.id !== false) {
                                    jQuery($this).attr('data-id', response.id);
                                }
                            }
                            jQuery("select#form_loader").select2('destroy');
                            jQuery('select#form_loader').empty();
                            jQuery('select#form_loader').append(response.form);
                            jQuery("select#form_loader").select2();
                            jQuery('div.builder-area #builder_msg').show();
                            jQuery('#wp-appointment-overlap .loader').fadeOut('slow');
                            jQuery('#wp-appointment-overlap').fadeOut('slow');
                            jQuery('html, body').animate({
                                scrollTop: jQuery("div.builder-area > #builder_msg").offset().top - 50
                            }, 1000);
                            setTimeout(function () {
                                jQuery('div.builder-area > #builder_msg').fadeOut('slow');
                            }, 5000);
                        }

                    });
                    return false;
                });


            });

        });

        ///

    };
    // End plugin


    attachTemplateToData = function (template, data) {
        var i = 0,
            len = data.length,
            fragment = '';

        function replace(obj) {
            var t, key, reg;
            for (key in obj) {
                reg = new RegExp('@' + key + '@', 'ig');
                t = (t || template).replace(reg, obj[key]);
            }
            return t;
        }

        for (i = 0; i < len; i++) {
            fragment += replace(data[i]);
        }
        return fragment;
    };
}(jQuery));
