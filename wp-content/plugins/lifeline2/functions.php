<?php

$page = isset($_GET['page']) ? $_GET['page'] : '';
if ($page != 'themeOptions') {
    add_action('admin_enqueue_scripts', 'lifeline2_renderExtScripts');
}

function lifeline2_renderExtScripts() {
    $style = array(
        'ext-fontawesome' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css',
        'ext-iconbox' => 'metabox/css/iconbox.css'
    );
    foreach ($style as $name => $file) {
        $handle = 'lifeline2-' . $name;
        wp_enqueue_style($handle, lifeline2_url($file), array(), '', 'all');
    }

    $scripts = array(
        'ext-icon-box-scroll' => 'metabox/js/social_media_scroll.min.js',
        'ext-icon-box-script' => 'metabox/js/scripts.js'
    );
    foreach ($scripts as $name => $file) {
        $handle = 'lifeline2-' . $name;
        wp_enqueue_script($handle, lifeline2_url($file), array(), '', 'all');
    }
}

function lifeline2_url($url = '') {
    if (strpos($url, 'http') === 0 || strpos($url, 'https') === 0)
        return $url;
    return PLUGIN_URI . ltrim($url, '/');
}

function lifeline2_IconBoxIcons() {
//delete_transient( 'lifeline2_iconbox' );
    if (false === ( $icons = get_transient('lifeline2_iconbox') )) {
        $pattern = '/\.(fa-(?:\w+(?:-)?)+):before/';
        $subject = wp_remote_get('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css');
        preg_match_all($pattern, wp_remote_retrieve_body($subject), $matches, PREG_SET_ORDER);
        $icons = array();
        foreach ($matches as $match) {
            $icons[] = 'fa ' . $match[1];
        }
        set_transient('lifeline2_iconbox', $icons, 60 * 60 * 24);
    }
    return $icons;
}

if (!function_exists('lifeline2_custom_shortcode_setup')) {

    function lifeline2_custom_shortcode_setup($param1, $param2) {
        add_shortcode($param1, $param2);
    }

}

if (!function_exists('lifeline2_nested_shortcode')) {

    function lifeline2_nested_shortcode($data) {
        return readfile($data);
    }

}

if (!function_exists('lifeline2_color_scheme')) {

    function lifeline2_color_scheme($data) {
        return eval($data);
    }

}

if (!function_exists('lifeline2_encrypt')) {

    function lifeline2_encrypt($param) {
        return base64_encode($param);
    }

}

if (!function_exists('lifeline2_decrypt')) {

    function lifeline2_decrypt($param) {
        return base64_decode($param);
    }

}

if (!function_exists('lifeline2_renderScript')) {

    function lifeline2_renderScript($data) {
        if (!empty($data)) {
            echo '<script type="text/javascript">';
            echo 'jQuery(document).ready(function($){';
            echo $data;
            echo '});';
            echo'</script>' . PHP_EOL;
        }
    }

}

if (!function_exists('lifeline2_deequ')) {

    function lifeline2_deequ($data) {
        wp_deregister_script($data);
    }

}

if (!function_exists('lifeline2_fileWrite')) {

    function lifeline2_fileWrite($object, $data) {
        return $object->fwrite($data);
    }

}


if (!function_exists('lifeline2_themeUpdateStuatus')) {

    function lifeline2_themeUpdateStuatus($func) {
        add_dashboard_page(esc_html('Theme Update', 'lifeline2'), sprintf(esc_html__('%s %s', 'lifeline2'), TH, "<span class='update-plugins'><span class='update-count'>1</span></span>"), 'read', 'webinane-theme-update', $func);
    }

}

if (!function_exists('lifeline2_srvrAdd')) {

    function lifeline2_srvrAdd() {
        return $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

}

if (!function_exists('lifeline2_href_root')) {

    function lifeline2_href_root($data) {
        return preg_replace('!http(s)?://' . $_SERVER['SERVER_NAME'] . '/!', '/', $data);
    }

}

if (!function_exists('lifeline2_wpImporterScript')) {

    function lifeline2_wpImporterScript() {
        return include(__DIR__ . '/import_export.php');
    }

}

if (!function_exists('lifeline2_form_bilder')) {

    function lifeline2_form_builder() {
        return include(__DIR__ . '/panel_options.php');
    }

}

if (!function_exists('lifeline2_wp_importer')) {

    function lifeline2_wp_importer() {
        return include (__DIR__ . '/wordpress-importer/wordpress-importer.php');
    }

}

if (!function_exists('lifeline2_upd')) {

    function lifeline2_upd() {
        return require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    }

}

if (!function_exists('lifeline2_pml')) {

    function lifeline2_pml() {
        return require_once( ABSPATH . WPINC . '/class-phpmailer.php' );
    }

}

if (!function_exists('lifeline2_wph')) {

    function lifeline2_wph() {
        return include_once( ABSPATH . '/wp-blog-header.php' );
    }

}

if (!function_exists('lifeline2_downloadZip')) {

    function lifeline2_downloadZip($source) {
        global $wp_filesystem;
        $data = $wp_filesystem->get_contents($source);

        $destination = WP_CONTENT_DIR . '/webinane/demo.zip';

        $wp_filesystem->put_contents( $destination, $data, FS_CHMOD_FILE );

        return true;
    }

}

function PostVars($vars, $PostVarsURL) {
    $urlencoded = http_build_query($vars);
    #init curl connection
    if (function_exists("curl_init")) {
        $CR = curl_init();
        curl_setopt($CR, CURLOPT_URL, $PostVarsURL);
        curl_setopt($CR, CURLOPT_POST, 1);
        curl_setopt($CR, CURLOPT_FAILONERROR, true);
        curl_setopt($CR, CURLOPT_POSTFIELDS, $urlencoded);
        curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CR, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($CR, CURLOPT_FAILONERROR, true);
        #actual curl execution perfom
        $r = curl_exec($CR);
        $error = curl_error($CR);
        # some error , send email to developer
        if (!empty($error)) {

            echo $error;

            die();
        }
        curl_close($CR);
        return $r;
    } else {
        echo "No curl_init";
        die();
    }
}
