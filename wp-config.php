<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Bb2pD8f%LfTP51Qo=]B01%J*vNZD<c&`7-loIq45E4&2SvB)C0.2H~P!Eh#j[.I9');
define('SECURE_AUTH_KEY',  '12Q.:|T0DoM,{=eVT|P)bMhUJ_pL{&X]ZtD3U!br+:vSTK%]h[r0[KAq-D>/wg 3');
define('LOGGED_IN_KEY',    'JHF.pEm]7%~sDLSHp_t*E{`2}Da;m/cc?V#)-|HCLmp/I&5JPRePn7zrq2y7biEz');
define('NONCE_KEY',        '?(T~ZD{Nvea&_I1Ux#6I}p0JX8$8-elhT=}=/{0]~mLCakLJD fu]5/[%yX/0[Fw');
define('AUTH_SALT',        '&/:%D$iqABOqTS/c&x7/FbD1?][6_$oI<G{GR#6Rw-OE-@5D!Zn;sl}4W2+ Qy}-');
define('SECURE_AUTH_SALT', 'x{P2~cN4S%B~#b9($}Z^fU~6j4y.9_GY|LUsCw;?sSyrSy][@;%Z_;8EZjhm]A$L');
define('LOGGED_IN_SALT',   '5&pITh(VaGasFA{#S3v/6:yu]EJH,m.VVq0j>969MToFuC.D<gT)kU@xskVG&69~');
define('NONCE_SALT',       ':XqO$~Rn0h3dL*@*.Y#CS)X3n!>hFJhJ/a;|g2WjQ?z?yP;Dd/z5qgvLWV5qnM6l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
